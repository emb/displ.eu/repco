**Find last used docker compose yml**

```
docker compose ls

// you find a NAME for the composed containers
// use it in -p as a projekt name to find instances, or interact via composer

docker compose -p repco-development ps


```

**Generic Commands to rebuild a "living" composer container**
see above commands to check (everytime with multiple developers involved!)
which composer yaml was used.

```
git pull | git checkout | change some testiecode

//Fast rebuild
docker compose -p repco-development -f /path/to/the/right/docker-compose.whatever.build.yml up -d --no-deps --build app

docker compose -f /path/to/the/right/docker-compose.whatever.build.yml -p repco-development(find via ls) build app(from yml file)

docker compose -f /path/to/the/right/docker-compose.whatever.build.yml -p repco-development(find via ls) down app(from yml file)

docker compose -f /home/devops/services/docker/repco/docker/docker-compose.arbeit.build.yml -p repco-development up -d app

// Watch whats going on

docker compose -f /home/devops/services/docker/repco/docker/docker-compose.arbeit.build.yml -p repco-development logs app -f -n 100
```

