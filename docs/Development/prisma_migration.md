
add an empty migration

```
yarn prisma migrate dev --create-only --name something-useful
// edit SQL file in migrations
yarn prisma migrate dev

```


ollback migrations:

```
// Compare Database with modell
yarn prisma migrate diff --from-url "postgresql://repco:repco@n.n.n.n:5432/repco_livedump" --to-schema-datamodel packages/repco-prisma/prisma/schema.prisma

yarn prisma migrate diff --from-url "postgresql://repco:repco@n.n.n.n:5432/repco_livedump" --to-schema-datamodel packages/repco-prisma/prisma/schema.prisma --script > down.sql
 // Copy 
 prisma migrate dev --name add_things

yarn prisma db execute --file ./down.sql --schema prisma/schema.prisma

yarn prisma migrate resolve --rolled-back add_things

```



[https://www.prisma.io/docs/orm/prisma-migrate/workflows/generating-down-migrations]