# Yarn Installation

``
curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | sudo apt-key add -
echo "deb https://dl.yarnpkg.com/debian/ stable main" | sudo tee /etc/apt/sources.list.d/yarn.list
apt update
apt install yarn

# Verify with
yarn --version

``




Ref: https://gcore.com/learning/how-to-install-yarn-on-ubuntu/
