**Working with a local dev instance**

Via [https://app.elasticvue.com] a nice GUI is avaliable, that works quite out of the box

2 http Settings in the container are important (can be added via docker compose file docker-compose.yml)
```
      - "http.cors.enabled=true"
      - "http.cors.allow-origin=/.*/"
```

To run Elastic with a small footprint:
In REST Tab interface / or via REST API:
```
// Optimize memory footprint
Method: PUT
Path: _cluster/settings
{
  "transient": {
    "cluster.routing.allocation.disk.watermark.low": "1gb",
    "cluster.routing.allocation.disk.watermark.high": "1gb",
    "cluster.routing.allocation.disk.watermark.flood_stage": "0.1gb",
    "cluster.info.update.interval": "1m"
  }
}

// Reduce Replicas
Method: PUT
Path: /_all/_settings
{
  "index": {
    "number_of_replicas": 0
  }
}


```