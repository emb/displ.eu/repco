**Nice GUI 4 Elastic avaliable:**
https://elasticvue.com/
The REST Query Forms can save and show History.


**Elastic Explaining Errors REST Call**
GET http://localhost:9201/_cluster/allocation/explain?pretty



**Adjust Memory Footprint**
PUT http://localhost:9201/_cluster/settings

``
{
  "transient": {
    "cluster.routing.allocation.disk.watermark.low": "2gb",
    "cluster.routing.allocation.disk.watermark.high": "2gb",
    "cluster.routing.allocation.disk.watermark.flood_stage": "0.2gb",
    "cluster.info.update.interval": "1m"
  }
}
``


**Usefull Reference Links**
Filtering objects in results by search queries: https://www.elastic.co/guide/en/elasticsearch/reference/current/common-options.html#common-options-response-filtering
https://www.elastic.co/guide/en/elasticsearch/reference/current/cluster-allocation-explain.html
https://opster.com/analysis/elasticsearch-high-disk-watermark-90-exceeded-on/

