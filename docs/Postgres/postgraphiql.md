**Activate Debugging for Postgraphiql**

To activate the EXPLAIN Debugging view of postgraphiql
go to the file
packages/repco-graphql/src/lib.ts
and uncomment the line with allowExplain

```
export function getPostGraphileOptions() {
  return {
    //allowExplain: (req:any) => { return true; },
    showErrorStack: true,
    extendedErrors: ['hint', 'detail', 'errcode'],
    ....


```

Ref: [https://www.graphile.org/postgraphile/debugging/#via-postgraphiql-explain]