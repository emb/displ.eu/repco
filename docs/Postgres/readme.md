**Optimzed repco database dumping**

Live Repco Database has 4 tables with huge amount of data,
they are not necesary 4 some development tasks

Option 1 via docker exec

```
docker exec repco-db pg_dump -Z0 -U repco --no-owner --exclude-table-data="public.\"SourceRecord\""  --exclude-table-data="public.\"Block\"" --exclude-table-data="public.\"SourceRecord\"" --exclude-table-data="public.\"FailedDataSourceFetches\"" --exclude-table-data="public.\"IngestError"\"  --exclude-table-data="public.\"SourceRecordArchive"\" repco | gzip  > db-backup-exec-$(date +%d-%m-%y).sql.gz

```


Drawback: if terminated (STRG+C) severeal times PG becomes unresponsive for dumps.
The SQL Statements run in Background.

Option 2:

```
docker exec -it repco-db /bin/bash

//inside container

cd /tmp
pg_dump -Z0 -U repco --no-owner --exclude-table-data="public.\"SourceRecord\""  --exclude-table-data="public.\"Block\"" --exclude-table-data="public.\"SourceRecord\"" --exclude-table-data="public.\"FailedDataSourceFetches\"" --exclude-table-data="public.\"IngestError\"" --exclude-table-data="public.\"SourceRecordArchive\"" repco | gzip  > db-backup-inside-$(date +%d-%m-%y).sql.gz

exit

//outside container

cp repco-db:/tmp/db-backup-inside-$(date +%d-%m-%y).sql.gz ./

// target Host / Container:
cp db-backup-inside-$(date +%d-%m-%y).sql.gz ./repco-db:/tmp/
docker exec -t repco-db /bin/bash
// inside container
gunzip < db-backup-inside-$(date +%d-%m-%y).sql.gz | psql targetdatabase  // ex4Test: repco-$(date +%d-%m-%y)

```

**Copy Tables between Databases on the same server**

```
// via Shell or/and docker exec & Copy table with structure

pg_dump -U repco -Fc -f tableName.dump -t "public.\"tableName\""   "oldDB"
pg_restore -U repco -d "newDB" tableName.dump

// Copy only data

pg_dump -U repco -Fc -a -f tableName_data.dump -t "public.\"tableName\""   "oldDB"
pg_restore -U repco -d "newDB" tableName_data.dump

```



**Find PG process and kill them**

SQL calls via psql or similar
```

# Find active process to kill

SELECT * FROM pg_stat_activity WHERE state = 'active';

SELECT pid,state ,query,* FROM pg_stat_activity
ORDER BY state,pid DESC;

# kill thme by PID

SELECT pg_cancel_backend(<pid of the process>)

```
****
Generate VACUUM FULL Statements for alle Tables in a schema
```
SELECT format('VACUUM FULL ANALYZE %I.%I;', table_schema, table_name)
  FROM information_schema.tables
 WHERE table_schema = 'your_schema'
   AND table_type = 'BASE TABLE'
```


**Check Size of DB**

Paste in psql
```
DO $$
DECLARE
schema_name TEXT;
db_size TEXT;
lob_size TEXT;
table_size TEXT;
BEGIN
SELECT pg_size_pretty(pg_database_size(current_database())) INTO db_size;
RAISE NOTICE 'Total size of the database: %', db_size;

SELECT pg_size_pretty(pg_total_relation_size('pg_largeobject')) INTO lob_size;
RAISE NOTICE 'Size of large objects: %', lob_size;

FOR schema_name IN SELECT iss.schema_name FROM information_schema.schemata iss
LOOP
    SELECT pg_size_pretty(SUM(pg_total_relation_size(quote_ident(schema_name) || '.' || quote_ident(t.table_name)))) INTO table_size
    FROM information_schema.tables t
    WHERE t.table_schema = schema_name
    AND t.table_type = 'BASE TABLE';

    IF table_size IS NOT NULL THEN
        RAISE NOTICE 'Schema % total table size: %', schema_name, table_size;
    END IF;
END LOOP;
END $$;
```
--

**get Table sizes sorted**

```
SELECT relname AS "table_name", pg_size_pretty(pg_table_size(C.oid)) AS "table_size" FROM pg_class C LEFT JOIN pg_namespace N ON (N.oid = C.relnamespace) WHERE nspname NOT IN ('pg_catalog', 'information_schema') AND nspname !~ '^pg_toast' AND relkind IN ('r') ORDER BY pg_table_size(C.oid);
```

or (nice and sorted)

```
SELECT relname  as table_name, pg_size_pretty(pg_total_relation_size(relid)) As "Total Size", pg_size_pretty(pg_indexes_size(relid)) as "Index Size", pg_size_pretty(pg_relation_size(relid)) as "Actual Size" FROM pg_catalog.pg_statio_user_tables  ORDER BY pg_total_relation_size(relid) DESC;
```
or all details including indexes and rows

```
SELECT
	NSPNAME AS "schema",
	PG_CLASS.RELNAME AS "table",
	PG_SIZE_PRETTY(PG_TOTAL_RELATION_SIZE(PG_CLASS.OID)) AS "total_size",
	PG_SIZE_PRETTY(PG_RELATION_SIZE(PG_CLASS.OID)) AS "data_size",
	PG_SIZE_PRETTY(PG_INDEXES_SIZE(PG_CLASS.OID)) AS "index_size",

  PG_SIZE_PRETTY( pg_total_relation_size(relid) - pg_relation_size(relid) - pg_indexes_size(relid) ) AS "toase_size",

	PG_STAT_USER_TABLES.N_LIVE_TUP AS "rows",
	PG_SIZE_PRETTY(
		PG_TOTAL_RELATION_SIZE(PG_CLASS.OID) / (PG_STAT_USER_TABLES.N_LIVE_TUP + 1)
	) AS "total_row_size",
	PG_SIZE_PRETTY(
		PG_RELATION_SIZE(PG_CLASS.OID) / (PG_STAT_USER_TABLES.N_LIVE_TUP + 1)
	) AS "row_size"
FROM
	PG_STAT_USER_TABLES
	JOIN PG_CLASS ON PG_STAT_USER_TABLES.RELID = PG_CLASS.OID
	JOIN PG_CATALOG.PG_NAMESPACE AS NS ON PG_CLASS.RELNAMESPACE = NS.OID -- WHERE --   nspname = 'public' ORDER BY  pg_total_relation_size(pg_class.oid) DESC;
```
![alt text](image.png)

Expanation what the toast size is related to, and some implications by that
https://www.crunchydata.com/blog/postgres-toast-the-greatest-thing-since-sliced-bread

```
PG_RELATION_SIZE 
select 
  pg_relation_size(20306, 'main') as main,
  pg_relation_size(20306, 'fsm') as fsm,
  pg_relation_size(20306, 'vm') as vm,
  pg_relation_size(20306, 'init') as init,
  pg_table_size(20306), 
  pg_indexes_size(20306) as indexes,
  pg_total_relation_size(20306) as total;
```
---

**view dead tubles**
```
SELECT relname, n_dead_tup FROM pg_stat_user_tables ORDER BY n_dead_tup DESC;
```

---

Cheek Vacuum stats:
psql:
view Settings:

```

select *
from pg_settings
where name like '%autovacuum%'
```

View last runtime

```
SELECT schemaname, relname, last_vacuum, last_autovacuum, vacuum_count, autovacuum_count  -- not available on 9.0 and earlier FROM pg_stat_user_tables;
```

**dump selected data to a CSV (sample)**

open db container
```
docker exec -it db /bin/bash
```

and run via psql
```
COPY (select * from "ContentItem" LIMIT 1000) TO '/tmp/ContentItemDump1000.csv'  WITH DELIMITER ',' CSV HEADER;
```

Copy file
```
docker cp db:/tmp/ContentItemDump1000.csv ./
```

**dump selected data to a JSON (sample)**

open db container
```docker exec -it db /bin/bash```

and run via psql (use db, Tuples on,PSQL's formatting to unaligned,  )
```
use repco \t on \pset format unaligned
with t(
uid,
"contentFormat",
title,
summary,
content,
"originalLanguages",
"contentUrl"
) as (
select
uid,
"contentFormat",
title,
summary,
content,
"originalLanguages",
"contentUrl"

from "ContentItem"
LIMIT 1000
)
select json_agg(t) from t \g /tmp/ContentItemDump1000_formated.json
```
Copy file
```
docker cp db:/tmp/ContentItemDump1000_formated.json ./
```

... have fun

refs:
https://www.enterprisedb.com/blog/postgresql-vacuum-and-analyze-best-practice-tips
https://www.datadoghq.com/blog/postgresql-vacuum-monitoring/
http://tuning.postgresql.de/postgresql_vacuum
https://hashrocket.com/blog/posts/create-quick-json-data-dumps-from-postgresql
