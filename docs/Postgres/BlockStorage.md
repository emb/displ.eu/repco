### ipld related

The Block storage is comsuming Tons of space.
Specialy by removing and added Repos it looks alike that data is stored that is
obsolete.

**Space to save findings and Statements**

The problem is ( see Complete Scheme.png) that postgres relations are not
set deep enough. That leads to the situation while adding and deleting datasources, or incomplete ingests unreferenced block remain in the database.

Most of these relation are commented out in the prisma schema. And this may need deeper inspection if any and which may improve this behaviour.

**A delete Statement that can be tuned via LIMIT to delete unreferenced Blocks **,
its meant to be run as often until the block table is cleaned from unreferenced data.

```
DELETE FROM public."Block" WHERE cid IN (
		
SELECT a.cid
--,b."rootCid", c."commitCid", d."parent",e.cid,f."contentCid",g."revisionCid"
FROM public."Block" as a

LEFT JOIN public."Commit" AS b
                        ON        a.cid = b."rootCid"
                LEFT JOIN public."Commit" AS c
                ON        a.cid = c."commitCid"
                LEFT JOIN public."Commit" AS d
                ON        a.cid = d."parent"
                LEFT JOIN public."Ucans" AS e
                ON        a.cid = e.cid
                LEFT JOIN public."Revision" AS f
                ON        a.cid = f."contentCid"
                LEFT JOIN public."Revision" AS g
                ON        a.cid = g."revisionCid"
WHERE 
b."rootCid" IS NULL AND c."commitCid" IS NULL AND d."parent" IS NULL  AND e."cid" IS NULL  AND f."contentCid" IS NULL AND g."revisionCid" IS NULL
--b."rootCid" IS NOT NULL OR c."commitCid" IS NOT NULL OR d."parent" IS NOT NULL  OR e."cid" IS NOT NULL  OR f."contentCid" IS NOT NULL OR g."revisionCid" IS NOT NULL
LIMIT 1
)


```

the base of the above delete

```
SELECT a.cid,b."rootCid", c."commitCid", d."parent",e.cid,f."contentCid",g."revisionCid"
FROM public."Block" as a

LEFT JOIN public."Commit" AS b
                        ON        a.cid = b."rootCid"
                LEFT JOIN public."Commit" AS c
                ON        a.cid = c."commitCid"
                LEFT JOIN public."Commit" AS d
                ON        a.cid = d."parent"
                LEFT JOIN public."Ucans" AS e
                ON        a.cid = e.cid
                LEFT JOIN public."Revision" AS f
                ON        a.cid = f."contentCid"
                LEFT JOIN public."Revision" AS g
                ON        a.cid = g."revisionCid"
WHERE 
b."rootCid" IS NULL AND c."commitCid" IS NULL AND d."parent" IS NULL  AND e."cid" IS NULL  AND f."contentCid" IS NULL AND g."revisionCid" IS NULL
--b."rootCid" IS NOT NULL OR c."commitCid" IS NOT NULL OR d."parent" IS NOT NULL  OR e."cid" IS NOT NULL  OR f."contentCid" IS NOT NULL OR g."revisionCid" IS NOT NULL
```

Find Ucans with a block reference

```
SELECT a.cid,u.cid FROM public."Ucans"  AS u
LEFT JOIN public."Block" AS a
ON        a.cid = u.cid
ORDER BY a.cid ASC 
```

Find Commit with a Block reference

```
SELECT commit."rootCid", commit."commitCid", commit."parent", a.cid
FROM public."Commit" as commit

LEFT JOIN public."Block" AS a
             ON        (
			a.cid = commit."rootCid"
			OR 
			a.cid = commit."commitCid"
			OR 
			a.cid = commit."parent"
			)
```

**Another Possible solution for the block problem**

add a coloumn noreference, and update it with "results.
After that everything with 'noreference' can be truncated

UPDATE public."Block"
SET noreference = 'noreference'

WHERE public."Block".cid IN (
SELECT cid
FROM public."Block" as a
LEFT JOIN public."Commit" AS b
ON        a.cid = b."rootCid"
LEFT JOIN public."Commit" AS c
ON        a.cid = c."commitCid"
LEFT JOIN public."Commit" AS d
ON        a.cid = d."parent"
WHERE
b."rootCid" IS NULL AND c."commitCid" IS NULL  AND d."parent" IS NULL

)

UPDATE public."Block"
SET noreference = 'root'

WHERE public."Block".cid IN (
SELECT cid
FROM public."Block" as a
LEFT JOIN public."Commit" AS b
ON        a.cid = b."rootCid"
LEFT JOIN public."Commit" AS c
ON        a.cid = c."commitCid"
LEFT JOIN public."Commit" AS d
ON        a.cid = d."parent"
WHERE
b."rootCid" IS NOT NULL AND c."commitCid" IS NULL  AND d."parent" IS NULL

)

UPDATE public."Block"
SET noreference = 'commit'

WHERE public."Block".cid IN (
SELECT cid
FROM public."Block" as a
LEFT JOIN public."Commit" AS b
ON        a.cid = b."rootCid"
LEFT JOIN public."Commit" AS c
ON        a.cid = c."commitCid"
LEFT JOIN public."Commit" AS d
ON        a.cid = d."parent"
WHERE
b."rootCid" IS NULL AND c."commitCid" IS NOT NULL  AND d."parent" IS NULL

)

UPDATE public."Block"
SET noreference = 'parent'

WHERE public."Block".cid IN (
SELECT cid
FROM public."Block" as a
LEFT JOIN public."Commit" AS b
ON        a.cid = b."rootCid"
LEFT JOIN public."Commit" AS c
ON        a.cid = c."commitCid"
LEFT JOIN public."Commit" AS d
ON        a.cid = d."parent"
WHERE
b."rootCid" IS NULL AND c."commitCid" IS  NULL  AND d."parent" IS NOT NULL

)
