** Full Vacuum**

```

psql -U repco -t -A -d "repco_livedump2" -c "select format('vacuum FULL %I.%I;', n.nspname::varchar, t.relname::varchar) FROM pg_class t JOIN pg_namespace n ON n.oid = t.relnamespace WHERE t.relkind = 'r' and n.nspname::varchar = 'public' order by 1" | psql -U repco -d "repco_livedump2"

repco_livedump2 > change to correct database!

```


