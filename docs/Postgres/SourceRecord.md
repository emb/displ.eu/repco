**Fact finding**
Concept

```
SELECT COL1,COL2,COL3,COL4, COUNT(*) as DUP_CNT
FROM TABLE_NAME
GROUP BY COL1,COL2,COL3,COL4
HAVING COUNT(*) > 1;
```

---

**Save longrun SQL Results to a File**
- screen
-- S: docker exec -it ... bash
-- container: psql -u recpo

Run long queries

```
\copy (
select "contentType", meta, "dataSourceUid", "containedEntityUris", "sourceType", "sourceUri" from (
    select * , 
    count(*) over (partition by "contentType", body, meta, "dataSourceUid", "containedEntityUris", "sourceType", "sourceUri" ) cnt
    from public."SourceRecord"
) t1 where cnt > 1

) to '/tmp/countSourceRecordWithpartitions.csv' with csv header
```



-- > DUPCOUNT_type1.csv ~ 8200 Rows with some over 8000 COUNTS
runtime ~ 30m

```
SELECT "sourceType","sourceUri","containedEntityUris", COUNT(*) as DUP_CNT
FROM public."SourceRecord"
GROUP BY "sourceType","sourceUri","containedEntityUris"
HAVING COUNT(*) > 1;
-- Gegenprobe: HAVING COUNT(*) == 1;
-- Gegenprobe: HAVING COUNT(*) >= 1;
```

-- Style 2 runtime +30m ...  not working

```
select 
cnt,"sourceType","sourceUri","containedEntityUris"
--*
 from (
    select * , 
    count(*) over (partition by "sourceType","sourceUri","containedEntityUris") cnt
    from public."SourceRecord"
) 
t1 where cnt > 1
-- Gegenprobe:  t1 where cnt == 1
-- Gegenprobe:  t1 where cnt >= 1
```

or with cnt:

```
select cnt,uid,"sourceType","sourceUri","containedEntityUris" from (
    select * , 
    count(*) over (partition by "sourceType","sourceUri","containedEntityUris") cnt
    from public."SourceRecord"
) t1 where cnt > 1

```

-- Find a records (containedEntityUris from above result)
SELECT *uid*  from public."SourceRecord" WHERE  "containedEntityUris" = '{repco:activityPub:e:account:m.akyel_eurozine.com_channel@displayeurope.video}'

**Runtime tests*


| date  | Q1  | Q2 | Q3(over reduced to 3cols) |
| ------- | ----- | ---- | --------------------------- |
| 29.11 | 11m |  23m  |                           |
|       |     |    |                           |
|       |     |    |                           |





**Propoasal 4 a delete statement**

--DELETE FROM public."SourceRecord"
--WHERE uid IN (

	
SELECT 
    uid
FROM public."SourceRecord"
GROUP BY "contentType", body, meta, "dataSourceUid", "containedEntityUris", "sourceType", "sourceUri"
HAVING COUNT(*) > 1;
    LIMIT 100

--);

```
--DELETE FROM public."SourceRecord"
--WHERE uid IN (

	SELECT uid FROM (
			SELECT uid,
			 ROW_NUMBER() OVER ( PARTITION BY 
					"contentType", body, meta, "dataSourceUid", "containedEntityUris", "sourceType", "sourceUri"
				ORDER BY timestamp DESC
			) AS row_num
		FROM public."SourceRecord"
	) t
	WHERE t.row_num > 1 
--);
```
Check with:
 select "contentType", meta, "dataSourceUid", "containedEntityUris", "sourceType", "sourceUri" , 
    count(*) over (partition by "contentType", body, meta, "dataSourceUid", "containedEntityUris", "sourceType", "sourceUri" ) cnt
    from public."SourceRecord" 