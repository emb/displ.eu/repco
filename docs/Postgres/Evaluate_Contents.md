*SQL Tests to examine Repco Data*



Find language_codes codes saved in originalLanguages

```
-- All Langeuage Code Combinations found
SELECT DISTINCT (("originalLanguages"->>'language_codes'))::text  FROM public."ContentItem"

-- Only First Languages in that list
SELECT DISTINCT TRIM(BOTH '"'::text FROM ((("originalLanguages"->>'language_codes')::jsonb->0)::text))  FROM public."ContentItem"


-- Debug Language Combinations
SELECT DISTINCT jsonb_array_elements(r::jsonb)::text,r::jsonb->0,r::jsonb->1,r::jsonb->2,r::jsonb->3,r::jsonb FROM (
SELECT DISTINCT ("originalLanguages"->>'language_codes') as r FROM public."ContentItem"
)

-- Receice it a text list
SELECT DISTINCT TRIM(BOTH '"'::text FROM (jsonb_array_elements(r::jsonb)::text)) as l FROM (
SELECT DISTINCT ("originalLanguages"->>'language_codes') as r FROM public."ContentItem"
)
ORDER BY l ASC
```