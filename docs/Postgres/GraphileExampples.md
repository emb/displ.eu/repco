*Example by title - against a testobject in Dev DB with Live data*

```
query MyQuery {
  contentItem(uid: "eayuxi46c2n7qryq2dr3njkrgdy") {
    titleTranslation(lang: "fr")
    de:titleTranslation(lang: "de")
    title
    titleTranslations
    titleString
    originalLanguages
    contentTranslation(lang: "de")
    summaryTranslation(lang: "de")
    subtitleTranslation(lang: "de")
    summaryTranslations
  }
}
```

Explanation ...
titleTranslations: Merges translations with the valuabler data in contentItem 
titleTranslation: Interface to receice contenttranslation data by lang code
DE: Auto trans is not used: data in contentitem is preferred