```
ALTER TABLE "SourceRecord" RENAME TO "SourceRecordArchive";

ALTER TABLE public."SourceRecordArchive"
    RENAME CONSTRAINT "SourceRecord_pkey" TO "SourceRecordArchive_pkey";

ALTER TABLE IF EXISTS public."SourceRecordArchive"
    RENAME CONSTRAINT "SourceRecord_dataSourceUid_fkey" TO "SourceRecordArchive_dataSourceUid_fkey";
	
ALTER INDEX IF EXISTS public."SourceRecord_uid_key"
    RENAME TO "SourceRecordArchive_uid_key";
	
	
INSERT INTO "SourceRecord" (uid, "contentType", body, meta, "dataSourceUid", "containedEntityUris", "sourceType", "sourceUri", timestamp)
SELECT DISTINCT uid, "contentType", body, meta, "dataSourceUid", "containedEntityUris", "sourceType", "sourceUri", timestamp
FROM public."SourceRecordArchive" 
WHERE timestamp >= '2025-01-01';
```