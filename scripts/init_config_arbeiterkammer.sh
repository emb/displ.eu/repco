#!/bin/bash
echo "Configuring repco repos and datasources for Arbeiterkammer Testsetting lokal"

yarn repco repo create arbeiterkammer

yarn repco ds add -r arbeiterkammer repco:datasource:rss '{"endpoint":"https://arbeiterkammer.at/rss/RSS_Presse.xml", "url":"https://www.arbeiterkammer.at","name":"Arbeiterkammer","image":"https://www.arbeiterkammer.at/infopool/aklogo/AKPortal.svg","thumbnail":"https://www.arbeiterkammer.at/infopool/aklogo/AKPortal.svg"}' 


yarn repco repo create momentum

yarn repco ds add -r momentum repco:datasource:rss '{"endpoint":"https://www.momentum-institut.at/momentum-institut-rss.xml", "url":"https://www.momentum-institut.at","name":"Momentum Institut","image":"","thumbnail":""}' 


yarn repco repo create arbeit-wirtschaft

yarn repco ds add -r arbeit-wirtschaft repco:datasource:rss '{"endpoint":"https://www.arbeit-wirtschaft.at/feed/", "url":"https://www.arbeit-wirtschaft.at","name":"Arbeit & Wirtschaft","image":"https://www.arbeit-wirtschaft.at/wp-content/uploads/2021/03/lono-new.png","thumbnail":"https://www.arbeit-wirtschaft.at/wp-content/uploads/2021/03/lono-new.png"}' 



yarn repco repo create forba

yarn repco ds add -r forba repco:datasource:rss '{"endpoint":"https://www.forba.at/feed", "url":"https://www.forba.at","name":"FORBA","image":"https://www.forba.at/wp-content/uploads/2021/02/FORBA_Logo_kurz.svg","thumbnail":"hhttps://www.forba.at/wp-content/uploads/2021/02/FORBA_Logo_kurz.svg"}' 
