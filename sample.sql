--
-- PostgreSQL database dump
--

-- Dumped from database version 17.0 (Debian 17.0-1.pgdg120+1)
-- Dumped by pg_dump version 17.0 (Debian 17.0-1.pgdg120+1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET transaction_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: postgraphile_watch; Type: SCHEMA; Schema: -; Owner: repco
--

CREATE SCHEMA postgraphile_watch;


ALTER SCHEMA postgraphile_watch OWNER TO repco;

--
-- Name: public; Type: SCHEMA; Schema: -; Owner: repco
--

-- *not* creating schema, since initdb creates it


ALTER SCHEMA public OWNER TO repco;

--
-- Name: SCHEMA public; Type: COMMENT; Schema: -; Owner: repco
--

COMMENT ON SCHEMA public IS '';


--
-- Name: AgentType; Type: TYPE; Schema: public; Owner: repco
--

CREATE TYPE public."AgentType" AS ENUM (
    'DATASOURCE',
    'USER'
);


ALTER TYPE public."AgentType" OWNER TO repco;

--
-- Name: ConceptKind; Type: TYPE; Schema: public; Owner: repco
--

CREATE TYPE public."ConceptKind" AS ENUM (
    'TAG',
    'CATEGORY'
);


ALTER TYPE public."ConceptKind" OWNER TO repco;

--
-- Name: ContentGroupingVariant; Type: TYPE; Schema: public; Owner: repco
--

CREATE TYPE public."ContentGroupingVariant" AS ENUM (
    'EPISODIC',
    'SERIAL'
);


ALTER TYPE public."ContentGroupingVariant" OWNER TO repco;

--
-- Name: KeypairScope; Type: TYPE; Schema: public; Owner: repco
--

CREATE TYPE public."KeypairScope" AS ENUM (
    'INSTANCE',
    'REPO'
);


ALTER TYPE public."KeypairScope" OWNER TO repco;

--
-- Name: notify_watchers_ddl(); Type: FUNCTION; Schema: postgraphile_watch; Owner: repco
--

CREATE FUNCTION postgraphile_watch.notify_watchers_ddl() RETURNS event_trigger
    LANGUAGE plpgsql
    AS $$
begin
  perform pg_notify(
    'postgraphile_watch',
    json_build_object(
      'type',
      'ddl',
      'payload',
      (select json_agg(json_build_object('schema', schema_name, 'command', command_tag)) from pg_event_trigger_ddl_commands() as x)
    )::text
  );
end;
$$;


ALTER FUNCTION postgraphile_watch.notify_watchers_ddl() OWNER TO repco;

--
-- Name: notify_watchers_drop(); Type: FUNCTION; Schema: postgraphile_watch; Owner: repco
--

CREATE FUNCTION postgraphile_watch.notify_watchers_drop() RETURNS event_trigger
    LANGUAGE plpgsql
    AS $$
begin
  perform pg_notify(
    'postgraphile_watch',
    json_build_object(
      'type',
      'drop',
      'payload',
      (select json_agg(distinct x.schema_name) from pg_event_trigger_dropped_objects() as x)
    )::text
  );
end;
$$;


ALTER FUNCTION postgraphile_watch.notify_watchers_drop() OWNER TO repco;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: Chapter; Type: TABLE; Schema: public; Owner: repco
--

CREATE TABLE public."Chapter" (
    uid text NOT NULL,
    "revisionId" text NOT NULL,
    start double precision NOT NULL,
    duration double precision NOT NULL,
    type text NOT NULL,
    "mediaAssetUid" text NOT NULL,
    title jsonb NOT NULL
);


ALTER TABLE public."Chapter" OWNER TO repco;

--
-- Name: Chapter_title_string(public."Chapter"); Type: FUNCTION; Schema: public; Owner: repco
--

CREATE FUNCTION public."Chapter_title_string"("Chapter" public."Chapter") RETURNS text
    LANGUAGE sql STABLE
    AS $$
SELECT "Chapter".title::text
$$;


ALTER FUNCTION public."Chapter_title_string"("Chapter" public."Chapter") OWNER TO repco;

--
-- Name: Concept; Type: TABLE; Schema: public; Owner: repco
--

CREATE TABLE public."Concept" (
    uid text NOT NULL,
    "revisionId" text NOT NULL,
    "originNamespace" text,
    "wikidataIdentifier" text,
    "sameAsUid" text,
    "parentUid" text,
    name jsonb DEFAULT '{}'::jsonb NOT NULL,
    summary jsonb,
    description jsonb,
    kind text NOT NULL
);


ALTER TABLE public."Concept" OWNER TO repco;

--
-- Name: Concept_description_string(public."Concept"); Type: FUNCTION; Schema: public; Owner: repco
--

CREATE FUNCTION public."Concept_description_string"("Concept" public."Concept") RETURNS text
    LANGUAGE sql STABLE
    AS $$
SELECT "Concept".description::text
$$;


ALTER FUNCTION public."Concept_description_string"("Concept" public."Concept") OWNER TO repco;

--
-- Name: Concept_name_string(public."Concept"); Type: FUNCTION; Schema: public; Owner: repco
--

CREATE FUNCTION public."Concept_name_string"("Concept" public."Concept") RETURNS text
    LANGUAGE sql STABLE
    AS $$
SELECT "Concept".name::text
$$;


ALTER FUNCTION public."Concept_name_string"("Concept" public."Concept") OWNER TO repco;

--
-- Name: Concept_summary_string(public."Concept"); Type: FUNCTION; Schema: public; Owner: repco
--

CREATE FUNCTION public."Concept_summary_string"("Concept" public."Concept") RETURNS text
    LANGUAGE sql STABLE
    AS $$
SELECT "Concept".summary::text
$$;


ALTER FUNCTION public."Concept_summary_string"("Concept" public."Concept") OWNER TO repco;

--
-- Name: ContentGrouping; Type: TABLE; Schema: public; Owner: repco
--

CREATE TABLE public."ContentGrouping" (
    uid text NOT NULL,
    "revisionId" text NOT NULL,
    "broadcastSchedule" text,
    "groupingType" text NOT NULL,
    "startingDate" timestamp(3) without time zone,
    subtitle text,
    "terminationDate" timestamp(3) without time zone,
    "licenseUid" text,
    variant public."ContentGroupingVariant" NOT NULL,
    description jsonb,
    summary jsonb,
    title jsonb DEFAULT '{}'::jsonb NOT NULL
);


ALTER TABLE public."ContentGrouping" OWNER TO repco;

--
-- Name: ContentGrouping_description_string(public."ContentGrouping"); Type: FUNCTION; Schema: public; Owner: repco
--

CREATE FUNCTION public."ContentGrouping_description_string"("ContentGrouping" public."ContentGrouping") RETURNS text
    LANGUAGE sql STABLE
    AS $$
SELECT "ContentGrouping".description::text
$$;


ALTER FUNCTION public."ContentGrouping_description_string"("ContentGrouping" public."ContentGrouping") OWNER TO repco;

--
-- Name: ContentGrouping_summary_string(public."ContentGrouping"); Type: FUNCTION; Schema: public; Owner: repco
--

CREATE FUNCTION public."ContentGrouping_summary_string"("ContentGrouping" public."ContentGrouping") RETURNS text
    LANGUAGE sql STABLE
    AS $$
SELECT "ContentGrouping".summary::text
$$;


ALTER FUNCTION public."ContentGrouping_summary_string"("ContentGrouping" public."ContentGrouping") OWNER TO repco;

--
-- Name: ContentGrouping_title_string(public."ContentGrouping"); Type: FUNCTION; Schema: public; Owner: repco
--

CREATE FUNCTION public."ContentGrouping_title_string"("ContentGrouping" public."ContentGrouping") RETURNS text
    LANGUAGE sql STABLE
    AS $$
SELECT "ContentGrouping".title::text
$$;


ALTER FUNCTION public."ContentGrouping_title_string"("ContentGrouping" public."ContentGrouping") OWNER TO repco;

--
-- Name: ContentItem; Type: TABLE; Schema: public; Owner: repco
--

CREATE TABLE public."ContentItem" (
    uid text NOT NULL,
    "revisionId" text NOT NULL,
    "pubDate" timestamp(3) without time zone,
    "contentFormat" text NOT NULL,
    "primaryGroupingUid" text,
    "licenseUid" text,
    "publicationServiceUid" text,
    title jsonb DEFAULT '{}'::jsonb NOT NULL,
    summary jsonb DEFAULT '{}'::jsonb,
    content jsonb DEFAULT '{}'::jsonb NOT NULL,
    "originalLanguages" jsonb,
    removed boolean NOT NULL,
    "contentUrl" jsonb NOT NULL,
    subtitle jsonb DEFAULT '{}'::jsonb
);


ALTER TABLE public."ContentItem" OWNER TO repco;

--
-- Name: ContentItem_content_string(public."ContentItem"); Type: FUNCTION; Schema: public; Owner: repco
--

CREATE FUNCTION public."ContentItem_content_string"("ContentItem" public."ContentItem") RETURNS text
    LANGUAGE sql STABLE
    AS $$
SELECT "ContentItem".content::text
$$;


ALTER FUNCTION public."ContentItem_content_string"("ContentItem" public."ContentItem") OWNER TO repco;

--
-- Name: ContentItem_summary_string(public."ContentItem"); Type: FUNCTION; Schema: public; Owner: repco
--

CREATE FUNCTION public."ContentItem_summary_string"("ContentItem" public."ContentItem") RETURNS text
    LANGUAGE sql STABLE
    AS $$
SELECT "ContentItem".summary::text
$$;


ALTER FUNCTION public."ContentItem_summary_string"("ContentItem" public."ContentItem") OWNER TO repco;

--
-- Name: ContentItem_title_string(public."ContentItem"); Type: FUNCTION; Schema: public; Owner: repco
--

CREATE FUNCTION public."ContentItem_title_string"("ContentItem" public."ContentItem") RETURNS text
    LANGUAGE sql STABLE
    AS $$
SELECT "ContentItem".title::text
$$;


ALTER FUNCTION public."ContentItem_title_string"("ContentItem" public."ContentItem") OWNER TO repco;

--
-- Name: MediaAsset; Type: TABLE; Schema: public; Owner: repco
--

CREATE TABLE public."MediaAsset" (
    uid text NOT NULL,
    "revisionId" text NOT NULL,
    duration double precision,
    "mediaType" text NOT NULL,
    "teaserImageUid" text,
    "licenseUid" text,
    title jsonb DEFAULT '{}'::jsonb NOT NULL,
    description jsonb
);


ALTER TABLE public."MediaAsset" OWNER TO repco;

--
-- Name: MediaAsset_description_string(public."MediaAsset"); Type: FUNCTION; Schema: public; Owner: repco
--

CREATE FUNCTION public."MediaAsset_description_string"("MediaAsset" public."MediaAsset") RETURNS text
    LANGUAGE sql STABLE
    AS $$
SELECT "MediaAsset".description::text
$$;


ALTER FUNCTION public."MediaAsset_description_string"("MediaAsset" public."MediaAsset") OWNER TO repco;

--
-- Name: MediaAsset_title_string(public."MediaAsset"); Type: FUNCTION; Schema: public; Owner: repco
--

CREATE FUNCTION public."MediaAsset_title_string"("MediaAsset" public."MediaAsset") RETURNS text
    LANGUAGE sql STABLE
    AS $$
SELECT "MediaAsset".title::text
$$;


ALTER FUNCTION public."MediaAsset_title_string"("MediaAsset" public."MediaAsset") OWNER TO repco;

--
-- Name: PublicationService; Type: TABLE; Schema: public; Owner: repco
--

CREATE TABLE public."PublicationService" (
    uid text NOT NULL,
    "revisionId" text NOT NULL,
    medium text,
    address text NOT NULL,
    "publisherUid" text,
    name jsonb NOT NULL
);


ALTER TABLE public."PublicationService" OWNER TO repco;

--
-- Name: PublicationService_name_string(public."PublicationService"); Type: FUNCTION; Schema: public; Owner: repco
--

CREATE FUNCTION public."PublicationService_name_string"("PublicationService" public."PublicationService") RETURNS text
    LANGUAGE sql STABLE
    AS $$
SELECT "PublicationService".name::text
$$;


ALTER FUNCTION public."PublicationService_name_string"("PublicationService" public."PublicationService") OWNER TO repco;

--
-- Name: get_chapter_by_language(text); Type: FUNCTION; Schema: public; Owner: repco
--

CREATE FUNCTION public.get_chapter_by_language(language_code text) RETURNS TABLE(uid text, revisionid text, start double precision, duration double precision, type text, title text)
    LANGUAGE plpgsql
    AS $$
begin
   return query EXECUTE 'select
   	uid, "Chapter"."revisionId", start, duration, type,
	title#>>''{' || language_code ||',value}'' as title
	from
	"Chapter"
	where title->>'|| quote_literal(language_code) ||' is not null'
     using language_code;
end; 
$$;


ALTER FUNCTION public.get_chapter_by_language(language_code text) OWNER TO repco;

--
-- Name: get_concept_by_language(text); Type: FUNCTION; Schema: public; Owner: repco
--

CREATE FUNCTION public.get_concept_by_language(language_code text) RETURNS TABLE(uid text, revisionid text, originnamespace text, wikidataidentifier text, sameasuid text, parentuid text, kind public."ConceptKind", name text, summary text, description text)
    LANGUAGE plpgsql
    AS $$
begin
   return query EXECUTE 'select
   	uid, "Concept"."revisionId", "PublicationService"."originNamespace", "PublicationService"."wikidataIdentifier",
	"PublicationService"."sameAsUid", "PublicationService"."parentUid", kind,
	name#>>''{' || language_code ||',value}'' as name,
	summary#>>''{' || language_code ||',value}'' as summary,
	description#>>''{' || language_code ||',value}'' as description
	from
	"Concept"
	where name->>'|| quote_literal(language_code) ||' is not null
	or summary->>'|| quote_literal(language_code) ||' is not null
	or description->>'|| quote_literal(language_code) ||' is not null'
     using language_code;
end; 
$$;


ALTER FUNCTION public.get_concept_by_language(language_code text) OWNER TO repco;

--
-- Name: get_content_groupings_by_language(text); Type: FUNCTION; Schema: public; Owner: repco
--

CREATE FUNCTION public.get_content_groupings_by_language(language_code text) RETURNS TABLE(uid text, revisionid text, broadcastschedule text, groupingtype text, startingdate timestamp without time zone, subtitle text, terminationdate timestamp without time zone, variant public."ContentGroupingVariant", licenseuid text, description text, title text, summary text)
    LANGUAGE plpgsql
    AS $$
begin
   return query EXECUTE 'select
   	uid, "ContentGrouping"."revisionId", "ContentGrouping"."broadcastSchedule", "ContentGrouping"."groupingType", "ContentGrouping"."startingDate",
	subtitle, "ContentGrouping"."terminationDate", variant, "ContentGrouping"."licenseUid",
	description#>>''{' || language_code ||',value}'' as description,
	title#>>''{' || language_code ||',value}'' as title,
	summary#>>''{' || language_code ||',value}'' as summary
	from
	"ContentGrouping"
	where title->>'|| quote_literal(language_code) ||' is not null
	or description->>'|| quote_literal(language_code) ||' is not null
	or summary->>'|| quote_literal(language_code) ||' is not null'
     using language_code;
end; 
$$;


ALTER FUNCTION public.get_content_groupings_by_language(language_code text) OWNER TO repco;

--
-- Name: get_content_items_by_language(text); Type: FUNCTION; Schema: public; Owner: repco
--

CREATE FUNCTION public.get_content_items_by_language(language_code text) RETURNS TABLE(uid text, revisionid text, subtitle text, pubdate timestamp without time zone, contentformat text, primarygroupinguid text, licenseuid text, publicationserviceuid text, title text, content text, summary text)
    LANGUAGE plpgsql
    AS $$
begin
   return query EXECUTE 'select
   	uid, "ContentItem"."revisionId", subtitle, "ContentItem"."pubDate", "ContentItem"."contentFormat", 
	"ContentItem"."primaryGroupingUid", "ContentItem"."licenseUid","ContentItem"."publicationServiceUid",
	title#>>''{' || language_code ||',value}'' as title,
	content#>>''{' || language_code ||',value}'' as content,
	summary#>>''{' || language_code ||',value}'' as summary
	from
	"ContentItem"
	where title->>'|| quote_literal(language_code) ||' is not null
	or content->>'|| quote_literal(language_code) ||' is not null
	or summary->>'|| quote_literal(language_code) ||' is not null'
     using language_code;
end; 
$$;


ALTER FUNCTION public.get_content_items_by_language(language_code text) OWNER TO repco;

--
-- Name: get_media_asset_by_language(text); Type: FUNCTION; Schema: public; Owner: repco
--

CREATE FUNCTION public.get_media_asset_by_language(language_code text) RETURNS TABLE(uid text, revisionid text, duration double precision, mediatype text, fileuid text, teaserimageuid text, licenseuid text, title text, description text)
    LANGUAGE plpgsql
    AS $$
begin
   return query EXECUTE 'select
   	uid, "MediaAsset"."revisionId", duration, "MediaAsset"."mediaType", "MediaAsset"."fileUid", 
	"MediaAsset"."teaserImageUid", "MediaAsset"."licenseUid",
	title#>>''{' || language_code ||',value}'' as title,
	description#>>''{' || language_code ||',value}'' as description
	from
	"MediaAsset"
	where title->>'|| quote_literal(language_code) ||' is not null
	or description->>'|| quote_literal(language_code) ||' is not null'
     using language_code;
end; 
$$;


ALTER FUNCTION public.get_media_asset_by_language(language_code text) OWNER TO repco;

--
-- Name: get_publication_service_by_language(text); Type: FUNCTION; Schema: public; Owner: repco
--

CREATE FUNCTION public.get_publication_service_by_language(language_code text) RETURNS TABLE(uid text, revisionid text, medium text, address text, publisheruid text, title text)
    LANGUAGE plpgsql
    AS $$
begin
   return query EXECUTE 'select
   	uid, "PublicationService"."revisionId", medium, address, "PublicationService"."publisherUid",
	name#>>''{' || language_code ||',value}'' as name
	from
	"PublicationService"
	where name->>'|| quote_literal(language_code) ||' is not null'
     using language_code;
end; 
$$;


ALTER FUNCTION public.get_publication_service_by_language(language_code text) OWNER TO repco;

--
-- Name: Agent; Type: TABLE; Schema: public; Owner: repco
--

CREATE TABLE public."Agent" (
    did text NOT NULL,
    type public."AgentType"
);


ALTER TABLE public."Agent" OWNER TO repco;

--
-- Name: ApActivities; Type: TABLE; Schema: public; Owner: repco
--

CREATE TABLE public."ApActivities" (
    id text NOT NULL,
    "actorId" text NOT NULL,
    type text NOT NULL,
    "objectId" text NOT NULL,
    "receivedAt" timestamp(3) without time zone NOT NULL,
    details jsonb NOT NULL,
    "attributedTo" text[]
);


ALTER TABLE public."ApActivities" OWNER TO repco;

--
-- Name: ApFollows; Type: TABLE; Schema: public; Owner: repco
--

CREATE TABLE public."ApFollows" (
    "localName" text NOT NULL,
    "remoteId" text NOT NULL
);


ALTER TABLE public."ApFollows" OWNER TO repco;

--
-- Name: ApLocalActor; Type: TABLE; Schema: public; Owner: repco
--

CREATE TABLE public."ApLocalActor" (
    name text NOT NULL,
    keypair jsonb NOT NULL
);


ALTER TABLE public."ApLocalActor" OWNER TO repco;

--
-- Name: Block; Type: TABLE; Schema: public; Owner: repco
--

CREATE TABLE public."Block" (
    cid text NOT NULL,
    bytes bytea NOT NULL
);


ALTER TABLE public."Block" OWNER TO repco;

--
-- Name: BroadcastEvent; Type: TABLE; Schema: public; Owner: repco
--

CREATE TABLE public."BroadcastEvent" (
    uid text NOT NULL,
    "revisionId" text NOT NULL,
    start double precision NOT NULL,
    duration double precision NOT NULL,
    "broadcastServiceUid" text NOT NULL,
    "contentItemUid" text NOT NULL
);


ALTER TABLE public."BroadcastEvent" OWNER TO repco;

--
-- Name: Commit; Type: TABLE; Schema: public; Owner: repco
--

CREATE TABLE public."Commit" (
    "rootCid" text NOT NULL,
    "commitCid" text NOT NULL,
    "repoDid" text NOT NULL,
    "agentDid" text NOT NULL,
    "timestamp" timestamp(3) without time zone NOT NULL,
    parent text
);


ALTER TABLE public."Commit" OWNER TO repco;

--
-- Name: ContentItemMachineTranslation; Type: TABLE; Schema: public; Owner: repco
--

CREATE TABLE public."ContentItemMachineTranslation" (
    uid text NOT NULL,
    languages jsonb DEFAULT '{}'::jsonb NOT NULL,
    "originalLanguages" jsonb DEFAULT '{}'::jsonb NOT NULL,
    title jsonb DEFAULT '{}'::jsonb NOT NULL,
    subtitle jsonb DEFAULT '{}'::jsonb,
    summary jsonb DEFAULT '{}'::jsonb,
    "summaryCleaned" jsonb DEFAULT '{}'::jsonb,
    content jsonb DEFAULT '{}'::jsonb NOT NULL,
    "contentCleaned" jsonb DEFAULT '{}'::jsonb,
    serviceresult text NOT NULL,
    engine text NOT NULL,
    "receivedAt" timestamp(3) without time zone NOT NULL,
    "revisionId" text NOT NULL,
    "contentItemUid" text NOT NULL
);


ALTER TABLE public."ContentItemMachineTranslation" OWNER TO repco;

--
-- Name: Contribution; Type: TABLE; Schema: public; Owner: repco
--

CREATE TABLE public."Contribution" (
    uid text NOT NULL,
    "revisionId" text NOT NULL,
    role text NOT NULL
);


ALTER TABLE public."Contribution" OWNER TO repco;

--
-- Name: Contributor; Type: TABLE; Schema: public; Owner: repco
--

CREATE TABLE public."Contributor" (
    uid text NOT NULL,
    "revisionId" text NOT NULL,
    name text NOT NULL,
    "personOrOrganization" text NOT NULL,
    "contactInformation" text NOT NULL,
    "profilePictureUid" text
);


ALTER TABLE public."Contributor" OWNER TO repco;

--
-- Name: DataSource; Type: TABLE; Schema: public; Owner: repco
--

CREATE TABLE public."DataSource" (
    uid text NOT NULL,
    "pluginUid" text NOT NULL,
    config jsonb,
    cursor text,
    active boolean,
    "repoDid" text NOT NULL
);


ALTER TABLE public."DataSource" OWNER TO repco;

--
-- Name: Entity; Type: TABLE; Schema: public; Owner: repco
--

CREATE TABLE public."Entity" (
    uid text NOT NULL,
    "revisionId" text NOT NULL,
    type text NOT NULL
);


ALTER TABLE public."Entity" OWNER TO repco;

--
-- Name: FailedDatasourceFetches; Type: TABLE; Schema: public; Owner: repco
--

CREATE TABLE public."FailedDatasourceFetches" (
    uri text NOT NULL,
    "datasourceUid" text NOT NULL,
    "timestamp" timestamp(3) without time zone NOT NULL,
    "errorMessage" text NOT NULL,
    "errorDetails" jsonb
);


ALTER TABLE public."FailedDatasourceFetches" OWNER TO repco;

--
-- Name: File; Type: TABLE; Schema: public; Owner: repco
--

CREATE TABLE public."File" (
    uid text NOT NULL,
    "revisionId" text NOT NULL,
    "contentUrl" text NOT NULL,
    "contentSize" integer,
    cid text,
    "mimeType" text,
    duration double precision,
    codec text,
    bitrate integer,
    resolution text,
    "additionalMetadata" text
);


ALTER TABLE public."File" OWNER TO repco;

--
-- Name: IngestError; Type: TABLE; Schema: public; Owner: repco
--

CREATE TABLE public."IngestError" (
    id text NOT NULL,
    "repoDid" text NOT NULL,
    "datasourceUid" text NOT NULL,
    kind text NOT NULL,
    cursor text,
    "sourceRecordId" text,
    "timestamp" timestamp(3) without time zone NOT NULL,
    "errorMessage" text NOT NULL,
    "errorDetails" jsonb
);


ALTER TABLE public."IngestError" OWNER TO repco;

--
-- Name: Keypair; Type: TABLE; Schema: public; Owner: repco
--

CREATE TABLE public."Keypair" (
    did text NOT NULL,
    name text,
    secret text NOT NULL,
    scope public."KeypairScope" NOT NULL
);


ALTER TABLE public."Keypair" OWNER TO repco;

--
-- Name: License; Type: TABLE; Schema: public; Owner: repco
--

CREATE TABLE public."License" (
    uid text NOT NULL,
    "revisionId" text NOT NULL,
    name text NOT NULL
);


ALTER TABLE public."License" OWNER TO repco;

--
-- Name: Metadata; Type: TABLE; Schema: public; Owner: repco
--

CREATE TABLE public."Metadata" (
    uid text NOT NULL,
    "revisionId" text NOT NULL,
    namespace text NOT NULL,
    content jsonb NOT NULL,
    "targetUid" text NOT NULL
);


ALTER TABLE public."Metadata" OWNER TO repco;

--
-- Name: Repo; Type: TABLE; Schema: public; Owner: repco
--

CREATE TABLE public."Repo" (
    did text NOT NULL,
    name text,
    head text,
    tail text,
    gateways text[]
);


ALTER TABLE public."Repo" OWNER TO repco;

--
-- Name: Revision; Type: TABLE; Schema: public; Owner: repco
--

CREATE TABLE public."Revision" (
    id text NOT NULL,
    "prevRevisionId" text,
    uid text NOT NULL,
    "repoDid" text NOT NULL,
    "agentDid" text NOT NULL,
    "entityType" text NOT NULL,
    "dateModified" timestamp(3) without time zone NOT NULL,
    "dateCreated" timestamp(3) without time zone NOT NULL,
    "isDeleted" boolean DEFAULT false NOT NULL,
    "entityUris" text[],
    "revisionUris" text[],
    "contentCid" text NOT NULL,
    "revisionCid" text NOT NULL,
    "derivedFromUid" text,
    languages text DEFAULT ''::text NOT NULL
);


ALTER TABLE public."Revision" OWNER TO repco;

--
-- Name: SourceRecord; Type: TABLE; Schema: public; Owner: repco
--

CREATE TABLE public."SourceRecord" (
    uid text NOT NULL,
    "contentType" text NOT NULL,
    body text NOT NULL,
    meta jsonb,
    "dataSourceUid" text,
    "containedEntityUris" text[],
    "sourceType" text NOT NULL,
    "sourceUri" text NOT NULL,
    "timestamp" timestamp(3) without time zone NOT NULL
);


ALTER TABLE public."SourceRecord" OWNER TO repco;

--
-- Name: Transcript; Type: TABLE; Schema: public; Owner: repco
--

CREATE TABLE public."Transcript" (
    uid text NOT NULL,
    language text NOT NULL,
    text text NOT NULL,
    engine text NOT NULL,
    "mediaAssetUid" text NOT NULL,
    author text NOT NULL,
    license text NOT NULL,
    "revisionId" text NOT NULL,
    "subtitleUrl" text NOT NULL
);


ALTER TABLE public."Transcript" OWNER TO repco;

--
-- Name: Ucans; Type: TABLE; Schema: public; Owner: repco
--

CREATE TABLE public."Ucans" (
    scope text NOT NULL,
    audience text NOT NULL,
    resource text NOT NULL,
    token text NOT NULL,
    cid text NOT NULL
);


ALTER TABLE public."Ucans" OWNER TO repco;

--
-- Name: User; Type: TABLE; Schema: public; Owner: repco
--

CREATE TABLE public."User" (
    did text NOT NULL,
    name text NOT NULL
);


ALTER TABLE public."User" OWNER TO repco;

--
-- Name: _ConceptToContentItem; Type: TABLE; Schema: public; Owner: repco
--

CREATE TABLE public."_ConceptToContentItem" (
    "A" text NOT NULL,
    "B" text NOT NULL
);


ALTER TABLE public."_ConceptToContentItem" OWNER TO repco;

--
-- Name: _ConceptToMediaAsset; Type: TABLE; Schema: public; Owner: repco
--

CREATE TABLE public."_ConceptToMediaAsset" (
    "A" text NOT NULL,
    "B" text NOT NULL
);


ALTER TABLE public."_ConceptToMediaAsset" OWNER TO repco;

--
-- Name: _ContentGroupingToContentItem; Type: TABLE; Schema: public; Owner: repco
--

CREATE TABLE public."_ContentGroupingToContentItem" (
    "A" text NOT NULL,
    "B" text NOT NULL
);


ALTER TABLE public."_ContentGroupingToContentItem" OWNER TO repco;

--
-- Name: _ContentItemToContribution; Type: TABLE; Schema: public; Owner: repco
--

CREATE TABLE public."_ContentItemToContribution" (
    "A" text NOT NULL,
    "B" text NOT NULL
);


ALTER TABLE public."_ContentItemToContribution" OWNER TO repco;

--
-- Name: _ContentItemToMediaAsset; Type: TABLE; Schema: public; Owner: repco
--

CREATE TABLE public."_ContentItemToMediaAsset" (
    "A" text NOT NULL,
    "B" text NOT NULL
);


ALTER TABLE public."_ContentItemToMediaAsset" OWNER TO repco;

--
-- Name: _ContributionToContributor; Type: TABLE; Schema: public; Owner: repco
--

CREATE TABLE public."_ContributionToContributor" (
    "A" text NOT NULL,
    "B" text NOT NULL
);


ALTER TABLE public."_ContributionToContributor" OWNER TO repco;

--
-- Name: _ContributionToMediaAsset; Type: TABLE; Schema: public; Owner: repco
--

CREATE TABLE public."_ContributionToMediaAsset" (
    "A" text NOT NULL,
    "B" text NOT NULL
);


ALTER TABLE public."_ContributionToMediaAsset" OWNER TO repco;

--
-- Name: _FileToMediaAsset; Type: TABLE; Schema: public; Owner: repco
--

CREATE TABLE public."_FileToMediaAsset" (
    "A" text NOT NULL,
    "B" text NOT NULL
);


ALTER TABLE public."_FileToMediaAsset" OWNER TO repco;

--
-- Name: _RevisionToCommit; Type: TABLE; Schema: public; Owner: repco
--

CREATE TABLE public."_RevisionToCommit" (
    "A" text NOT NULL,
    "B" text NOT NULL
);


ALTER TABLE public."_RevisionToCommit" OWNER TO repco;

--
-- Name: _prisma_migrations; Type: TABLE; Schema: public; Owner: repco
--

CREATE TABLE public._prisma_migrations (
    id character varying(36) NOT NULL,
    checksum character varying(64) NOT NULL,
    finished_at timestamp with time zone,
    migration_name character varying(255) NOT NULL,
    logs text,
    rolled_back_at timestamp with time zone,
    started_at timestamp with time zone DEFAULT now() NOT NULL,
    applied_steps_count integer DEFAULT 0 NOT NULL
);


ALTER TABLE public._prisma_migrations OWNER TO repco;

--
-- Name: Agent Agent_pkey; Type: CONSTRAINT; Schema: public; Owner: repco
--

ALTER TABLE ONLY public."Agent"
    ADD CONSTRAINT "Agent_pkey" PRIMARY KEY (did);


--
-- Name: Block Block_pkey; Type: CONSTRAINT; Schema: public; Owner: repco
--

ALTER TABLE ONLY public."Block"
    ADD CONSTRAINT "Block_pkey" PRIMARY KEY (cid);


--
-- Name: BroadcastEvent BroadcastEvent_pkey; Type: CONSTRAINT; Schema: public; Owner: repco
--

ALTER TABLE ONLY public."BroadcastEvent"
    ADD CONSTRAINT "BroadcastEvent_pkey" PRIMARY KEY (uid);


--
-- Name: Chapter Chapter_pkey; Type: CONSTRAINT; Schema: public; Owner: repco
--

ALTER TABLE ONLY public."Chapter"
    ADD CONSTRAINT "Chapter_pkey" PRIMARY KEY (uid);


--
-- Name: Commit Commit_pkey; Type: CONSTRAINT; Schema: public; Owner: repco
--

ALTER TABLE ONLY public."Commit"
    ADD CONSTRAINT "Commit_pkey" PRIMARY KEY ("rootCid");


--
-- Name: Concept Concept_pkey; Type: CONSTRAINT; Schema: public; Owner: repco
--

ALTER TABLE ONLY public."Concept"
    ADD CONSTRAINT "Concept_pkey" PRIMARY KEY (uid);


--
-- Name: ContentGrouping ContentGrouping_pkey; Type: CONSTRAINT; Schema: public; Owner: repco
--

ALTER TABLE ONLY public."ContentGrouping"
    ADD CONSTRAINT "ContentGrouping_pkey" PRIMARY KEY (uid);


--
-- Name: ContentItemMachineTranslation ContentItemMachineTranslation_pkey; Type: CONSTRAINT; Schema: public; Owner: repco
--

ALTER TABLE ONLY public."ContentItemMachineTranslation"
    ADD CONSTRAINT "ContentItemMachineTranslation_pkey" PRIMARY KEY (uid);


--
-- Name: ContentItem ContentItem_pkey; Type: CONSTRAINT; Schema: public; Owner: repco
--

ALTER TABLE ONLY public."ContentItem"
    ADD CONSTRAINT "ContentItem_pkey" PRIMARY KEY (uid);


--
-- Name: Contribution Contribution_pkey; Type: CONSTRAINT; Schema: public; Owner: repco
--

ALTER TABLE ONLY public."Contribution"
    ADD CONSTRAINT "Contribution_pkey" PRIMARY KEY (uid);


--
-- Name: Contributor Contributor_pkey; Type: CONSTRAINT; Schema: public; Owner: repco
--

ALTER TABLE ONLY public."Contributor"
    ADD CONSTRAINT "Contributor_pkey" PRIMARY KEY (uid);


--
-- Name: DataSource DataSource_pkey; Type: CONSTRAINT; Schema: public; Owner: repco
--

ALTER TABLE ONLY public."DataSource"
    ADD CONSTRAINT "DataSource_pkey" PRIMARY KEY (uid);


--
-- Name: Entity Entity_pkey; Type: CONSTRAINT; Schema: public; Owner: repco
--

ALTER TABLE ONLY public."Entity"
    ADD CONSTRAINT "Entity_pkey" PRIMARY KEY (uid);


--
-- Name: FailedDatasourceFetches FailedDatasourceFetches_pkey; Type: CONSTRAINT; Schema: public; Owner: repco
--

ALTER TABLE ONLY public."FailedDatasourceFetches"
    ADD CONSTRAINT "FailedDatasourceFetches_pkey" PRIMARY KEY (uri, "datasourceUid");


--
-- Name: File File_pkey; Type: CONSTRAINT; Schema: public; Owner: repco
--

ALTER TABLE ONLY public."File"
    ADD CONSTRAINT "File_pkey" PRIMARY KEY (uid);


--
-- Name: IngestError IngestError_pkey; Type: CONSTRAINT; Schema: public; Owner: repco
--

ALTER TABLE ONLY public."IngestError"
    ADD CONSTRAINT "IngestError_pkey" PRIMARY KEY (id);


--
-- Name: Keypair Keypair_pkey; Type: CONSTRAINT; Schema: public; Owner: repco
--

ALTER TABLE ONLY public."Keypair"
    ADD CONSTRAINT "Keypair_pkey" PRIMARY KEY (did);


--
-- Name: License License_pkey; Type: CONSTRAINT; Schema: public; Owner: repco
--

ALTER TABLE ONLY public."License"
    ADD CONSTRAINT "License_pkey" PRIMARY KEY (uid);


--
-- Name: MediaAsset MediaAsset_pkey; Type: CONSTRAINT; Schema: public; Owner: repco
--

ALTER TABLE ONLY public."MediaAsset"
    ADD CONSTRAINT "MediaAsset_pkey" PRIMARY KEY (uid);


--
-- Name: PublicationService PublicationService_pkey; Type: CONSTRAINT; Schema: public; Owner: repco
--

ALTER TABLE ONLY public."PublicationService"
    ADD CONSTRAINT "PublicationService_pkey" PRIMARY KEY (uid);


--
-- Name: Repo Repo_pkey; Type: CONSTRAINT; Schema: public; Owner: repco
--

ALTER TABLE ONLY public."Repo"
    ADD CONSTRAINT "Repo_pkey" PRIMARY KEY (did);


--
-- Name: Revision Revision_pkey; Type: CONSTRAINT; Schema: public; Owner: repco
--

ALTER TABLE ONLY public."Revision"
    ADD CONSTRAINT "Revision_pkey" PRIMARY KEY (id);


--
-- Name: SourceRecord SourceRecord_pkey; Type: CONSTRAINT; Schema: public; Owner: repco
--

ALTER TABLE ONLY public."SourceRecord"
    ADD CONSTRAINT "SourceRecord_pkey" PRIMARY KEY (uid);


--
-- Name: Transcript Transcript_pkey; Type: CONSTRAINT; Schema: public; Owner: repco
--

ALTER TABLE ONLY public."Transcript"
    ADD CONSTRAINT "Transcript_pkey" PRIMARY KEY (uid);


--
-- Name: Ucans Ucans_pkey; Type: CONSTRAINT; Schema: public; Owner: repco
--

ALTER TABLE ONLY public."Ucans"
    ADD CONSTRAINT "Ucans_pkey" PRIMARY KEY (cid);


--
-- Name: User User_pkey; Type: CONSTRAINT; Schema: public; Owner: repco
--

ALTER TABLE ONLY public."User"
    ADD CONSTRAINT "User_pkey" PRIMARY KEY (did);


--
-- Name: _prisma_migrations _prisma_migrations_pkey; Type: CONSTRAINT; Schema: public; Owner: repco
--

ALTER TABLE ONLY public._prisma_migrations
    ADD CONSTRAINT _prisma_migrations_pkey PRIMARY KEY (id);


--
-- Name: Agent_did_key; Type: INDEX; Schema: public; Owner: repco
--

CREATE UNIQUE INDEX "Agent_did_key" ON public."Agent" USING btree (did);


--
-- Name: ApActivities_id_key; Type: INDEX; Schema: public; Owner: repco
--

CREATE UNIQUE INDEX "ApActivities_id_key" ON public."ApActivities" USING btree (id);


--
-- Name: ApFollows_localName_remoteId_key; Type: INDEX; Schema: public; Owner: repco
--

CREATE UNIQUE INDEX "ApFollows_localName_remoteId_key" ON public."ApFollows" USING btree ("localName", "remoteId");


--
-- Name: ApLocalActor_name_key; Type: INDEX; Schema: public; Owner: repco
--

CREATE UNIQUE INDEX "ApLocalActor_name_key" ON public."ApLocalActor" USING btree (name);


--
-- Name: Block_cid_key; Type: INDEX; Schema: public; Owner: repco
--

CREATE UNIQUE INDEX "Block_cid_key" ON public."Block" USING btree (cid);


--
-- Name: BroadcastEvent_revisionId_key; Type: INDEX; Schema: public; Owner: repco
--

CREATE UNIQUE INDEX "BroadcastEvent_revisionId_key" ON public."BroadcastEvent" USING btree ("revisionId");


--
-- Name: BroadcastEvent_uid_key; Type: INDEX; Schema: public; Owner: repco
--

CREATE UNIQUE INDEX "BroadcastEvent_uid_key" ON public."BroadcastEvent" USING btree (uid);


--
-- Name: Chapter_revisionId_key; Type: INDEX; Schema: public; Owner: repco
--

CREATE UNIQUE INDEX "Chapter_revisionId_key" ON public."Chapter" USING btree ("revisionId");


--
-- Name: Chapter_uid_key; Type: INDEX; Schema: public; Owner: repco
--

CREATE UNIQUE INDEX "Chapter_uid_key" ON public."Chapter" USING btree (uid);


--
-- Name: Commit_commitCid_key; Type: INDEX; Schema: public; Owner: repco
--

CREATE UNIQUE INDEX "Commit_commitCid_key" ON public."Commit" USING btree ("commitCid");


--
-- Name: Commit_parent_key; Type: INDEX; Schema: public; Owner: repco
--

CREATE UNIQUE INDEX "Commit_parent_key" ON public."Commit" USING btree (parent);


--
-- Name: Commit_rootCid_key; Type: INDEX; Schema: public; Owner: repco
--

CREATE UNIQUE INDEX "Commit_rootCid_key" ON public."Commit" USING btree ("rootCid");


--
-- Name: Concept_revisionId_key; Type: INDEX; Schema: public; Owner: repco
--

CREATE UNIQUE INDEX "Concept_revisionId_key" ON public."Concept" USING btree ("revisionId");


--
-- Name: Concept_sameAsUid_key; Type: INDEX; Schema: public; Owner: repco
--

CREATE UNIQUE INDEX "Concept_sameAsUid_key" ON public."Concept" USING btree ("sameAsUid");


--
-- Name: Concept_uid_key; Type: INDEX; Schema: public; Owner: repco
--

CREATE UNIQUE INDEX "Concept_uid_key" ON public."Concept" USING btree (uid);


--
-- Name: ContentGrouping_revisionId_key; Type: INDEX; Schema: public; Owner: repco
--

CREATE UNIQUE INDEX "ContentGrouping_revisionId_key" ON public."ContentGrouping" USING btree ("revisionId");


--
-- Name: ContentGrouping_uid_key; Type: INDEX; Schema: public; Owner: repco
--

CREATE UNIQUE INDEX "ContentGrouping_uid_key" ON public."ContentGrouping" USING btree (uid);


--
-- Name: ContentItemMachineTranslation_contentItemUid_key; Type: INDEX; Schema: public; Owner: repco
--

CREATE UNIQUE INDEX "ContentItemMachineTranslation_contentItemUid_key" ON public."ContentItemMachineTranslation" USING btree ("contentItemUid");


--
-- Name: ContentItemMachineTranslation_revisionId_key; Type: INDEX; Schema: public; Owner: repco
--

CREATE UNIQUE INDEX "ContentItemMachineTranslation_revisionId_key" ON public."ContentItemMachineTranslation" USING btree ("revisionId");


--
-- Name: ContentItemMachineTranslation_uid_key; Type: INDEX; Schema: public; Owner: repco
--

CREATE UNIQUE INDEX "ContentItemMachineTranslation_uid_key" ON public."ContentItemMachineTranslation" USING btree (uid);


--
-- Name: ContentItem_revisionId_key; Type: INDEX; Schema: public; Owner: repco
--

CREATE UNIQUE INDEX "ContentItem_revisionId_key" ON public."ContentItem" USING btree ("revisionId");


--
-- Name: ContentItem_uid_key; Type: INDEX; Schema: public; Owner: repco
--

CREATE UNIQUE INDEX "ContentItem_uid_key" ON public."ContentItem" USING btree (uid);


--
-- Name: Contribution_revisionId_key; Type: INDEX; Schema: public; Owner: repco
--

CREATE UNIQUE INDEX "Contribution_revisionId_key" ON public."Contribution" USING btree ("revisionId");


--
-- Name: Contribution_uid_key; Type: INDEX; Schema: public; Owner: repco
--

CREATE UNIQUE INDEX "Contribution_uid_key" ON public."Contribution" USING btree (uid);


--
-- Name: Contributor_revisionId_key; Type: INDEX; Schema: public; Owner: repco
--

CREATE UNIQUE INDEX "Contributor_revisionId_key" ON public."Contributor" USING btree ("revisionId");


--
-- Name: Contributor_uid_key; Type: INDEX; Schema: public; Owner: repco
--

CREATE UNIQUE INDEX "Contributor_uid_key" ON public."Contributor" USING btree (uid);


--
-- Name: DataSource_uid_key; Type: INDEX; Schema: public; Owner: repco
--

CREATE UNIQUE INDEX "DataSource_uid_key" ON public."DataSource" USING btree (uid);


--
-- Name: Entity_revisionId_key; Type: INDEX; Schema: public; Owner: repco
--

CREATE UNIQUE INDEX "Entity_revisionId_key" ON public."Entity" USING btree ("revisionId");


--
-- Name: Entity_uid_key; Type: INDEX; Schema: public; Owner: repco
--

CREATE UNIQUE INDEX "Entity_uid_key" ON public."Entity" USING btree (uid);


--
-- Name: File_revisionId_key; Type: INDEX; Schema: public; Owner: repco
--

CREATE UNIQUE INDEX "File_revisionId_key" ON public."File" USING btree ("revisionId");


--
-- Name: File_uid_key; Type: INDEX; Schema: public; Owner: repco
--

CREATE UNIQUE INDEX "File_uid_key" ON public."File" USING btree (uid);


--
-- Name: IngestError_id_key; Type: INDEX; Schema: public; Owner: repco
--

CREATE UNIQUE INDEX "IngestError_id_key" ON public."IngestError" USING btree (id);


--
-- Name: Keypair_did_key; Type: INDEX; Schema: public; Owner: repco
--

CREATE UNIQUE INDEX "Keypair_did_key" ON public."Keypair" USING btree (did);


--
-- Name: License_revisionId_key; Type: INDEX; Schema: public; Owner: repco
--

CREATE UNIQUE INDEX "License_revisionId_key" ON public."License" USING btree ("revisionId");


--
-- Name: License_uid_key; Type: INDEX; Schema: public; Owner: repco
--

CREATE UNIQUE INDEX "License_uid_key" ON public."License" USING btree (uid);


--
-- Name: MediaAsset_revisionId_key; Type: INDEX; Schema: public; Owner: repco
--

CREATE UNIQUE INDEX "MediaAsset_revisionId_key" ON public."MediaAsset" USING btree ("revisionId");


--
-- Name: MediaAsset_uid_key; Type: INDEX; Schema: public; Owner: repco
--

CREATE UNIQUE INDEX "MediaAsset_uid_key" ON public."MediaAsset" USING btree (uid);


--
-- Name: Metadata_revisionId_key; Type: INDEX; Schema: public; Owner: repco
--

CREATE UNIQUE INDEX "Metadata_revisionId_key" ON public."Metadata" USING btree ("revisionId");


--
-- Name: Metadata_uid_key; Type: INDEX; Schema: public; Owner: repco
--

CREATE UNIQUE INDEX "Metadata_uid_key" ON public."Metadata" USING btree (uid);


--
-- Name: PublicationService_revisionId_key; Type: INDEX; Schema: public; Owner: repco
--

CREATE UNIQUE INDEX "PublicationService_revisionId_key" ON public."PublicationService" USING btree ("revisionId");


--
-- Name: PublicationService_uid_key; Type: INDEX; Schema: public; Owner: repco
--

CREATE UNIQUE INDEX "PublicationService_uid_key" ON public."PublicationService" USING btree (uid);


--
-- Name: Repo_did_key; Type: INDEX; Schema: public; Owner: repco
--

CREATE UNIQUE INDEX "Repo_did_key" ON public."Repo" USING btree (did);


--
-- Name: Repo_head_key; Type: INDEX; Schema: public; Owner: repco
--

CREATE UNIQUE INDEX "Repo_head_key" ON public."Repo" USING btree (head);


--
-- Name: Repo_name_key; Type: INDEX; Schema: public; Owner: repco
--

CREATE UNIQUE INDEX "Repo_name_key" ON public."Repo" USING btree (name);


--
-- Name: Repo_tail_key; Type: INDEX; Schema: public; Owner: repco
--

CREATE UNIQUE INDEX "Repo_tail_key" ON public."Repo" USING btree (tail);


--
-- Name: Revision_id_key; Type: INDEX; Schema: public; Owner: repco
--

CREATE UNIQUE INDEX "Revision_id_key" ON public."Revision" USING btree (id);


--
-- Name: Revision_prevRevisionId_key; Type: INDEX; Schema: public; Owner: repco
--

CREATE UNIQUE INDEX "Revision_prevRevisionId_key" ON public."Revision" USING btree ("prevRevisionId");


--
-- Name: Revision_revisionCid_key; Type: INDEX; Schema: public; Owner: repco
--

CREATE UNIQUE INDEX "Revision_revisionCid_key" ON public."Revision" USING btree ("revisionCid");


--
-- Name: SourceRecord_uid_key; Type: INDEX; Schema: public; Owner: repco
--

CREATE UNIQUE INDEX "SourceRecord_uid_key" ON public."SourceRecord" USING btree (uid);


--
-- Name: Transcript_revisionId_key; Type: INDEX; Schema: public; Owner: repco
--

CREATE UNIQUE INDEX "Transcript_revisionId_key" ON public."Transcript" USING btree ("revisionId");


--
-- Name: Transcript_uid_key; Type: INDEX; Schema: public; Owner: repco
--

CREATE UNIQUE INDEX "Transcript_uid_key" ON public."Transcript" USING btree (uid);


--
-- Name: Ucans_cid_key; Type: INDEX; Schema: public; Owner: repco
--

CREATE UNIQUE INDEX "Ucans_cid_key" ON public."Ucans" USING btree (cid);


--
-- Name: User_did_key; Type: INDEX; Schema: public; Owner: repco
--

CREATE UNIQUE INDEX "User_did_key" ON public."User" USING btree (did);


--
-- Name: _ConceptToContentItem_AB_unique; Type: INDEX; Schema: public; Owner: repco
--

CREATE UNIQUE INDEX "_ConceptToContentItem_AB_unique" ON public."_ConceptToContentItem" USING btree ("A", "B");


--
-- Name: _ConceptToContentItem_B_index; Type: INDEX; Schema: public; Owner: repco
--

CREATE INDEX "_ConceptToContentItem_B_index" ON public."_ConceptToContentItem" USING btree ("B");


--
-- Name: _ConceptToMediaAsset_AB_unique; Type: INDEX; Schema: public; Owner: repco
--

CREATE UNIQUE INDEX "_ConceptToMediaAsset_AB_unique" ON public."_ConceptToMediaAsset" USING btree ("A", "B");


--
-- Name: _ConceptToMediaAsset_B_index; Type: INDEX; Schema: public; Owner: repco
--

CREATE INDEX "_ConceptToMediaAsset_B_index" ON public."_ConceptToMediaAsset" USING btree ("B");


--
-- Name: _ContentGroupingToContentItem_AB_unique; Type: INDEX; Schema: public; Owner: repco
--

CREATE UNIQUE INDEX "_ContentGroupingToContentItem_AB_unique" ON public."_ContentGroupingToContentItem" USING btree ("A", "B");


--
-- Name: _ContentGroupingToContentItem_B_index; Type: INDEX; Schema: public; Owner: repco
--

CREATE INDEX "_ContentGroupingToContentItem_B_index" ON public."_ContentGroupingToContentItem" USING btree ("B");


--
-- Name: _ContentItemToContribution_AB_unique; Type: INDEX; Schema: public; Owner: repco
--

CREATE UNIQUE INDEX "_ContentItemToContribution_AB_unique" ON public."_ContentItemToContribution" USING btree ("A", "B");


--
-- Name: _ContentItemToContribution_B_index; Type: INDEX; Schema: public; Owner: repco
--

CREATE INDEX "_ContentItemToContribution_B_index" ON public."_ContentItemToContribution" USING btree ("B");


--
-- Name: _ContentItemToMediaAsset_AB_unique; Type: INDEX; Schema: public; Owner: repco
--

CREATE UNIQUE INDEX "_ContentItemToMediaAsset_AB_unique" ON public."_ContentItemToMediaAsset" USING btree ("A", "B");


--
-- Name: _ContentItemToMediaAsset_B_index; Type: INDEX; Schema: public; Owner: repco
--

CREATE INDEX "_ContentItemToMediaAsset_B_index" ON public."_ContentItemToMediaAsset" USING btree ("B");


--
-- Name: _ContributionToContributor_AB_unique; Type: INDEX; Schema: public; Owner: repco
--

CREATE UNIQUE INDEX "_ContributionToContributor_AB_unique" ON public."_ContributionToContributor" USING btree ("A", "B");


--
-- Name: _ContributionToContributor_B_index; Type: INDEX; Schema: public; Owner: repco
--

CREATE INDEX "_ContributionToContributor_B_index" ON public."_ContributionToContributor" USING btree ("B");


--
-- Name: _ContributionToMediaAsset_AB_unique; Type: INDEX; Schema: public; Owner: repco
--

CREATE UNIQUE INDEX "_ContributionToMediaAsset_AB_unique" ON public."_ContributionToMediaAsset" USING btree ("A", "B");


--
-- Name: _ContributionToMediaAsset_B_index; Type: INDEX; Schema: public; Owner: repco
--

CREATE INDEX "_ContributionToMediaAsset_B_index" ON public."_ContributionToMediaAsset" USING btree ("B");


--
-- Name: _FileToMediaAsset_AB_unique; Type: INDEX; Schema: public; Owner: repco
--

CREATE UNIQUE INDEX "_FileToMediaAsset_AB_unique" ON public."_FileToMediaAsset" USING btree ("A", "B");


--
-- Name: _FileToMediaAsset_B_index; Type: INDEX; Schema: public; Owner: repco
--

CREATE INDEX "_FileToMediaAsset_B_index" ON public."_FileToMediaAsset" USING btree ("B");


--
-- Name: _RevisionToCommit_AB_unique; Type: INDEX; Schema: public; Owner: repco
--

CREATE UNIQUE INDEX "_RevisionToCommit_AB_unique" ON public."_RevisionToCommit" USING btree ("A", "B");


--
-- Name: _RevisionToCommit_B_index; Type: INDEX; Schema: public; Owner: repco
--

CREATE INDEX "_RevisionToCommit_B_index" ON public."_RevisionToCommit" USING btree ("B");


--
-- Name: BroadcastEvent BroadcastEvent_broadcastServiceUid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: repco
--

ALTER TABLE ONLY public."BroadcastEvent"
    ADD CONSTRAINT "BroadcastEvent_broadcastServiceUid_fkey" FOREIGN KEY ("broadcastServiceUid") REFERENCES public."PublicationService"(uid) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: BroadcastEvent BroadcastEvent_contentItemUid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: repco
--

ALTER TABLE ONLY public."BroadcastEvent"
    ADD CONSTRAINT "BroadcastEvent_contentItemUid_fkey" FOREIGN KEY ("contentItemUid") REFERENCES public."ContentItem"(uid) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: BroadcastEvent BroadcastEvent_revisionId_fkey; Type: FK CONSTRAINT; Schema: public; Owner: repco
--

ALTER TABLE ONLY public."BroadcastEvent"
    ADD CONSTRAINT "BroadcastEvent_revisionId_fkey" FOREIGN KEY ("revisionId") REFERENCES public."Revision"(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: Chapter Chapter_mediaAssetUid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: repco
--

ALTER TABLE ONLY public."Chapter"
    ADD CONSTRAINT "Chapter_mediaAssetUid_fkey" FOREIGN KEY ("mediaAssetUid") REFERENCES public."MediaAsset"(uid) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: Chapter Chapter_revisionId_fkey; Type: FK CONSTRAINT; Schema: public; Owner: repco
--

ALTER TABLE ONLY public."Chapter"
    ADD CONSTRAINT "Chapter_revisionId_fkey" FOREIGN KEY ("revisionId") REFERENCES public."Revision"(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: Commit Commit_agentDid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: repco
--

ALTER TABLE ONLY public."Commit"
    ADD CONSTRAINT "Commit_agentDid_fkey" FOREIGN KEY ("agentDid") REFERENCES public."Agent"(did) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: Commit Commit_parent_fkey; Type: FK CONSTRAINT; Schema: public; Owner: repco
--

ALTER TABLE ONLY public."Commit"
    ADD CONSTRAINT "Commit_parent_fkey" FOREIGN KEY (parent) REFERENCES public."Commit"("rootCid") ON UPDATE CASCADE ON DELETE SET NULL;


--
-- Name: Commit Commit_repoDid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: repco
--

ALTER TABLE ONLY public."Commit"
    ADD CONSTRAINT "Commit_repoDid_fkey" FOREIGN KEY ("repoDid") REFERENCES public."Repo"(did) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: Concept Concept_parentUid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: repco
--

ALTER TABLE ONLY public."Concept"
    ADD CONSTRAINT "Concept_parentUid_fkey" FOREIGN KEY ("parentUid") REFERENCES public."Concept"(uid) ON UPDATE CASCADE ON DELETE SET NULL;


--
-- Name: Concept Concept_revisionId_fkey; Type: FK CONSTRAINT; Schema: public; Owner: repco
--

ALTER TABLE ONLY public."Concept"
    ADD CONSTRAINT "Concept_revisionId_fkey" FOREIGN KEY ("revisionId") REFERENCES public."Revision"(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: Concept Concept_sameAsUid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: repco
--

ALTER TABLE ONLY public."Concept"
    ADD CONSTRAINT "Concept_sameAsUid_fkey" FOREIGN KEY ("sameAsUid") REFERENCES public."Concept"(uid) ON UPDATE CASCADE ON DELETE SET NULL;


--
-- Name: ContentGrouping ContentGrouping_licenseUid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: repco
--

ALTER TABLE ONLY public."ContentGrouping"
    ADD CONSTRAINT "ContentGrouping_licenseUid_fkey" FOREIGN KEY ("licenseUid") REFERENCES public."License"(uid) ON UPDATE CASCADE ON DELETE SET NULL;


--
-- Name: ContentGrouping ContentGrouping_revisionId_fkey; Type: FK CONSTRAINT; Schema: public; Owner: repco
--

ALTER TABLE ONLY public."ContentGrouping"
    ADD CONSTRAINT "ContentGrouping_revisionId_fkey" FOREIGN KEY ("revisionId") REFERENCES public."Revision"(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: ContentItemMachineTranslation ContentItemMachineTranslation_contentItemUid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: repco
--

ALTER TABLE ONLY public."ContentItemMachineTranslation"
    ADD CONSTRAINT "ContentItemMachineTranslation_contentItemUid_fkey" FOREIGN KEY ("contentItemUid") REFERENCES public."ContentItem"(uid) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: ContentItemMachineTranslation ContentItemMachineTranslation_revisionId_fkey; Type: FK CONSTRAINT; Schema: public; Owner: repco
--

ALTER TABLE ONLY public."ContentItemMachineTranslation"
    ADD CONSTRAINT "ContentItemMachineTranslation_revisionId_fkey" FOREIGN KEY ("revisionId") REFERENCES public."Revision"(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: ContentItem ContentItem_licenseUid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: repco
--

ALTER TABLE ONLY public."ContentItem"
    ADD CONSTRAINT "ContentItem_licenseUid_fkey" FOREIGN KEY ("licenseUid") REFERENCES public."License"(uid) ON UPDATE CASCADE ON DELETE SET NULL;


--
-- Name: ContentItem ContentItem_primaryGroupingUid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: repco
--

ALTER TABLE ONLY public."ContentItem"
    ADD CONSTRAINT "ContentItem_primaryGroupingUid_fkey" FOREIGN KEY ("primaryGroupingUid") REFERENCES public."ContentGrouping"(uid) ON UPDATE CASCADE ON DELETE SET NULL;


--
-- Name: ContentItem ContentItem_publicationServiceUid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: repco
--

ALTER TABLE ONLY public."ContentItem"
    ADD CONSTRAINT "ContentItem_publicationServiceUid_fkey" FOREIGN KEY ("publicationServiceUid") REFERENCES public."PublicationService"(uid) ON UPDATE CASCADE ON DELETE SET NULL;


--
-- Name: ContentItem ContentItem_revisionId_fkey; Type: FK CONSTRAINT; Schema: public; Owner: repco
--

ALTER TABLE ONLY public."ContentItem"
    ADD CONSTRAINT "ContentItem_revisionId_fkey" FOREIGN KEY ("revisionId") REFERENCES public."Revision"(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: Contribution Contribution_revisionId_fkey; Type: FK CONSTRAINT; Schema: public; Owner: repco
--

ALTER TABLE ONLY public."Contribution"
    ADD CONSTRAINT "Contribution_revisionId_fkey" FOREIGN KEY ("revisionId") REFERENCES public."Revision"(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: Contributor Contributor_profilePictureUid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: repco
--

ALTER TABLE ONLY public."Contributor"
    ADD CONSTRAINT "Contributor_profilePictureUid_fkey" FOREIGN KEY ("profilePictureUid") REFERENCES public."File"(uid) ON UPDATE CASCADE ON DELETE SET NULL;


--
-- Name: Contributor Contributor_revisionId_fkey; Type: FK CONSTRAINT; Schema: public; Owner: repco
--

ALTER TABLE ONLY public."Contributor"
    ADD CONSTRAINT "Contributor_revisionId_fkey" FOREIGN KEY ("revisionId") REFERENCES public."Revision"(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: DataSource DataSource_repoDid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: repco
--

ALTER TABLE ONLY public."DataSource"
    ADD CONSTRAINT "DataSource_repoDid_fkey" FOREIGN KEY ("repoDid") REFERENCES public."Repo"(did) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: Entity Entity_revisionId_fkey; Type: FK CONSTRAINT; Schema: public; Owner: repco
--

ALTER TABLE ONLY public."Entity"
    ADD CONSTRAINT "Entity_revisionId_fkey" FOREIGN KEY ("revisionId") REFERENCES public."Revision"(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: File File_revisionId_fkey; Type: FK CONSTRAINT; Schema: public; Owner: repco
--

ALTER TABLE ONLY public."File"
    ADD CONSTRAINT "File_revisionId_fkey" FOREIGN KEY ("revisionId") REFERENCES public."Revision"(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: License License_revisionId_fkey; Type: FK CONSTRAINT; Schema: public; Owner: repco
--

ALTER TABLE ONLY public."License"
    ADD CONSTRAINT "License_revisionId_fkey" FOREIGN KEY ("revisionId") REFERENCES public."Revision"(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: MediaAsset MediaAsset_licenseUid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: repco
--

ALTER TABLE ONLY public."MediaAsset"
    ADD CONSTRAINT "MediaAsset_licenseUid_fkey" FOREIGN KEY ("licenseUid") REFERENCES public."License"(uid) ON UPDATE CASCADE ON DELETE SET NULL;


--
-- Name: MediaAsset MediaAsset_revisionId_fkey; Type: FK CONSTRAINT; Schema: public; Owner: repco
--

ALTER TABLE ONLY public."MediaAsset"
    ADD CONSTRAINT "MediaAsset_revisionId_fkey" FOREIGN KEY ("revisionId") REFERENCES public."Revision"(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: MediaAsset MediaAsset_teaserImageUid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: repco
--

ALTER TABLE ONLY public."MediaAsset"
    ADD CONSTRAINT "MediaAsset_teaserImageUid_fkey" FOREIGN KEY ("teaserImageUid") REFERENCES public."File"(uid) ON UPDATE CASCADE ON DELETE SET NULL;


--
-- Name: Metadata Metadata_revisionId_fkey; Type: FK CONSTRAINT; Schema: public; Owner: repco
--

ALTER TABLE ONLY public."Metadata"
    ADD CONSTRAINT "Metadata_revisionId_fkey" FOREIGN KEY ("revisionId") REFERENCES public."Revision"(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: Metadata Metadata_targetUid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: repco
--

ALTER TABLE ONLY public."Metadata"
    ADD CONSTRAINT "Metadata_targetUid_fkey" FOREIGN KEY ("targetUid") REFERENCES public."Entity"(uid) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: PublicationService PublicationService_publisherUid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: repco
--

ALTER TABLE ONLY public."PublicationService"
    ADD CONSTRAINT "PublicationService_publisherUid_fkey" FOREIGN KEY ("publisherUid") REFERENCES public."Contributor"(uid) ON UPDATE CASCADE ON DELETE SET NULL;


--
-- Name: PublicationService PublicationService_revisionId_fkey; Type: FK CONSTRAINT; Schema: public; Owner: repco
--

ALTER TABLE ONLY public."PublicationService"
    ADD CONSTRAINT "PublicationService_revisionId_fkey" FOREIGN KEY ("revisionId") REFERENCES public."Revision"(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: Repo Repo_head_fkey; Type: FK CONSTRAINT; Schema: public; Owner: repco
--

ALTER TABLE ONLY public."Repo"
    ADD CONSTRAINT "Repo_head_fkey" FOREIGN KEY (head) REFERENCES public."Commit"("rootCid") ON UPDATE CASCADE ON DELETE SET NULL;


--
-- Name: Revision Revision_agentDid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: repco
--

ALTER TABLE ONLY public."Revision"
    ADD CONSTRAINT "Revision_agentDid_fkey" FOREIGN KEY ("agentDid") REFERENCES public."Agent"(did) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: Revision Revision_prevRevisionId_fkey; Type: FK CONSTRAINT; Schema: public; Owner: repco
--

ALTER TABLE ONLY public."Revision"
    ADD CONSTRAINT "Revision_prevRevisionId_fkey" FOREIGN KEY ("prevRevisionId") REFERENCES public."Revision"(id) ON UPDATE CASCADE ON DELETE SET NULL;


--
-- Name: Revision Revision_repoDid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: repco
--

ALTER TABLE ONLY public."Revision"
    ADD CONSTRAINT "Revision_repoDid_fkey" FOREIGN KEY ("repoDid") REFERENCES public."Repo"(did) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: SourceRecord SourceRecord_dataSourceUid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: repco
--

ALTER TABLE ONLY public."SourceRecord"
    ADD CONSTRAINT "SourceRecord_dataSourceUid_fkey" FOREIGN KEY ("dataSourceUid") REFERENCES public."DataSource"(uid) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: Transcript Transcript_mediaAssetUid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: repco
--

ALTER TABLE ONLY public."Transcript"
    ADD CONSTRAINT "Transcript_mediaAssetUid_fkey" FOREIGN KEY ("mediaAssetUid") REFERENCES public."MediaAsset"(uid) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: Transcript Transcript_revisionId_fkey; Type: FK CONSTRAINT; Schema: public; Owner: repco
--

ALTER TABLE ONLY public."Transcript"
    ADD CONSTRAINT "Transcript_revisionId_fkey" FOREIGN KEY ("revisionId") REFERENCES public."Revision"(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: User User_did_fkey; Type: FK CONSTRAINT; Schema: public; Owner: repco
--

ALTER TABLE ONLY public."User"
    ADD CONSTRAINT "User_did_fkey" FOREIGN KEY (did) REFERENCES public."Agent"(did) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: _ConceptToContentItem _ConceptToContentItem_A_fkey; Type: FK CONSTRAINT; Schema: public; Owner: repco
--

ALTER TABLE ONLY public."_ConceptToContentItem"
    ADD CONSTRAINT "_ConceptToContentItem_A_fkey" FOREIGN KEY ("A") REFERENCES public."Concept"(uid) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: _ConceptToContentItem _ConceptToContentItem_B_fkey; Type: FK CONSTRAINT; Schema: public; Owner: repco
--

ALTER TABLE ONLY public."_ConceptToContentItem"
    ADD CONSTRAINT "_ConceptToContentItem_B_fkey" FOREIGN KEY ("B") REFERENCES public."ContentItem"(uid) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: _ConceptToMediaAsset _ConceptToMediaAsset_A_fkey; Type: FK CONSTRAINT; Schema: public; Owner: repco
--

ALTER TABLE ONLY public."_ConceptToMediaAsset"
    ADD CONSTRAINT "_ConceptToMediaAsset_A_fkey" FOREIGN KEY ("A") REFERENCES public."Concept"(uid) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: _ConceptToMediaAsset _ConceptToMediaAsset_B_fkey; Type: FK CONSTRAINT; Schema: public; Owner: repco
--

ALTER TABLE ONLY public."_ConceptToMediaAsset"
    ADD CONSTRAINT "_ConceptToMediaAsset_B_fkey" FOREIGN KEY ("B") REFERENCES public."MediaAsset"(uid) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: _ContentGroupingToContentItem _ContentGroupingToContentItem_A_fkey; Type: FK CONSTRAINT; Schema: public; Owner: repco
--

ALTER TABLE ONLY public."_ContentGroupingToContentItem"
    ADD CONSTRAINT "_ContentGroupingToContentItem_A_fkey" FOREIGN KEY ("A") REFERENCES public."ContentGrouping"(uid) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: _ContentGroupingToContentItem _ContentGroupingToContentItem_B_fkey; Type: FK CONSTRAINT; Schema: public; Owner: repco
--

ALTER TABLE ONLY public."_ContentGroupingToContentItem"
    ADD CONSTRAINT "_ContentGroupingToContentItem_B_fkey" FOREIGN KEY ("B") REFERENCES public."ContentItem"(uid) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: _ContentItemToContribution _ContentItemToContribution_A_fkey; Type: FK CONSTRAINT; Schema: public; Owner: repco
--

ALTER TABLE ONLY public."_ContentItemToContribution"
    ADD CONSTRAINT "_ContentItemToContribution_A_fkey" FOREIGN KEY ("A") REFERENCES public."ContentItem"(uid) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: _ContentItemToContribution _ContentItemToContribution_B_fkey; Type: FK CONSTRAINT; Schema: public; Owner: repco
--

ALTER TABLE ONLY public."_ContentItemToContribution"
    ADD CONSTRAINT "_ContentItemToContribution_B_fkey" FOREIGN KEY ("B") REFERENCES public."Contribution"(uid) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: _ContentItemToMediaAsset _ContentItemToMediaAsset_A_fkey; Type: FK CONSTRAINT; Schema: public; Owner: repco
--

ALTER TABLE ONLY public."_ContentItemToMediaAsset"
    ADD CONSTRAINT "_ContentItemToMediaAsset_A_fkey" FOREIGN KEY ("A") REFERENCES public."ContentItem"(uid) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: _ContentItemToMediaAsset _ContentItemToMediaAsset_B_fkey; Type: FK CONSTRAINT; Schema: public; Owner: repco
--

ALTER TABLE ONLY public."_ContentItemToMediaAsset"
    ADD CONSTRAINT "_ContentItemToMediaAsset_B_fkey" FOREIGN KEY ("B") REFERENCES public."MediaAsset"(uid) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: _ContributionToContributor _ContributionToContributor_A_fkey; Type: FK CONSTRAINT; Schema: public; Owner: repco
--

ALTER TABLE ONLY public."_ContributionToContributor"
    ADD CONSTRAINT "_ContributionToContributor_A_fkey" FOREIGN KEY ("A") REFERENCES public."Contribution"(uid) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: _ContributionToContributor _ContributionToContributor_B_fkey; Type: FK CONSTRAINT; Schema: public; Owner: repco
--

ALTER TABLE ONLY public."_ContributionToContributor"
    ADD CONSTRAINT "_ContributionToContributor_B_fkey" FOREIGN KEY ("B") REFERENCES public."Contributor"(uid) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: _ContributionToMediaAsset _ContributionToMediaAsset_A_fkey; Type: FK CONSTRAINT; Schema: public; Owner: repco
--

ALTER TABLE ONLY public."_ContributionToMediaAsset"
    ADD CONSTRAINT "_ContributionToMediaAsset_A_fkey" FOREIGN KEY ("A") REFERENCES public."Contribution"(uid) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: _ContributionToMediaAsset _ContributionToMediaAsset_B_fkey; Type: FK CONSTRAINT; Schema: public; Owner: repco
--

ALTER TABLE ONLY public."_ContributionToMediaAsset"
    ADD CONSTRAINT "_ContributionToMediaAsset_B_fkey" FOREIGN KEY ("B") REFERENCES public."MediaAsset"(uid) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: _FileToMediaAsset _FileToMediaAsset_A_fkey; Type: FK CONSTRAINT; Schema: public; Owner: repco
--

ALTER TABLE ONLY public."_FileToMediaAsset"
    ADD CONSTRAINT "_FileToMediaAsset_A_fkey" FOREIGN KEY ("A") REFERENCES public."File"(uid) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: _FileToMediaAsset _FileToMediaAsset_B_fkey; Type: FK CONSTRAINT; Schema: public; Owner: repco
--

ALTER TABLE ONLY public."_FileToMediaAsset"
    ADD CONSTRAINT "_FileToMediaAsset_B_fkey" FOREIGN KEY ("B") REFERENCES public."MediaAsset"(uid) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: _RevisionToCommit _RevisionToCommit_A_fkey; Type: FK CONSTRAINT; Schema: public; Owner: repco
--

ALTER TABLE ONLY public."_RevisionToCommit"
    ADD CONSTRAINT "_RevisionToCommit_A_fkey" FOREIGN KEY ("A") REFERENCES public."Commit"("rootCid") ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: _RevisionToCommit _RevisionToCommit_B_fkey; Type: FK CONSTRAINT; Schema: public; Owner: repco
--

ALTER TABLE ONLY public."_RevisionToCommit"
    ADD CONSTRAINT "_RevisionToCommit_B_fkey" FOREIGN KEY ("B") REFERENCES public."Revision"(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: SCHEMA public; Type: ACL; Schema: -; Owner: repco
--

REVOKE USAGE ON SCHEMA public FROM PUBLIC;


--
-- Name: postgraphile_watch_ddl; Type: EVENT TRIGGER; Schema: -; Owner: repco
--

CREATE EVENT TRIGGER postgraphile_watch_ddl ON ddl_command_end
         WHEN TAG IN ('ALTER AGGREGATE', 'ALTER DOMAIN', 'ALTER EXTENSION', 'ALTER FOREIGN TABLE', 'ALTER FUNCTION', 'ALTER POLICY', 'ALTER SCHEMA', 'ALTER TABLE', 'ALTER TYPE', 'ALTER VIEW', 'COMMENT', 'CREATE AGGREGATE', 'CREATE DOMAIN', 'CREATE EXTENSION', 'CREATE FOREIGN TABLE', 'CREATE FUNCTION', 'CREATE INDEX', 'CREATE POLICY', 'CREATE RULE', 'CREATE SCHEMA', 'CREATE TABLE', 'CREATE TABLE AS', 'CREATE VIEW', 'DROP AGGREGATE', 'DROP DOMAIN', 'DROP EXTENSION', 'DROP FOREIGN TABLE', 'DROP FUNCTION', 'DROP INDEX', 'DROP OWNED', 'DROP POLICY', 'DROP RULE', 'DROP SCHEMA', 'DROP TABLE', 'DROP TYPE', 'DROP VIEW', 'GRANT', 'REVOKE', 'SELECT INTO')
   EXECUTE FUNCTION postgraphile_watch.notify_watchers_ddl();


ALTER EVENT TRIGGER postgraphile_watch_ddl OWNER TO repco;

--
-- Name: postgraphile_watch_drop; Type: EVENT TRIGGER; Schema: -; Owner: repco
--

CREATE EVENT TRIGGER postgraphile_watch_drop ON sql_drop
   EXECUTE FUNCTION postgraphile_watch.notify_watchers_drop();


ALTER EVENT TRIGGER postgraphile_watch_drop OWNER TO repco;

--
-- PostgreSQL database dump complete
--

