import { makeWrapResolversPlugin } from 'graphile-utils'

// Adds some validation logic for pagination arguments:
// - first/last may not be higher than 100
// - if no pagination args are set, default to first 10
// TODO: Decide if the latter is not too much in conflict with the spec
// https://relay.dev/graphql/connections.htm#sec-Pagination-algorithm
// Maybe default to error. Breaking change so decide before release.
const FilterRemovedPlugin = makeWrapResolversPlugin(
  (context) => {
    const scope = context.scope
    // I *think* this means that we do a select many query.
    if (scope.isPgFieldConnection) {
      return { scope }
    }
    return null
  },
  ({ scope }) =>
    async (resolver, _user, args, _context, resolveInfo) => {
      if (scope.pgFieldIntrospection.name === 'ContentItem') {
        if ('filter' in args) {
          args.filter.removed = {
            equalTo: false,
          }
        } else {
          args.filter = {
            removed: {
              equalTo: false,
            },
          }
        }
      }
      // @ts-ignore
      const result = await resolver()
      return result
    },
)

export default FilterRemovedPlugin
