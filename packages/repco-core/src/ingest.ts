import { log, UntilStopped } from 'repco-common'
import { setTimeout } from 'timers/promises';
import { randomInt } from 'crypto';

import {
  DataSourceRegistry,
  IngestError,
  IngestErrorScope,
  ingestUpdatesFromDataSource,
} from './datasource.js'
import { DataSourcePluginRegistry } from './plugins.js'
import { Repo } from './repo.js'


// export type WorkerConstructor
export enum WorkerStatus {
  Running = 'running',
  Stopped = 'stopped',
}

export enum IngestState {
  Cancelled = 'cancelled',
  Ready = 'ready',
  Finished = 'finished',
  FailedAtFetch = 'failed_fetch',
  FailedAtIngest = 'failed_ingest',
  FailedFatal = 'failed_fatal',
}

export class IngestOutcome {
  error?: Error
  cursor?: string

  constructor(
    public uid: string,
    public state: IngestState,
    details?: { error?: Error; cursor?: string },
  ) {
    this.error = details?.error
    this.cursor = details?.cursor
  }

  didFail(): boolean {
    return this.state.startsWith('failed_')
  }

  shouldContinue(): boolean {
    return (
      this.state !== IngestState.FailedFatal &&
      this.state !== IngestState.Cancelled
    )
  }
}

export class Ingester {
  // recheck finished datasources every 10 seconds
  waitAfterFinish = 1000 * 10
  // retry failed datasources every 30 seconds
  waitRetry = 1000 * 30

  plugins: DataSourcePluginRegistry
  repo: Repo
  hydrated = false
  untilStopped = new UntilStopped()

  constructor(plugins: DataSourcePluginRegistry, repo: Repo) {
    this.plugins = plugins
    this.repo = repo
  }

  get datasources(): DataSourceRegistry {
    return this.repo.dsr
  }

  async init() {
    if (this.hydrated) return
    await this.repo.dsr.hydrate(this.repo.prisma, this.plugins, this.repo.did)
    this.hydrated = true
  }

  async ingest(uid: string, wait?: number): Promise<IngestOutcome> {
    //log.info('Ingest '+uid+' run')
    
    const to:any = randomInt(900,3000)
    const start = new Date();
    //console.log('timeInMs => ', to);
    //console.log('start => ',start);
    const until = await setTimeout(to);
    //console.log(`random TO over ${to}`)

    if (this.untilStopped.stopped) {
      //log.info('Ingest '+uid+'  Cancelled 1')
      return new IngestOutcome(uid, IngestState.Cancelled)
    }
    if (!this.hydrated) await this.init()
    const ds = this.datasources.get(uid)
    if (!ds) {
      const error = new Error(`Datasource \`${uid}\` not found`)
      //log.info('Ingest '+uid+'  FailedFatal')
      return new IngestOutcome(uid, IngestState.FailedFatal, { error })
    }

    if (wait) await this.untilStopped.timeout(wait)

    if (this.untilStopped.stopped) {
      //log.info('Ingest '+uid+'  Cancelled 2')
      return new IngestOutcome(uid, IngestState.Cancelled)
    }

    try {
      //log.info('await ingestUpdatesFromDataSource '+uid+'  Cancelled 2')
      const { finished, nextCursor } = await ingestUpdatesFromDataSource(
        this.repo,
        ds,
        true,
      )

      //console.log(finished, nextCursor)
      const details = { cursor: nextCursor }

      const end = new Date();
      //log.info(`Ingest start randomized for less DB Load - took ${end.getTime() - start.getTime()} msec`)
      log.info(`Operation ingest took ${end.getTime() - start.getTime()} msec ${uid}`);
      log.info(`Memory ingest took: ${JSON.stringify(process.memoryUsage())}`)


      if (finished) {
        //log.info('Ingest '+uid+'  Finished')
        return new IngestOutcome(uid, IngestState.Finished, details)
      } else {
        //log.info('Ingest '+uid+'  Ready')
        return new IngestOutcome(uid, IngestState.Ready, details)
      }
  
    } catch (error) {
      // if the error is not an IngestError, it is a bug, and thus a fatal error
      if (!(error instanceof IngestError)) {
        //log.info('Ingest '+uid+'  FailedFatal')
        return new IngestOutcome(uid, IngestState.FailedFatal, {
          error: error as Error,
        })
      }

      let state = IngestState.FailedAtIngest
      if (error.scope === IngestErrorScope.FetchUpdates) {
        state = IngestState.FailedAtFetch
      }
      //log.info('Ingest '+uid+'  Final ')
      //console.log(`Memory ingest Final: ${JSON.stringify(process.memoryUsage())}`)
      return new IngestOutcome(uid, state, { error })
    }
  }

  async ingestAll() {
    //log.info('Ingest all Started')
    if (!this.hydrated) await this.init()
    return await Promise.all(
      this.datasources.ids().map((uid) => this.ingest(uid)),
    )
  }

  stop() {
    //log.info('Ingest stopped' + this.repo.did)
    this.untilStopped.stop()
  }

  get stopped() {
    return this.untilStopped.stopped
  }

  async *workLoop() {
    if (!this.hydrated) await this.init()
    const pending = new Map(
      this.datasources.ids().map((uid) => [uid, this.ingest(uid)]),
    )

    const onDsCreate = (uid: string) => {
      if (this.stopped) {
        this.repo.dsr.events.removeListener('create', onDsCreate)
        return
      }
      log.info(`added datasource to ingest ${this.repo.did} ${uid}`)
      pending.set(uid, this.ingest(uid))
    }
    this.repo.dsr.events.on('create', onDsCreate)
    while (pending.size) {
      const res = await Promise.race(pending.values())
      yield res
      pending.delete(res.uid)

      if (!this.stopped && res.shouldContinue()) {
        let wait: number | undefined
        if (res.state === IngestState.FailedAtFetch
            || res.state === IngestState.FailedAtIngest
            || res.state === IngestState.FailedFatal
        ) {
          wait = this.waitRetry
        }
        if (res.state === IngestState.Finished
          || res.state === IngestState.Ready
          || res.state === IngestState.Cancelled
        ) {
          wait = this.waitAfterFinish
        }

        //console.log('set pending', res.uid, " state: ",res.state, " to ", wait)
        //console.log(`Memory Usage ingest set wait: ${JSON.stringify(process.memoryUsage())}`)
        pending.set(res.uid, this.ingest(res.uid, wait))
      }
    }
  }
}
