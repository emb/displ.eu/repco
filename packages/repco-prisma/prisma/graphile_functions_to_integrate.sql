CREATE OR REPLACE FUNCTION public."ContentItem_title_translation"(
	c "ContentItem",lang text)
    RETURNS jsonb
    LANGUAGE 'sql'
    COST 100
    STABLE PARALLEL UNSAFE
AS $BODY$

SELECT
			jsonb_build_object(
				lang,jsonb_build_object('value',t.title,'engine',t.engine)
			)

from "ContentItemTranslation" as t
WHERE 
		t."contentItemUid" = c.uid
		AND 
		t."targetLanguage" = lang
$BODY$;

CREATE OR REPLACE FUNCTION public."ContentItem_subtitle_translation"(
	c "ContentItem",lang text)
    RETURNS jsonb
    LANGUAGE 'sql'
    COST 100
    STABLE PARALLEL UNSAFE
AS $BODY$


SELECT
			jsonb_build_object(
				lang,jsonb_build_object('value',t.subtitle,'engine',t.engine)
			)

from "ContentItemTranslation" as t
WHERE 
		t."contentItemUid" = c.uid
		AND 
		t."targetLanguage" = lang
$BODY$;


CREATE OR REPLACE FUNCTION public."ContentItem_summary_translation"(
	c "ContentItem",lang text)
    RETURNS jsonb
    LANGUAGE 'sql'
    COST 100
    STABLE PARALLEL UNSAFE
AS $BODY$


SELECT
			jsonb_build_object(
				lang,jsonb_build_object('value',t.summary,'engine',t.engine)
			)

from "ContentItemTranslation" as t
WHERE 
		t."contentItemUid" = c.uid
		AND 
		t."targetLanguage" = lang
$BODY$;

CREATE OR REPLACE FUNCTION public."ContentItem_content_translation"(
	c "ContentItem",lang text)
    RETURNS jsonb
    LANGUAGE 'sql'
    COST 100
    STABLE PARALLEL UNSAFE
AS $BODY$


SELECT
			jsonb_build_object(
				lang,jsonb_build_object('value',t.content,'engine',t.engine)
			)

from "ContentItemTranslation" as t
WHERE 
		t."contentItemUid" = c.uid
		AND 
		t."targetLanguage" = lang
$BODY$;



CREATE OR REPLACE FUNCTION public."ContentItem_title_translations"(
	c "ContentItem")
    RETURNS jsonb
    LANGUAGE 'sql'
    COST 100
    STABLE PARALLEL UNSAFE
AS $BODY$

SELECT 


jsonb_merge_recurse(
			jsonb_build_object(
				'de',jsonb_build_object('value',de.title,'engine',de.engine),
				'en',jsonb_build_object('value',en.title,'engine',en.engine),
				'fr',jsonb_build_object('value',fr.title,'engine',fr.engine),
				'es',jsonb_build_object('value',es.title,'engine',es.engine),
				'it',jsonb_build_object('value',it.title,'engine',it.engine),
				'pl',jsonb_build_object('value',pl.title,'engine',pl.engine),
				'pt',jsonb_build_object('value',pt.title,'engine',pt.engine),
				'nl',jsonb_build_object('value',nl.title,'engine',nl.engine),
				'cs',jsonb_build_object('value',cs.title,'engine',cs.engine),
				'hu',jsonb_build_object('value',hu.title,'engine',hu.engine),
				'ru',jsonb_build_object('value',ru.title,'engine',ru.engine),
				'sk',jsonb_build_object('value',sk.title,'engine',sk.engine),
				'tr',jsonb_build_object('value',tr.title,'engine',tr.engine),
				'uk',jsonb_build_object('value',uk.title,'engine',uk.engine),
				'hr',jsonb_build_object('value',hr.title,'engine',hr.engine),
				'ro',jsonb_build_object('value',ro.title,'engine',ro.engine),
				'bg',jsonb_build_object('value',bg.title,'engine',bg.engine),
				'fi',jsonb_build_object('value',fi.title,'engine',fi.engine),
				'sv',jsonb_build_object('value',sv.title,'engine',sv.engine),
				'el',jsonb_build_object('value',el.title,'engine',el.engine)
			),
			c.title
		)

FROM 
"ContentItem"
LEFT JOIN "ContentItemTranslation" as de ON (c.uid = de."contentItemUid" AND de."targetLanguage" = 'de' )
LEFT JOIN "ContentItemTranslation" as en ON (c.uid = en."contentItemUid" AND en."targetLanguage" = 'en' )
LEFT JOIN "ContentItemTranslation" as fr ON (c.uid = fr."contentItemUid" AND fr."targetLanguage" = 'fr' )
LEFT JOIN "ContentItemTranslation" as es ON (c.uid = es."contentItemUid" AND es."targetLanguage" = 'es' )
LEFT JOIN "ContentItemTranslation" as it ON (c.uid = it."contentItemUid" AND it."targetLanguage" = 'it' )
LEFT JOIN "ContentItemTranslation" as pl ON (c.uid = pl."contentItemUid" AND pl."targetLanguage" = 'pl' )
LEFT JOIN "ContentItemTranslation" as pt ON (c.uid = pt."contentItemUid" AND pt."targetLanguage" = 'pt' )
LEFT JOIN "ContentItemTranslation" as nl ON (c.uid = nl."contentItemUid" AND nl."targetLanguage" = 'nl' )
LEFT JOIN "ContentItemTranslation" as cs ON (c.uid = cs."contentItemUid" AND cs."targetLanguage" = 'cs' )
LEFT JOIN "ContentItemTranslation" as hu ON (c.uid = hu."contentItemUid" AND hu."targetLanguage" = 'hu' )
LEFT JOIN "ContentItemTranslation" as ru ON (c.uid = ru."contentItemUid" AND ru."targetLanguage" = 'ru' )
LEFT JOIN "ContentItemTranslation" as sk ON (c.uid = sk."contentItemUid" AND sk."targetLanguage" = 'sk' )
LEFT JOIN "ContentItemTranslation" as tr ON (c.uid = tr."contentItemUid" AND tr."targetLanguage" = 'tr' )
LEFT JOIN "ContentItemTranslation" as uk ON (c.uid = uk."contentItemUid" AND uk."targetLanguage" = 'uk' )
LEFT JOIN "ContentItemTranslation" as hr ON (c.uid = hr."contentItemUid" AND hr."targetLanguage" = 'hr' )
LEFT JOIN "ContentItemTranslation" as ro ON (c.uid = ro."contentItemUid" AND ro."targetLanguage" = 'ro' )
LEFT JOIN "ContentItemTranslation" as bg ON (c.uid = bg."contentItemUid" AND bg."targetLanguage" = 'bg' )
LEFT JOIN "ContentItemTranslation" as fi ON (c.uid = fi."contentItemUid" AND fi."targetLanguage" = 'fi' )
LEFT JOIN "ContentItemTranslation" as sv ON (c.uid = sv."contentItemUid" AND sv."targetLanguage" = 'sv' )
LEFT JOIN "ContentItemTranslation" as el ON (c.uid = el."contentItemUid" AND el."targetLanguage" = 'el' )


$BODY$;


CREATE OR REPLACE FUNCTION public."ContentItem_subtitle_translations"(
	c "ContentItem")
    RETURNS jsonb
    LANGUAGE 'sql'
    COST 100
    STABLE PARALLEL UNSAFE
AS $BODY$

SELECT 
jsonb_merge_recurse(
			jsonb_build_object(
				'de',jsonb_build_object('value',de.subtitle,'engine',de.engine),
				'en',jsonb_build_object('value',en.subtitle,'engine',en.engine),
				'fr',jsonb_build_object('value',fr.subtitle,'engine',fr.engine),
				'es',jsonb_build_object('value',es.subtitle,'engine',es.engine),
				'it',jsonb_build_object('value',it.subtitle,'engine',it.engine),
				'pl',jsonb_build_object('value',pl.subtitle,'engine',pl.engine),
				'pt',jsonb_build_object('value',pt.subtitle,'engine',pt.engine),
				'nl',jsonb_build_object('value',nl.subtitle,'engine',nl.engine),
				'cs',jsonb_build_object('value',cs.subtitle,'engine',cs.engine),
				'hu',jsonb_build_object('value',hu.subtitle,'engine',hu.engine),
				'ru',jsonb_build_object('value',ru.subtitle,'engine',ru.engine),
				'sk',jsonb_build_object('value',sk.subtitle,'engine',sk.engine),
				'tr',jsonb_build_object('value',tr.subtitle,'engine',tr.engine),
				'uk',jsonb_build_object('value',uk.subtitle,'engine',uk.engine),
				'hr',jsonb_build_object('value',hr.subtitle,'engine',hr.engine),
				'ro',jsonb_build_object('value',ro.subtitle,'engine',ro.engine),
				'bg',jsonb_build_object('value',bg.subtitle,'engine',bg.engine),
				'fi',jsonb_build_object('value',fi.subtitle,'engine',fi.engine),
				'sv',jsonb_build_object('value',sv.subtitle,'engine',sv.engine),
				'el',jsonb_build_object('value',el.subtitle,'engine',el.engine)
			),
			c.subtitle
		)

FROM 
"ContentItem"
LEFT JOIN "ContentItemTranslation" as de ON (c.uid = de."contentItemUid" AND de."targetLanguage" = 'de' )
LEFT JOIN "ContentItemTranslation" as en ON (c.uid = en."contentItemUid" AND en."targetLanguage" = 'en' )
LEFT JOIN "ContentItemTranslation" as fr ON (c.uid = fr."contentItemUid" AND fr."targetLanguage" = 'fr' )
LEFT JOIN "ContentItemTranslation" as es ON (c.uid = es."contentItemUid" AND es."targetLanguage" = 'es' )
LEFT JOIN "ContentItemTranslation" as it ON (c.uid = it."contentItemUid" AND it."targetLanguage" = 'it' )
LEFT JOIN "ContentItemTranslation" as pl ON (c.uid = pl."contentItemUid" AND pl."targetLanguage" = 'pl' )
LEFT JOIN "ContentItemTranslation" as pt ON (c.uid = pt."contentItemUid" AND pt."targetLanguage" = 'pt' )
LEFT JOIN "ContentItemTranslation" as nl ON (c.uid = nl."contentItemUid" AND nl."targetLanguage" = 'nl' )
LEFT JOIN "ContentItemTranslation" as cs ON (c.uid = cs."contentItemUid" AND cs."targetLanguage" = 'cs' )
LEFT JOIN "ContentItemTranslation" as hu ON (c.uid = hu."contentItemUid" AND hu."targetLanguage" = 'hu' )
LEFT JOIN "ContentItemTranslation" as ru ON (c.uid = ru."contentItemUid" AND ru."targetLanguage" = 'ru' )
LEFT JOIN "ContentItemTranslation" as sk ON (c.uid = sk."contentItemUid" AND sk."targetLanguage" = 'sk' )
LEFT JOIN "ContentItemTranslation" as tr ON (c.uid = tr."contentItemUid" AND tr."targetLanguage" = 'tr' )
LEFT JOIN "ContentItemTranslation" as uk ON (c.uid = uk."contentItemUid" AND uk."targetLanguage" = 'uk' )
LEFT JOIN "ContentItemTranslation" as hr ON (c.uid = hr."contentItemUid" AND hr."targetLanguage" = 'hr' )
LEFT JOIN "ContentItemTranslation" as ro ON (c.uid = ro."contentItemUid" AND ro."targetLanguage" = 'ro' )
LEFT JOIN "ContentItemTranslation" as bg ON (c.uid = bg."contentItemUid" AND bg."targetLanguage" = 'bg' )
LEFT JOIN "ContentItemTranslation" as fi ON (c.uid = fi."contentItemUid" AND fi."targetLanguage" = 'fi' )
LEFT JOIN "ContentItemTranslation" as sv ON (c.uid = sv."contentItemUid" AND sv."targetLanguage" = 'sv' )
LEFT JOIN "ContentItemTranslation" as el ON (c.uid = el."contentItemUid" AND el."targetLanguage" = 'el' )


$BODY$;



CREATE OR REPLACE FUNCTION public."ContentItem_summary_translations"(
	c "ContentItem")
    RETURNS jsonb
    LANGUAGE 'sql'
    COST 100
    STABLE PARALLEL UNSAFE
AS $BODY$

SELECT 
jsonb_merge_recurse(
			jsonb_build_object(
				'de',jsonb_build_object('value',de.summary,'engine',de.engine),
				'en',jsonb_build_object('value',en.summary,'engine',en.engine),
				'fr',jsonb_build_object('value',fr.summary,'engine',fr.engine),
				'es',jsonb_build_object('value',es.summary,'engine',es.engine),
				'it',jsonb_build_object('value',it.summary,'engine',it.engine),
				'pl',jsonb_build_object('value',pl.summary,'engine',pl.engine),
				'pt',jsonb_build_object('value',pt.summary,'engine',pt.engine),
				'nl',jsonb_build_object('value',nl.summary,'engine',nl.engine),
				'cs',jsonb_build_object('value',cs.summary,'engine',cs.engine),
				'hu',jsonb_build_object('value',hu.summary,'engine',hu.engine),
				'ru',jsonb_build_object('value',ru.summary,'engine',ru.engine),
				'sk',jsonb_build_object('value',sk.summary,'engine',sk.engine),
				'tr',jsonb_build_object('value',tr.summary,'engine',tr.engine),
				'uk',jsonb_build_object('value',uk.summary,'engine',uk.engine),
				'hr',jsonb_build_object('value',hr.summary,'engine',hr.engine),
				'ro',jsonb_build_object('value',ro.summary,'engine',ro.engine),
				'bg',jsonb_build_object('value',bg.summary,'engine',bg.engine),
				'fi',jsonb_build_object('value',fi.summary,'engine',fi.engine),
				'sv',jsonb_build_object('value',sv.summary,'engine',sv.engine),
				'el',jsonb_build_object('value',el.summary,'engine',el.engine)
			),
			c.summary
		)

FROM 
"ContentItem"
LEFT JOIN "ContentItemTranslation" as de ON (c.uid = de."contentItemUid" AND de."targetLanguage" = 'de' )
LEFT JOIN "ContentItemTranslation" as en ON (c.uid = en."contentItemUid" AND en."targetLanguage" = 'en' )
LEFT JOIN "ContentItemTranslation" as fr ON (c.uid = fr."contentItemUid" AND fr."targetLanguage" = 'fr' )
LEFT JOIN "ContentItemTranslation" as es ON (c.uid = es."contentItemUid" AND es."targetLanguage" = 'es' )
LEFT JOIN "ContentItemTranslation" as it ON (c.uid = it."contentItemUid" AND it."targetLanguage" = 'it' )
LEFT JOIN "ContentItemTranslation" as pl ON (c.uid = pl."contentItemUid" AND pl."targetLanguage" = 'pl' )
LEFT JOIN "ContentItemTranslation" as pt ON (c.uid = pt."contentItemUid" AND pt."targetLanguage" = 'pt' )
LEFT JOIN "ContentItemTranslation" as nl ON (c.uid = nl."contentItemUid" AND nl."targetLanguage" = 'nl' )
LEFT JOIN "ContentItemTranslation" as cs ON (c.uid = cs."contentItemUid" AND cs."targetLanguage" = 'cs' )
LEFT JOIN "ContentItemTranslation" as hu ON (c.uid = hu."contentItemUid" AND hu."targetLanguage" = 'hu' )
LEFT JOIN "ContentItemTranslation" as ru ON (c.uid = ru."contentItemUid" AND ru."targetLanguage" = 'ru' )
LEFT JOIN "ContentItemTranslation" as sk ON (c.uid = sk."contentItemUid" AND sk."targetLanguage" = 'sk' )
LEFT JOIN "ContentItemTranslation" as tr ON (c.uid = tr."contentItemUid" AND tr."targetLanguage" = 'tr' )
LEFT JOIN "ContentItemTranslation" as uk ON (c.uid = uk."contentItemUid" AND uk."targetLanguage" = 'uk' )
LEFT JOIN "ContentItemTranslation" as hr ON (c.uid = hr."contentItemUid" AND hr."targetLanguage" = 'hr' )
LEFT JOIN "ContentItemTranslation" as ro ON (c.uid = ro."contentItemUid" AND ro."targetLanguage" = 'ro' )
LEFT JOIN "ContentItemTranslation" as bg ON (c.uid = bg."contentItemUid" AND bg."targetLanguage" = 'bg' )
LEFT JOIN "ContentItemTranslation" as fi ON (c.uid = fi."contentItemUid" AND fi."targetLanguage" = 'fi' )
LEFT JOIN "ContentItemTranslation" as sv ON (c.uid = sv."contentItemUid" AND sv."targetLanguage" = 'sv' )
LEFT JOIN "ContentItemTranslation" as el ON (c.uid = el."contentItemUid" AND el."targetLanguage" = 'el' )


$BODY$;



CREATE OR REPLACE FUNCTION public."ContentItem_content_translations"(
	c "ContentItem")
    RETURNS jsonb
    LANGUAGE 'sql'
    COST 100
    STABLE PARALLEL UNSAFE
AS $BODY$

SELECT 
jsonb_merge_recurse(
			jsonb_build_object(
				'de',jsonb_build_object('value',de.content,'engine',de.engine),
				'en',jsonb_build_object('value',en.content,'engine',en.engine),
				'fr',jsonb_build_object('value',fr.content,'engine',fr.engine),
				'es',jsonb_build_object('value',es.content,'engine',es.engine),
				'it',jsonb_build_object('value',it.content,'engine',it.engine),
				'pl',jsonb_build_object('value',pl.content,'engine',pl.engine),
				'pt',jsonb_build_object('value',pt.content,'engine',pt.engine),
				'nl',jsonb_build_object('value',nl.content,'engine',nl.engine),
				'cs',jsonb_build_object('value',cs.content,'engine',cs.engine),
				'hu',jsonb_build_object('value',hu.content,'engine',hu.engine),
				'ru',jsonb_build_object('value',ru.content,'engine',ru.engine),
				'sk',jsonb_build_object('value',sk.content,'engine',sk.engine),
				'tr',jsonb_build_object('value',tr.content,'engine',tr.engine),
				'uk',jsonb_build_object('value',uk.content,'engine',uk.engine),
				'hr',jsonb_build_object('value',hr.content,'engine',hr.engine),
				'ro',jsonb_build_object('value',ro.content,'engine',ro.engine),
				'bg',jsonb_build_object('value',bg.content,'engine',bg.engine),
				'fi',jsonb_build_object('value',fi.content,'engine',fi.engine),
				'sv',jsonb_build_object('value',sv.content,'engine',sv.engine),
				'el',jsonb_build_object('value',el.content,'engine',el.engine)
			),
			c.content
		)

FROM 
"ContentItem"
LEFT JOIN "ContentItemTranslation" as de ON (c.uid = de."contentItemUid" AND de."targetLanguage" = 'en' )
LEFT JOIN "ContentItemTranslation" as en ON (c.uid = en."contentItemUid" AND en."targetLanguage" = 'en' )
LEFT JOIN "ContentItemTranslation" as fr ON (c.uid = fr."contentItemUid" AND fr."targetLanguage" = 'fr' )
LEFT JOIN "ContentItemTranslation" as es ON (c.uid = es."contentItemUid" AND es."targetLanguage" = 'es' )
LEFT JOIN "ContentItemTranslation" as it ON (c.uid = it."contentItemUid" AND it."targetLanguage" = 'it' )
LEFT JOIN "ContentItemTranslation" as pl ON (c.uid = pl."contentItemUid" AND pl."targetLanguage" = 'pl' )
LEFT JOIN "ContentItemTranslation" as pt ON (c.uid = pt."contentItemUid" AND pt."targetLanguage" = 'pt' )
LEFT JOIN "ContentItemTranslation" as nl ON (c.uid = nl."contentItemUid" AND nl."targetLanguage" = 'nl' )
LEFT JOIN "ContentItemTranslation" as cs ON (c.uid = cs."contentItemUid" AND cs."targetLanguage" = 'cs' )
LEFT JOIN "ContentItemTranslation" as hu ON (c.uid = hu."contentItemUid" AND hu."targetLanguage" = 'hu' )
LEFT JOIN "ContentItemTranslation" as ru ON (c.uid = ru."contentItemUid" AND ru."targetLanguage" = 'ru' )
LEFT JOIN "ContentItemTranslation" as sk ON (c.uid = sk."contentItemUid" AND sk."targetLanguage" = 'sk' )
LEFT JOIN "ContentItemTranslation" as tr ON (c.uid = tr."contentItemUid" AND tr."targetLanguage" = 'tr' )
LEFT JOIN "ContentItemTranslation" as uk ON (c.uid = uk."contentItemUid" AND uk."targetLanguage" = 'uk' )
LEFT JOIN "ContentItemTranslation" as hr ON (c.uid = hr."contentItemUid" AND hr."targetLanguage" = 'hr' )
LEFT JOIN "ContentItemTranslation" as ro ON (c.uid = ro."contentItemUid" AND ro."targetLanguage" = 'ro' )
LEFT JOIN "ContentItemTranslation" as bg ON (c.uid = bg."contentItemUid" AND bg."targetLanguage" = 'bg' )
LEFT JOIN "ContentItemTranslation" as fi ON (c.uid = fi."contentItemUid" AND fi."targetLanguage" = 'fi' )
LEFT JOIN "ContentItemTranslation" as sv ON (c.uid = sv."contentItemUid" AND sv."targetLanguage" = 'sv' )
LEFT JOIN "ContentItemTranslation" as el ON (c.uid = el."contentItemUid" AND el."targetLanguage" = 'el' )


$BODY$;




--DROP FUNCTION "ContentItem_get_translation_detail"("ContentItem");
CREATE OR REPLACE FUNCTION public."ContentItem_LanguageDetails"(
	c "ContentItem")
    RETURNS jsonb
    LANGUAGE 'sql'
    COST 100
    STABLE PARALLEL UNSAFE
AS $BODY$
SELECT
	jsonb_build_object(
		'ContentItemTranslations',
		(
		SELECT (COUNT (*)::text)::jsonb as count
		from "ContentItemTranslation" as t
		WHERE 
				t."contentItemUid" = c.uid
		GROUP BY t."contentItemUid"
		),
		'ContentItemLangauges',
		(SELECT (COUNT (*)::text)::jsonb as count 
		FROM
		jsonb_object_keys(c.title)
		),
		'OriginalLangauges',
		(SELECT (COUNT (*)::text)::jsonb as count 
		FROM
		jsonb_object_keys(c."originalLanguages")
		)
)

$BODY$;















