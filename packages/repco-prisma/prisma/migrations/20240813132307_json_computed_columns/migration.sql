CREATE OR REPLACE FUNCTION "ContentItem_title_string"("ContentItem" "ContentItem") RETURNS text AS $$
SELECT "ContentItem".title::text
$$ LANGUAGE sql STABLE;

CREATE OR REPLACE FUNCTION "ContentItem_content_string"("ContentItem" "ContentItem") RETURNS text AS $$
SELECT "ContentItem".content::text
$$ LANGUAGE sql STABLE;

CREATE OR REPLACE FUNCTION "ContentItem_summary_string"("ContentItem" "ContentItem") RETURNS text AS $$
SELECT "ContentItem".summary::text
$$ LANGUAGE sql STABLE;

CREATE OR REPLACE FUNCTION "ContentGrouping_description_string"("ContentGrouping" "ContentGrouping") RETURNS text AS $$
SELECT "ContentGrouping".description::text
$$ LANGUAGE sql STABLE;

CREATE OR REPLACE FUNCTION "ContentGrouping_summary_string"("ContentGrouping" "ContentGrouping") RETURNS text AS $$
SELECT "ContentGrouping".summary::text
$$ LANGUAGE sql STABLE;

CREATE OR REPLACE FUNCTION "ContentGrouping_title_string"("ContentGrouping" "ContentGrouping") RETURNS text AS $$
SELECT "ContentGrouping".title::text
$$ LANGUAGE sql STABLE;

CREATE OR REPLACE FUNCTION "MediaAsset_title_string"("MediaAsset" "MediaAsset") RETURNS text AS $$
SELECT "MediaAsset".title::text
$$ LANGUAGE sql STABLE;

CREATE OR REPLACE FUNCTION "MediaAsset_description_string"("MediaAsset" "MediaAsset") RETURNS text AS $$
SELECT "MediaAsset".description::text
$$ LANGUAGE sql STABLE;

CREATE OR REPLACE FUNCTION "Chapter_title_string"("Chapter" "Chapter") RETURNS text AS $$
SELECT "Chapter".title::text
$$ LANGUAGE sql STABLE;

CREATE OR REPLACE FUNCTION "PublicationService_name_string"("PublicationService" "PublicationService") RETURNS text AS $$
SELECT "PublicationService".name::text
$$ LANGUAGE sql STABLE;

CREATE OR REPLACE FUNCTION "Concept_name_string"("Concept" "Concept") RETURNS text AS $$
SELECT "Concept".name::text
$$ LANGUAGE sql STABLE;

CREATE OR REPLACE FUNCTION "Concept_description_string"("Concept" "Concept") RETURNS text AS $$
SELECT "Concept".description::text
$$ LANGUAGE sql STABLE;

CREATE OR REPLACE FUNCTION "Concept_summary_string"("Concept" "Concept") RETURNS text AS $$
SELECT "Concept".summary::text
$$ LANGUAGE sql STABLE;
