-- FUNCTION: public.jsonb_merge_recurse(jsonb, jsonb)

-- DROP FUNCTION IF EXISTS public.jsonb_merge_recurse(jsonb, jsonb);

CREATE OR REPLACE FUNCTION public.jsonb_merge_recurse(
	orig jsonb,
	delta jsonb)
    RETURNS jsonb
    LANGUAGE 'sql'
    COST 100
    VOLATILE PARALLEL UNSAFE
AS $BODY$
    select
        jsonb_object_agg(
            coalesce(keyOrig, keyDelta),
            case
                when valOrig isnull then valDelta
                when valDelta isnull then valOrig
                when (jsonb_typeof(valOrig) <> 'object' or jsonb_typeof(valDelta) <> 'object') then valDelta
                else jsonb_merge_recurse(valOrig, valDelta)
            end
        )
    from jsonb_each(orig) e1(keyOrig, valOrig)
    full join jsonb_each(delta) e2(keyDelta, valDelta) on keyOrig = keyDelta
$BODY$;

ALTER FUNCTION public.jsonb_merge_recurse(jsonb, jsonb)
    OWNER TO repco;



CREATE OR REPLACE FUNCTION public."ContentItem_title_translation"(
	c "ContentItem",lang text)
    RETURNS jsonb
    LANGUAGE 'sql'
    COST 100
    STABLE PARALLEL UNSAFE
AS $BODY$

SELECT
			jsonb_build_object(
				lang,jsonb_build_object('value',t.title,'engine',t.engine)
			)

from "ContentItemTranslation" as t
WHERE 
		t."contentItemUid" = c.uid
		AND 
		t."targetLanguage" = lang
$BODY$;

CREATE OR REPLACE FUNCTION public."ContentItem_subtitle_translation"(
	c "ContentItem",lang text)
    RETURNS jsonb
    LANGUAGE 'sql'
    COST 100
    STABLE PARALLEL UNSAFE
AS $BODY$


SELECT
			jsonb_build_object(
				lang,jsonb_build_object('value',t.subtitle,'engine',t.engine)
			)

from "ContentItemTranslation" as t
WHERE 
		t."contentItemUid" = c.uid
		AND 
		t."targetLanguage" = lang
$BODY$;


CREATE OR REPLACE FUNCTION public."ContentItem_summary_translation"(
	c "ContentItem",lang text)
    RETURNS jsonb
    LANGUAGE 'sql'
    COST 100
    STABLE PARALLEL UNSAFE
AS $BODY$


SELECT
			jsonb_build_object(
				lang,jsonb_build_object('value',t.summary,'engine',t.engine)
			)

from "ContentItemTranslation" as t
WHERE 
		t."contentItemUid" = c.uid
		AND 
		t."targetLanguage" = lang
$BODY$;

CREATE OR REPLACE FUNCTION public."ContentItem_content_translation"(
	c "ContentItem",lang text)
    RETURNS jsonb
    LANGUAGE 'sql'
    COST 100
    STABLE PARALLEL UNSAFE
AS $BODY$


SELECT
			jsonb_build_object(
				lang,jsonb_build_object('value',t.content,'engine',t.engine)
			)

from "ContentItemTranslation" as t
WHERE 
		t."contentItemUid" = c.uid
		AND 
		t."targetLanguage" = lang
$BODY$;



CREATE OR REPLACE FUNCTION public."ContentItem_title_translations"(
	c "ContentItem")
    RETURNS jsonb
    LANGUAGE 'sql'
    COST 100
    STABLE PARALLEL UNSAFE
AS $BODY$

SELECT 


jsonb_merge_recurse(
			jsonb_build_object(
				'de',jsonb_build_object('value',de.title,'engine',de.engine),
				'en',jsonb_build_object('value',en.title,'engine',en.engine),
				'fr',jsonb_build_object('value',fr.title,'engine',fr.engine),
				'es',jsonb_build_object('value',es.title,'engine',es.engine),
				'it',jsonb_build_object('value',it.title,'engine',it.engine),
				'pl',jsonb_build_object('value',pl.title,'engine',pl.engine),
				'pt',jsonb_build_object('value',pt.title,'engine',pt.engine),
				'nl',jsonb_build_object('value',nl.title,'engine',nl.engine),
				'cs',jsonb_build_object('value',cs.title,'engine',cs.engine),
				'hu',jsonb_build_object('value',hu.title,'engine',hu.engine),
				'ru',jsonb_build_object('value',ru.title,'engine',ru.engine),
				'sk',jsonb_build_object('value',sk.title,'engine',sk.engine),
				'tr',jsonb_build_object('value',tr.title,'engine',tr.engine),
				'uk',jsonb_build_object('value',uk.title,'engine',uk.engine),
				'hr',jsonb_build_object('value',hr.title,'engine',hr.engine),
				'ro',jsonb_build_object('value',ro.title,'engine',ro.engine),
				'bg',jsonb_build_object('value',bg.title,'engine',bg.engine),
				'fi',jsonb_build_object('value',fi.title,'engine',fi.engine),
				'sv',jsonb_build_object('value',sv.title,'engine',sv.engine),
				'el',jsonb_build_object('value',el.title,'engine',el.engine)
			),
			c.title
		)

FROM 
"ContentItem"
LEFT JOIN "ContentItemTranslation" as de ON (c.uid = de."contentItemUid" AND de."targetLanguage" = 'de' )
LEFT JOIN "ContentItemTranslation" as en ON (c.uid = en."contentItemUid" AND en."targetLanguage" = 'en' )
LEFT JOIN "ContentItemTranslation" as fr ON (c.uid = fr."contentItemUid" AND fr."targetLanguage" = 'fr' )
LEFT JOIN "ContentItemTranslation" as es ON (c.uid = es."contentItemUid" AND es."targetLanguage" = 'es' )
LEFT JOIN "ContentItemTranslation" as it ON (c.uid = it."contentItemUid" AND it."targetLanguage" = 'it' )
LEFT JOIN "ContentItemTranslation" as pl ON (c.uid = pl."contentItemUid" AND pl."targetLanguage" = 'pl' )
LEFT JOIN "ContentItemTranslation" as pt ON (c.uid = pt."contentItemUid" AND pt."targetLanguage" = 'pt' )
LEFT JOIN "ContentItemTranslation" as nl ON (c.uid = nl."contentItemUid" AND nl."targetLanguage" = 'nl' )
LEFT JOIN "ContentItemTranslation" as cs ON (c.uid = cs."contentItemUid" AND cs."targetLanguage" = 'cs' )
LEFT JOIN "ContentItemTranslation" as hu ON (c.uid = hu."contentItemUid" AND hu."targetLanguage" = 'hu' )
LEFT JOIN "ContentItemTranslation" as ru ON (c.uid = ru."contentItemUid" AND ru."targetLanguage" = 'ru' )
LEFT JOIN "ContentItemTranslation" as sk ON (c.uid = sk."contentItemUid" AND sk."targetLanguage" = 'sk' )
LEFT JOIN "ContentItemTranslation" as tr ON (c.uid = tr."contentItemUid" AND tr."targetLanguage" = 'tr' )
LEFT JOIN "ContentItemTranslation" as uk ON (c.uid = uk."contentItemUid" AND uk."targetLanguage" = 'uk' )
LEFT JOIN "ContentItemTranslation" as hr ON (c.uid = hr."contentItemUid" AND hr."targetLanguage" = 'hr' )
LEFT JOIN "ContentItemTranslation" as ro ON (c.uid = ro."contentItemUid" AND ro."targetLanguage" = 'ro' )
LEFT JOIN "ContentItemTranslation" as bg ON (c.uid = bg."contentItemUid" AND bg."targetLanguage" = 'bg' )
LEFT JOIN "ContentItemTranslation" as fi ON (c.uid = fi."contentItemUid" AND fi."targetLanguage" = 'fi' )
LEFT JOIN "ContentItemTranslation" as sv ON (c.uid = sv."contentItemUid" AND sv."targetLanguage" = 'sv' )
LEFT JOIN "ContentItemTranslation" as el ON (c.uid = el."contentItemUid" AND el."targetLanguage" = 'el' )


$BODY$;


CREATE OR REPLACE FUNCTION public."ContentItem_subtitle_translations"(
	c "ContentItem")
    RETURNS jsonb
    LANGUAGE 'sql'
    COST 100
    STABLE PARALLEL UNSAFE
AS $BODY$

SELECT 
jsonb_merge_recurse(
			jsonb_build_object(
				'de',jsonb_build_object('value',de.subtitle,'engine',de.engine),
				'en',jsonb_build_object('value',en.subtitle,'engine',en.engine),
				'fr',jsonb_build_object('value',fr.subtitle,'engine',fr.engine),
				'es',jsonb_build_object('value',es.subtitle,'engine',es.engine),
				'it',jsonb_build_object('value',it.subtitle,'engine',it.engine),
				'pl',jsonb_build_object('value',pl.subtitle,'engine',pl.engine),
				'pt',jsonb_build_object('value',pt.subtitle,'engine',pt.engine),
				'nl',jsonb_build_object('value',nl.subtitle,'engine',nl.engine),
				'cs',jsonb_build_object('value',cs.subtitle,'engine',cs.engine),
				'hu',jsonb_build_object('value',hu.subtitle,'engine',hu.engine),
				'ru',jsonb_build_object('value',ru.subtitle,'engine',ru.engine),
				'sk',jsonb_build_object('value',sk.subtitle,'engine',sk.engine),
				'tr',jsonb_build_object('value',tr.subtitle,'engine',tr.engine),
				'uk',jsonb_build_object('value',uk.subtitle,'engine',uk.engine),
				'hr',jsonb_build_object('value',hr.subtitle,'engine',hr.engine),
				'ro',jsonb_build_object('value',ro.subtitle,'engine',ro.engine),
				'bg',jsonb_build_object('value',bg.subtitle,'engine',bg.engine),
				'fi',jsonb_build_object('value',fi.subtitle,'engine',fi.engine),
				'sv',jsonb_build_object('value',sv.subtitle,'engine',sv.engine),
				'el',jsonb_build_object('value',el.subtitle,'engine',el.engine)
			),
			c.subtitle
		)

FROM 
"ContentItem"
LEFT JOIN "ContentItemTranslation" as de ON (c.uid = de."contentItemUid" AND de."targetLanguage" = 'de' )
LEFT JOIN "ContentItemTranslation" as en ON (c.uid = en."contentItemUid" AND en."targetLanguage" = 'en' )
LEFT JOIN "ContentItemTranslation" as fr ON (c.uid = fr."contentItemUid" AND fr."targetLanguage" = 'fr' )
LEFT JOIN "ContentItemTranslation" as es ON (c.uid = es."contentItemUid" AND es."targetLanguage" = 'es' )
LEFT JOIN "ContentItemTranslation" as it ON (c.uid = it."contentItemUid" AND it."targetLanguage" = 'it' )
LEFT JOIN "ContentItemTranslation" as pl ON (c.uid = pl."contentItemUid" AND pl."targetLanguage" = 'pl' )
LEFT JOIN "ContentItemTranslation" as pt ON (c.uid = pt."contentItemUid" AND pt."targetLanguage" = 'pt' )
LEFT JOIN "ContentItemTranslation" as nl ON (c.uid = nl."contentItemUid" AND nl."targetLanguage" = 'nl' )
LEFT JOIN "ContentItemTranslation" as cs ON (c.uid = cs."contentItemUid" AND cs."targetLanguage" = 'cs' )
LEFT JOIN "ContentItemTranslation" as hu ON (c.uid = hu."contentItemUid" AND hu."targetLanguage" = 'hu' )
LEFT JOIN "ContentItemTranslation" as ru ON (c.uid = ru."contentItemUid" AND ru."targetLanguage" = 'ru' )
LEFT JOIN "ContentItemTranslation" as sk ON (c.uid = sk."contentItemUid" AND sk."targetLanguage" = 'sk' )
LEFT JOIN "ContentItemTranslation" as tr ON (c.uid = tr."contentItemUid" AND tr."targetLanguage" = 'tr' )
LEFT JOIN "ContentItemTranslation" as uk ON (c.uid = uk."contentItemUid" AND uk."targetLanguage" = 'uk' )
LEFT JOIN "ContentItemTranslation" as hr ON (c.uid = hr."contentItemUid" AND hr."targetLanguage" = 'hr' )
LEFT JOIN "ContentItemTranslation" as ro ON (c.uid = ro."contentItemUid" AND ro."targetLanguage" = 'ro' )
LEFT JOIN "ContentItemTranslation" as bg ON (c.uid = bg."contentItemUid" AND bg."targetLanguage" = 'bg' )
LEFT JOIN "ContentItemTranslation" as fi ON (c.uid = fi."contentItemUid" AND fi."targetLanguage" = 'fi' )
LEFT JOIN "ContentItemTranslation" as sv ON (c.uid = sv."contentItemUid" AND sv."targetLanguage" = 'sv' )
LEFT JOIN "ContentItemTranslation" as el ON (c.uid = el."contentItemUid" AND el."targetLanguage" = 'el' )


$BODY$;



CREATE OR REPLACE FUNCTION public."ContentItem_summary_translations"(
	c "ContentItem")
    RETURNS jsonb
    LANGUAGE 'sql'
    COST 100
    STABLE PARALLEL UNSAFE
AS $BODY$

SELECT 
jsonb_merge_recurse(
			jsonb_build_object(
				'de',jsonb_build_object('value',de.summary,'engine',de.engine),
				'en',jsonb_build_object('value',en.summary,'engine',en.engine),
				'fr',jsonb_build_object('value',fr.summary,'engine',fr.engine),
				'es',jsonb_build_object('value',es.summary,'engine',es.engine),
				'it',jsonb_build_object('value',it.summary,'engine',it.engine),
				'pl',jsonb_build_object('value',pl.summary,'engine',pl.engine),
				'pt',jsonb_build_object('value',pt.summary,'engine',pt.engine),
				'nl',jsonb_build_object('value',nl.summary,'engine',nl.engine),
				'cs',jsonb_build_object('value',cs.summary,'engine',cs.engine),
				'hu',jsonb_build_object('value',hu.summary,'engine',hu.engine),
				'ru',jsonb_build_object('value',ru.summary,'engine',ru.engine),
				'sk',jsonb_build_object('value',sk.summary,'engine',sk.engine),
				'tr',jsonb_build_object('value',tr.summary,'engine',tr.engine),
				'uk',jsonb_build_object('value',uk.summary,'engine',uk.engine),
				'hr',jsonb_build_object('value',hr.summary,'engine',hr.engine),
				'ro',jsonb_build_object('value',ro.summary,'engine',ro.engine),
				'bg',jsonb_build_object('value',bg.summary,'engine',bg.engine),
				'fi',jsonb_build_object('value',fi.summary,'engine',fi.engine),
				'sv',jsonb_build_object('value',sv.summary,'engine',sv.engine),
				'el',jsonb_build_object('value',el.summary,'engine',el.engine)
			),
			c.summary
		)

FROM 
"ContentItem"
LEFT JOIN "ContentItemTranslation" as de ON (c.uid = de."contentItemUid" AND de."targetLanguage" = 'de' )
LEFT JOIN "ContentItemTranslation" as en ON (c.uid = en."contentItemUid" AND en."targetLanguage" = 'en' )
LEFT JOIN "ContentItemTranslation" as fr ON (c.uid = fr."contentItemUid" AND fr."targetLanguage" = 'fr' )
LEFT JOIN "ContentItemTranslation" as es ON (c.uid = es."contentItemUid" AND es."targetLanguage" = 'es' )
LEFT JOIN "ContentItemTranslation" as it ON (c.uid = it."contentItemUid" AND it."targetLanguage" = 'it' )
LEFT JOIN "ContentItemTranslation" as pl ON (c.uid = pl."contentItemUid" AND pl."targetLanguage" = 'pl' )
LEFT JOIN "ContentItemTranslation" as pt ON (c.uid = pt."contentItemUid" AND pt."targetLanguage" = 'pt' )
LEFT JOIN "ContentItemTranslation" as nl ON (c.uid = nl."contentItemUid" AND nl."targetLanguage" = 'nl' )
LEFT JOIN "ContentItemTranslation" as cs ON (c.uid = cs."contentItemUid" AND cs."targetLanguage" = 'cs' )
LEFT JOIN "ContentItemTranslation" as hu ON (c.uid = hu."contentItemUid" AND hu."targetLanguage" = 'hu' )
LEFT JOIN "ContentItemTranslation" as ru ON (c.uid = ru."contentItemUid" AND ru."targetLanguage" = 'ru' )
LEFT JOIN "ContentItemTranslation" as sk ON (c.uid = sk."contentItemUid" AND sk."targetLanguage" = 'sk' )
LEFT JOIN "ContentItemTranslation" as tr ON (c.uid = tr."contentItemUid" AND tr."targetLanguage" = 'tr' )
LEFT JOIN "ContentItemTranslation" as uk ON (c.uid = uk."contentItemUid" AND uk."targetLanguage" = 'uk' )
LEFT JOIN "ContentItemTranslation" as hr ON (c.uid = hr."contentItemUid" AND hr."targetLanguage" = 'hr' )
LEFT JOIN "ContentItemTranslation" as ro ON (c.uid = ro."contentItemUid" AND ro."targetLanguage" = 'ro' )
LEFT JOIN "ContentItemTranslation" as bg ON (c.uid = bg."contentItemUid" AND bg."targetLanguage" = 'bg' )
LEFT JOIN "ContentItemTranslation" as fi ON (c.uid = fi."contentItemUid" AND fi."targetLanguage" = 'fi' )
LEFT JOIN "ContentItemTranslation" as sv ON (c.uid = sv."contentItemUid" AND sv."targetLanguage" = 'sv' )
LEFT JOIN "ContentItemTranslation" as el ON (c.uid = el."contentItemUid" AND el."targetLanguage" = 'el' )


$BODY$;



CREATE OR REPLACE FUNCTION public."ContentItem_content_translations"(
	c "ContentItem")
    RETURNS jsonb
    LANGUAGE 'sql'
    COST 100
    STABLE PARALLEL UNSAFE
AS $BODY$

SELECT 
jsonb_merge_recurse(
			jsonb_build_object(
				'de',jsonb_build_object('value',de.content,'engine',de.engine),
				'en',jsonb_build_object('value',en.content,'engine',en.engine),
				'fr',jsonb_build_object('value',fr.content,'engine',fr.engine),
				'es',jsonb_build_object('value',es.content,'engine',es.engine),
				'it',jsonb_build_object('value',it.content,'engine',it.engine),
				'pl',jsonb_build_object('value',pl.content,'engine',pl.engine),
				'pt',jsonb_build_object('value',pt.content,'engine',pt.engine),
				'nl',jsonb_build_object('value',nl.content,'engine',nl.engine),
				'cs',jsonb_build_object('value',cs.content,'engine',cs.engine),
				'hu',jsonb_build_object('value',hu.content,'engine',hu.engine),
				'ru',jsonb_build_object('value',ru.content,'engine',ru.engine),
				'sk',jsonb_build_object('value',sk.content,'engine',sk.engine),
				'tr',jsonb_build_object('value',tr.content,'engine',tr.engine),
				'uk',jsonb_build_object('value',uk.content,'engine',uk.engine),
				'hr',jsonb_build_object('value',hr.content,'engine',hr.engine),
				'ro',jsonb_build_object('value',ro.content,'engine',ro.engine),
				'bg',jsonb_build_object('value',bg.content,'engine',bg.engine),
				'fi',jsonb_build_object('value',fi.content,'engine',fi.engine),
				'sv',jsonb_build_object('value',sv.content,'engine',sv.engine),
				'el',jsonb_build_object('value',el.content,'engine',el.engine)
			),
			c.content
		)

FROM 
"ContentItem"
LEFT JOIN "ContentItemTranslation" as de ON (c.uid = de."contentItemUid" AND de."targetLanguage" = 'en' )
LEFT JOIN "ContentItemTranslation" as en ON (c.uid = en."contentItemUid" AND en."targetLanguage" = 'en' )
LEFT JOIN "ContentItemTranslation" as fr ON (c.uid = fr."contentItemUid" AND fr."targetLanguage" = 'fr' )
LEFT JOIN "ContentItemTranslation" as es ON (c.uid = es."contentItemUid" AND es."targetLanguage" = 'es' )
LEFT JOIN "ContentItemTranslation" as it ON (c.uid = it."contentItemUid" AND it."targetLanguage" = 'it' )
LEFT JOIN "ContentItemTranslation" as pl ON (c.uid = pl."contentItemUid" AND pl."targetLanguage" = 'pl' )
LEFT JOIN "ContentItemTranslation" as pt ON (c.uid = pt."contentItemUid" AND pt."targetLanguage" = 'pt' )
LEFT JOIN "ContentItemTranslation" as nl ON (c.uid = nl."contentItemUid" AND nl."targetLanguage" = 'nl' )
LEFT JOIN "ContentItemTranslation" as cs ON (c.uid = cs."contentItemUid" AND cs."targetLanguage" = 'cs' )
LEFT JOIN "ContentItemTranslation" as hu ON (c.uid = hu."contentItemUid" AND hu."targetLanguage" = 'hu' )
LEFT JOIN "ContentItemTranslation" as ru ON (c.uid = ru."contentItemUid" AND ru."targetLanguage" = 'ru' )
LEFT JOIN "ContentItemTranslation" as sk ON (c.uid = sk."contentItemUid" AND sk."targetLanguage" = 'sk' )
LEFT JOIN "ContentItemTranslation" as tr ON (c.uid = tr."contentItemUid" AND tr."targetLanguage" = 'tr' )
LEFT JOIN "ContentItemTranslation" as uk ON (c.uid = uk."contentItemUid" AND uk."targetLanguage" = 'uk' )
LEFT JOIN "ContentItemTranslation" as hr ON (c.uid = hr."contentItemUid" AND hr."targetLanguage" = 'hr' )
LEFT JOIN "ContentItemTranslation" as ro ON (c.uid = ro."contentItemUid" AND ro."targetLanguage" = 'ro' )
LEFT JOIN "ContentItemTranslation" as bg ON (c.uid = bg."contentItemUid" AND bg."targetLanguage" = 'bg' )
LEFT JOIN "ContentItemTranslation" as fi ON (c.uid = fi."contentItemUid" AND fi."targetLanguage" = 'fi' )
LEFT JOIN "ContentItemTranslation" as sv ON (c.uid = sv."contentItemUid" AND sv."targetLanguage" = 'sv' )
LEFT JOIN "ContentItemTranslation" as el ON (c.uid = el."contentItemUid" AND el."targetLanguage" = 'el' )


$BODY$;




--DROP FUNCTION "ContentItem_get_translation_detail"("ContentItem");
CREATE OR REPLACE FUNCTION public."ContentItem_LanguageDetails"(
	c "ContentItem")
    RETURNS jsonb
    LANGUAGE 'sql'
    COST 100
    STABLE PARALLEL UNSAFE
AS $BODY$
SELECT
	jsonb_build_object(
		'ContentItemTranslations',
		(
		SELECT (COUNT (*)::text)::jsonb as count
		from "ContentItemTranslation" as t
		WHERE 
				t."contentItemUid" = c.uid
		GROUP BY t."contentItemUid"
		),
		'ContentItemLangauges',
		(SELECT (COUNT (*)::text)::jsonb as count 
		FROM
		jsonb_object_keys(c.title)
		),
		'OriginalLangauges',
		(SELECT (COUNT (*)::text)::jsonb as count 
		FROM
		jsonb_object_keys(c."originalLanguages")
		)
)

$BODY$;

-- FUNCTION: public.getmlbase(text)

-- DROP FUNCTION IF EXISTS public.getmlbase(text);

CREATE OR REPLACE FUNCTION public.getmlbase(
	lang text)
    RETURNS TABLE(uid text, revisionid text, queryparam text, title text, summary text, content text, originallanguages text, contenturl text, pubdate timestamp without time zone) 
    LANGUAGE 'sql'
    COST 100
    VOLATILE PARALLEL UNSAFE
    ROWS 1000

AS $BODY$
    SELECT 
	uid,
	"revisionId",
	$1 as param,
	TRIM(BOTH '"'::text FROM (((title ->> $1::text)::jsonb) -> 'value'::text)::text) AS title,
    TRIM(BOTH '"'::text FROM (((summary ->> $1::text)::jsonb) -> 'value'::text)::text) AS summary,
    TRIM(BOTH '"'::text FROM (((content ->> $1::text)::jsonb) -> 'value'::text)::text) AS content,
    TRIM(BOTH '"'::text FROM ((("originalLanguages" ->> 'language_codes'::text)::jsonb) -> 0)::text) AS "origlang",
	"contentUrl",
	"pubDate"
	FROM
	"ContentItem"
	WHERE 
	removed = false
	AND TRIM(BOTH '"'::text FROM ((("originalLanguages" ->> 'language_codes'::text)::jsonb) -> 0)::text) = $1
	;
$BODY$;


CREATE MATERIALIZED VIEW IF NOT EXISTS public."SourceRecordArchiveCount"
TABLESPACE pg_default
AS
 SELECT count(*) AS dup_cnt,
    "contentType",
    body,
    "sourceType",
    meta,
    "dataSourceUid",
    "sourceUri",
    "containedEntityUris"
   FROM "SourceRecordArchive"
  GROUP BY "contentType", body, "sourceType", meta, "dataSourceUid", "sourceUri", "containedEntityUris"
 HAVING count(*) > 1
WITH DATA;

CREATE MATERIALIZED VIEW IF NOT EXISTS public."SourceRecordStats"
TABLESPACE pg_default
AS
 SELECT count(*) AS dup_cnt,
    "contentType",
    "sourceType",
    meta,
    "dataSourceUid",
    "sourceUri",
    "containedEntityUris"
   FROM "SourceRecordArchive"
  GROUP BY "contentType", "sourceType", meta, "dataSourceUid", "sourceUri", "containedEntityUris"
 HAVING count(*) > 1
WITH DATA;

CREATE OR REPLACE VIEW public."Translation"
 AS
 SELECT "ContentItem".uid,
    jsonb_merge_recurse("ContentItem".title, jsonb_build_object('am', jsonb_build_object('value', am.title), 'ar', jsonb_build_object('value', ar.title), 'az', jsonb_build_object('value', az.title), 'be', jsonb_build_object('value', be.title), 'bg', jsonb_build_object('value', bg.title), 'bs', jsonb_build_object('value', bs.title), 'ca', jsonb_build_object('value', ca.title), 'ce', jsonb_build_object('value', ce.title), 'cs', jsonb_build_object('value', cs.title), 'cy', jsonb_build_object('value', cy.title), 'da', jsonb_build_object('value', da.title), 'de', jsonb_build_object('value', de.title), 'el', jsonb_build_object('value', el.title), 'en', jsonb_build_object('value', en.title), 'eo', jsonb_build_object('value', eo.title), 'es', jsonb_build_object('value', es.title), 'et', jsonb_build_object('value', et.title), 'fa', jsonb_build_object('value', fa.title), 'fi', jsonb_build_object('value', fi.title), 'fr', jsonb_build_object('value', fr.title), 'he', jsonb_build_object('value', he.title), 'hr', jsonb_build_object('value', hr.title), 'hu', jsonb_build_object('value', hu.title), 'it', jsonb_build_object('value', it.title), 'ja', jsonb_build_object('value', ja.title), 'ki', jsonb_build_object('value', ki.title), 'ko', jsonb_build_object('value', ko.title), 'ku', jsonb_build_object('value', ku.title), 'lt', jsonb_build_object('value', lt.title), 'lv', jsonb_build_object('value', lv.title), 'mk', jsonb_build_object('value', mk.title), 'nl', jsonb_build_object('value', nl.title), 'no', jsonb_build_object('value', no.title), 'pl', jsonb_build_object('value', pl.title), 'pt', jsonb_build_object('value', pt.title), 'rm', jsonb_build_object('value', rm.title), 'ro', jsonb_build_object('value', ro.title), 'ru', jsonb_build_object('value', ru.title), 'si', jsonb_build_object('value', si.title), 'sk', jsonb_build_object('value', sk.title), 'sl', jsonb_build_object('value', sl.title), 'so', jsonb_build_object('value', so.title), 'sq', jsonb_build_object('value', sq.title))) AS t_title,
    jsonb_merge_recurse("ContentItem".subtitle, jsonb_build_object('am', jsonb_build_object('value', am.subtitle), 'ar', jsonb_build_object('value', ar.subtitle), 'az', jsonb_build_object('value', az.subtitle), 'be', jsonb_build_object('value', be.subtitle), 'bg', jsonb_build_object('value', bg.subtitle), 'bs', jsonb_build_object('value', bs.subtitle), 'ca', jsonb_build_object('value', ca.subtitle), 'ce', jsonb_build_object('value', ce.subtitle), 'cs', jsonb_build_object('value', cs.subtitle), 'cy', jsonb_build_object('value', cy.subtitle), 'da', jsonb_build_object('value', da.subtitle), 'de', jsonb_build_object('value', de.subtitle), 'el', jsonb_build_object('value', el.subtitle), 'en', jsonb_build_object('value', en.subtitle), 'eo', jsonb_build_object('value', eo.subtitle), 'es', jsonb_build_object('value', es.subtitle), 'et', jsonb_build_object('value', et.subtitle), 'fa', jsonb_build_object('value', fa.subtitle), 'fi', jsonb_build_object('value', fi.subtitle), 'fr', jsonb_build_object('value', fr.subtitle), 'he', jsonb_build_object('value', he.subtitle), 'hr', jsonb_build_object('value', hr.subtitle), 'hu', jsonb_build_object('value', hu.subtitle), 'it', jsonb_build_object('value', it.subtitle), 'ja', jsonb_build_object('value', ja.subtitle), 'ki', jsonb_build_object('value', ki.subtitle), 'ko', jsonb_build_object('value', ko.subtitle), 'ku', jsonb_build_object('value', ku.subtitle), 'lt', jsonb_build_object('value', lt.subtitle), 'lv', jsonb_build_object('value', lv.subtitle), 'mk', jsonb_build_object('value', mk.subtitle), 'nl', jsonb_build_object('value', nl.subtitle), 'no', jsonb_build_object('value', no.subtitle), 'pl', jsonb_build_object('value', pl.subtitle), 'pt', jsonb_build_object('value', pt.subtitle), 'rm', jsonb_build_object('value', rm.subtitle), 'ro', jsonb_build_object('value', ro.subtitle), 'ru', jsonb_build_object('value', ru.subtitle), 'si', jsonb_build_object('value', si.subtitle), 'sk', jsonb_build_object('value', sk.subtitle), 'sl', jsonb_build_object('value', sl.subtitle), 'so', jsonb_build_object('value', so.subtitle), 'sq', jsonb_build_object('value', sq.subtitle))) AS t_subtitle,
    jsonb_merge_recurse("ContentItem".summary, jsonb_build_object('am', jsonb_build_object('value', am.summary), 'ar', jsonb_build_object('value', ar.summary), 'az', jsonb_build_object('value', az.summary), 'be', jsonb_build_object('value', be.summary), 'bg', jsonb_build_object('value', bg.summary), 'bs', jsonb_build_object('value', bs.summary), 'ca', jsonb_build_object('value', ca.summary), 'ce', jsonb_build_object('value', ce.summary), 'cs', jsonb_build_object('value', cs.summary), 'cy', jsonb_build_object('value', cy.summary), 'da', jsonb_build_object('value', da.summary), 'de', jsonb_build_object('value', de.summary), 'el', jsonb_build_object('value', el.summary), 'en', jsonb_build_object('value', en.summary), 'eo', jsonb_build_object('value', eo.summary), 'es', jsonb_build_object('value', es.summary), 'et', jsonb_build_object('value', et.summary), 'fa', jsonb_build_object('value', fa.summary), 'fi', jsonb_build_object('value', fi.summary), 'fr', jsonb_build_object('value', fr.summary), 'he', jsonb_build_object('value', he.summary), 'hr', jsonb_build_object('value', hr.summary), 'hu', jsonb_build_object('value', hu.summary), 'it', jsonb_build_object('value', it.summary), 'ja', jsonb_build_object('value', ja.summary), 'ki', jsonb_build_object('value', ki.summary), 'ko', jsonb_build_object('value', ko.summary), 'ku', jsonb_build_object('value', ku.summary), 'lt', jsonb_build_object('value', lt.summary), 'lv', jsonb_build_object('value', lv.summary), 'mk', jsonb_build_object('value', mk.summary), 'nl', jsonb_build_object('value', nl.summary), 'no', jsonb_build_object('value', no.summary), 'pl', jsonb_build_object('value', pl.summary), 'pt', jsonb_build_object('value', pt.summary), 'rm', jsonb_build_object('value', rm.summary), 'ro', jsonb_build_object('value', ro.summary), 'ru', jsonb_build_object('value', ru.summary), 'si', jsonb_build_object('value', si.summary), 'sk', jsonb_build_object('value', sk.summary), 'sl', jsonb_build_object('value', sl.summary), 'so', jsonb_build_object('value', so.summary), 'sq', jsonb_build_object('value', sq.summary))) AS t_summary,
    jsonb_merge_recurse("ContentItem".content, jsonb_build_object('am', jsonb_build_object('value', am.content), 'ar', jsonb_build_object('value', ar.content), 'az', jsonb_build_object('value', az.content), 'be', jsonb_build_object('value', be.content), 'bg', jsonb_build_object('value', bg.content), 'bs', jsonb_build_object('value', bs.content), 'ca', jsonb_build_object('value', ca.content), 'ce', jsonb_build_object('value', ce.content), 'cs', jsonb_build_object('value', cs.content), 'cy', jsonb_build_object('value', cy.content), 'da', jsonb_build_object('value', da.content), 'de', jsonb_build_object('value', de.content), 'el', jsonb_build_object('value', el.content), 'en', jsonb_build_object('value', en.content), 'eo', jsonb_build_object('value', eo.content), 'es', jsonb_build_object('value', es.content), 'et', jsonb_build_object('value', et.content), 'fa', jsonb_build_object('value', fa.content), 'fi', jsonb_build_object('value', fi.content), 'fr', jsonb_build_object('value', fr.content), 'he', jsonb_build_object('value', he.content), 'hr', jsonb_build_object('value', hr.content), 'hu', jsonb_build_object('value', hu.content), 'it', jsonb_build_object('value', it.content), 'ja', jsonb_build_object('value', ja.content), 'ki', jsonb_build_object('value', ki.content), 'ko', jsonb_build_object('value', ko.content), 'ku', jsonb_build_object('value', ku.content), 'lt', jsonb_build_object('value', lt.content), 'lv', jsonb_build_object('value', lv.content), 'mk', jsonb_build_object('value', mk.content), 'nl', jsonb_build_object('value', nl.content), 'no', jsonb_build_object('value', no.content), 'pl', jsonb_build_object('value', pl.content), 'pt', jsonb_build_object('value', pt.content), 'rm', jsonb_build_object('value', rm.content), 'ro', jsonb_build_object('value', ro.content), 'ru', jsonb_build_object('value', ru.content), 'si', jsonb_build_object('value', si.content), 'sk', jsonb_build_object('value', sk.content), 'sl', jsonb_build_object('value', sl.content), 'so', jsonb_build_object('value', so.content), 'sq', jsonb_build_object('value', sq.content))) AS t_content
   FROM "ContentItem"
     LEFT JOIN "ContentItemTranslation" am ON "ContentItem".uid = am."contentItemUid" AND am."targetLanguage" = 'am'::text
     LEFT JOIN "ContentItemTranslation" ar ON "ContentItem".uid = ar."contentItemUid" AND ar."targetLanguage" = 'ar'::text
     LEFT JOIN "ContentItemTranslation" az ON "ContentItem".uid = az."contentItemUid" AND az."targetLanguage" = 'az'::text
     LEFT JOIN "ContentItemTranslation" be ON "ContentItem".uid = be."contentItemUid" AND be."targetLanguage" = 'be'::text
     LEFT JOIN "ContentItemTranslation" bg ON "ContentItem".uid = bg."contentItemUid" AND bg."targetLanguage" = 'bg'::text
     LEFT JOIN "ContentItemTranslation" bs ON "ContentItem".uid = bs."contentItemUid" AND bs."targetLanguage" = 'bs'::text
     LEFT JOIN "ContentItemTranslation" ca ON "ContentItem".uid = ca."contentItemUid" AND ca."targetLanguage" = 'ca'::text
     LEFT JOIN "ContentItemTranslation" ce ON "ContentItem".uid = ce."contentItemUid" AND ce."targetLanguage" = 'ce'::text
     LEFT JOIN "ContentItemTranslation" cs ON "ContentItem".uid = cs."contentItemUid" AND cs."targetLanguage" = 'cs'::text
     LEFT JOIN "ContentItemTranslation" cy ON "ContentItem".uid = cy."contentItemUid" AND cy."targetLanguage" = 'cy'::text
     LEFT JOIN "ContentItemTranslation" da ON "ContentItem".uid = da."contentItemUid" AND da."targetLanguage" = 'da'::text
     LEFT JOIN "ContentItemTranslation" de ON "ContentItem".uid = de."contentItemUid" AND de."targetLanguage" = 'de'::text
     LEFT JOIN "ContentItemTranslation" el ON "ContentItem".uid = el."contentItemUid" AND el."targetLanguage" = 'el'::text
     LEFT JOIN "ContentItemTranslation" en ON "ContentItem".uid = en."contentItemUid" AND en."targetLanguage" = 'en'::text
     LEFT JOIN "ContentItemTranslation" eo ON "ContentItem".uid = eo."contentItemUid" AND eo."targetLanguage" = 'eo'::text
     LEFT JOIN "ContentItemTranslation" es ON "ContentItem".uid = es."contentItemUid" AND es."targetLanguage" = 'es'::text
     LEFT JOIN "ContentItemTranslation" et ON "ContentItem".uid = et."contentItemUid" AND et."targetLanguage" = 'et'::text
     LEFT JOIN "ContentItemTranslation" fa ON "ContentItem".uid = fa."contentItemUid" AND fa."targetLanguage" = 'fa'::text
     LEFT JOIN "ContentItemTranslation" fi ON "ContentItem".uid = fi."contentItemUid" AND fi."targetLanguage" = 'fi'::text
     LEFT JOIN "ContentItemTranslation" fr ON "ContentItem".uid = fr."contentItemUid" AND fr."targetLanguage" = 'fr'::text
     LEFT JOIN "ContentItemTranslation" he ON "ContentItem".uid = he."contentItemUid" AND he."targetLanguage" = 'he'::text
     LEFT JOIN "ContentItemTranslation" hr ON "ContentItem".uid = hr."contentItemUid" AND hr."targetLanguage" = 'hr'::text
     LEFT JOIN "ContentItemTranslation" hu ON "ContentItem".uid = hu."contentItemUid" AND hu."targetLanguage" = 'hu'::text
     LEFT JOIN "ContentItemTranslation" it ON "ContentItem".uid = it."contentItemUid" AND it."targetLanguage" = 'it'::text
     LEFT JOIN "ContentItemTranslation" ja ON "ContentItem".uid = ja."contentItemUid" AND ja."targetLanguage" = 'ja'::text
     LEFT JOIN "ContentItemTranslation" ki ON "ContentItem".uid = ki."contentItemUid" AND ki."targetLanguage" = 'ki'::text
     LEFT JOIN "ContentItemTranslation" ko ON "ContentItem".uid = ko."contentItemUid" AND ko."targetLanguage" = 'ko'::text
     LEFT JOIN "ContentItemTranslation" ku ON "ContentItem".uid = ku."contentItemUid" AND ku."targetLanguage" = 'ku'::text
     LEFT JOIN "ContentItemTranslation" lt ON "ContentItem".uid = lt."contentItemUid" AND lt."targetLanguage" = 'lt'::text
     LEFT JOIN "ContentItemTranslation" lv ON "ContentItem".uid = lv."contentItemUid" AND lv."targetLanguage" = 'lv'::text
     LEFT JOIN "ContentItemTranslation" mk ON "ContentItem".uid = mk."contentItemUid" AND mk."targetLanguage" = 'mk'::text
     LEFT JOIN "ContentItemTranslation" nl ON "ContentItem".uid = nl."contentItemUid" AND nl."targetLanguage" = 'nl'::text
     LEFT JOIN "ContentItemTranslation" no ON "ContentItem".uid = no."contentItemUid" AND no."targetLanguage" = 'no'::text
     LEFT JOIN "ContentItemTranslation" pl ON "ContentItem".uid = pl."contentItemUid" AND pl."targetLanguage" = 'pl'::text
     LEFT JOIN "ContentItemTranslation" pt ON "ContentItem".uid = pt."contentItemUid" AND pt."targetLanguage" = 'pt'::text
     LEFT JOIN "ContentItemTranslation" rm ON "ContentItem".uid = rm."contentItemUid" AND rm."targetLanguage" = 'rm'::text
     LEFT JOIN "ContentItemTranslation" ro ON "ContentItem".uid = ro."contentItemUid" AND ro."targetLanguage" = 'ro'::text
     LEFT JOIN "ContentItemTranslation" ru ON "ContentItem".uid = ru."contentItemUid" AND ru."targetLanguage" = 'ru'::text
     LEFT JOIN "ContentItemTranslation" si ON "ContentItem".uid = si."contentItemUid" AND si."targetLanguage" = 'si'::text
     LEFT JOIN "ContentItemTranslation" sk ON "ContentItem".uid = sk."contentItemUid" AND sk."targetLanguage" = 'sk'::text
     LEFT JOIN "ContentItemTranslation" sl ON "ContentItem".uid = sl."contentItemUid" AND sl."targetLanguage" = 'sl'::text
     LEFT JOIN "ContentItemTranslation" so ON "ContentItem".uid = so."contentItemUid" AND so."targetLanguage" = 'so'::text
     LEFT JOIN "ContentItemTranslation" sq ON "ContentItem".uid = sq."contentItemUid" AND sq."targetLanguage" = 'sq'::text
     LEFT JOIN "ContentItemTranslation" sr ON "ContentItem".uid = sr."contentItemUid" AND sr."targetLanguage" = 'sr'::text
     LEFT JOIN "ContentItemTranslation" sv ON "ContentItem".uid = sv."contentItemUid" AND sv."targetLanguage" = 'sv'::text
     LEFT JOIN "ContentItemTranslation" th ON "ContentItem".uid = th."contentItemUid" AND th."targetLanguage" = 'th'::text
     LEFT JOIN "ContentItemTranslation" tr ON "ContentItem".uid = tr."contentItemUid" AND tr."targetLanguage" = 'tr'::text
     LEFT JOIN "ContentItemTranslation" uk ON "ContentItem".uid = uk."contentItemUid" AND uk."targetLanguage" = 'uk'::text
     LEFT JOIN "ContentItemTranslation" ur ON "ContentItem".uid = ur."contentItemUid" AND ur."targetLanguage" = 'ur'::text
     LEFT JOIN "ContentItemTranslation" yi ON "ContentItem".uid = yi."contentItemUid" AND yi."targetLanguage" = 'yi'::text
     LEFT JOIN "ContentItemTranslation" zh ON "ContentItem".uid = zh."contentItemUid" AND zh."targetLanguage" = 'zh'::text;

INSERT INTO public."Language"(title, title_de, shortcode, isoocde)
SELECT DISTINCT 
--"originalLanguages",
("originalLanguages"->'language_codes'->>0) as title,
("originalLanguages"->'language_codes'->>0) as title_de,
("originalLanguages"->'language_codes'->>0) as shortcode,
CONCAT("originalLanguages"->'language_codes'->>0,'_',"originalLanguages"->'language_codes'->>0) as isoocde
FROM public."ContentItem"
WHERE 
"originalLanguages" ? 'language_codes' = true

