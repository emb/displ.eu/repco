-- CreateTable
CREATE TABLE  IF NOT EXISTS "SourceRecordArchive" (
    "uid" TEXT NOT NULL,
    "timestamp" TIMESTAMP(3) NOT NULL,
    "sourceUri" TEXT NOT NULL,
    "containedEntityUris" TEXT[],
    "contentType" TEXT NOT NULL,
    "sourceType" TEXT NOT NULL,
    "body" TEXT NOT NULL,
    "meta" JSONB,
    "dataSourceUid" TEXT,

    CONSTRAINT "SourceRecordArchive_pkey" PRIMARY KEY ("uid")
);

-- CreateTable
CREATE TABLE  IF NOT EXISTS "ContentItemTranslation" (
    "uid" TEXT NOT NULL DEFAULT gen_random_uuid(),
    "contentItemUid" TEXT NOT NULL,
    "targetLanguage" TEXT NOT NULL,
    "revisionId" TEXT NOT NULL,
    "title" TEXT,
    "subtitle" TEXT,
    "summary" TEXT,
    "summaryCleaned" TEXT,
    "content" TEXT,
    "contentCleaned" TEXT,
    "metadata" TEXT,
    "engine" TEXT,
    "createdAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updatedAt" TIMESTAMP(3),

    CONSTRAINT "ContentItemTranslation_pkey" PRIMARY KEY ("uid")
);

-- CreateTable
CREATE TABLE  IF NOT EXISTS "Language" (
    "uid" TEXT NOT NULL DEFAULT gen_random_uuid(),
    "title" TEXT NOT NULL,
    "title_de" TEXT NOT NULL,
    "shortcode" TEXT NOT NULL,
    "isoocde" TEXT NOT NULL,

    CONSTRAINT "Language_pkey" PRIMARY KEY ("uid")
);

-- CreateIndex
CREATE UNIQUE INDEX  IF NOT EXISTS "SourceRecordArchive_uid_key" ON "SourceRecordArchive"("uid");

-- CreateIndex
CREATE UNIQUE INDEX  IF NOT EXISTS "SourceRecordArchive_uid_key" ON "SourceRecordArchive"("uid");
CREATE UNIQUE INDEX  IF NOT EXISTS "ContentItemTranslation_uid_key" ON "ContentItemTranslation"("uid");

-- CreateIndex
CREATE UNIQUE INDEX  IF NOT EXISTS "SourceRecordArchive_uid_key" ON "SourceRecordArchive"("uid");
CREATE UNIQUE INDEX  IF NOT EXISTS "ContentItemTranslation_contentItemUid_targetLanguage_revisi_key" ON "ContentItemTranslation"("contentItemUid", "targetLanguage", "revisionId");

-- CreateIndex
CREATE UNIQUE INDEX  IF NOT EXISTS "SourceRecordArchive_uid_key" ON "SourceRecordArchive"("uid");
CREATE UNIQUE INDEX  IF NOT EXISTS "Language_uid_key" ON "Language"("uid");


-- AddForeignKey
DO $$ BEGIN
    IF NOT EXISTS (SELECT FROM pg_constraint 
                   WHERE conrelid = '"SourceRecordArchive"'::regclass AND conname = 'SourceRecordArchive_dataSourceUid_fkey') THEN 
        ALTER TABLE "SourceRecordArchive" ADD CONSTRAINT "SourceRecordArchive_dataSourceUid_fkey" FOREIGN KEY ("dataSourceUid") REFERENCES "DataSource"("uid") ON DELETE CASCADE ON UPDATE CASCADE;
    END IF;
END $$;


-- AddForeignKey

DO $$ BEGIN
    IF NOT EXISTS (SELECT FROM pg_constraint 
                   WHERE conrelid = '"ContentItemTranslation"'::regclass AND conname = 'ContentItemTranslation_revisionId_fkey') THEN 
        ALTER TABLE "ContentItemTranslation" DROP CONSTRAINT IF EXISTS "ContentItemTranslation_revisionId_fkey";
    END IF;
END $$;

-- AddForeignKey


DO $$ BEGIN
    IF NOT EXISTS (SELECT FROM pg_constraint 
                   WHERE conrelid = '"ContentItemTranslation"'::regclass AND conname = 'ContentItemTranslation_contentItemUid_fkey') THEN 
        ALTER TABLE "ContentItemTranslation" DROP CONSTRAINT IF EXISTS "ContentItemTranslation_contentItemUid_fkey";
    END IF;
END $$;