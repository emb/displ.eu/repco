/**
 * Routes for API
 */
const API_ROOT = '/api'
export const API_PATH = {
  root: API_ROOT,
  translations: {
    root: '/translations',
    byUid: '/translations/:uid',
  },
  recommendations: {
    root: '/recommendations',
  },
}
