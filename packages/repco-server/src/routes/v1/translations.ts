import express, { Router } from 'express'
import routeCache from 'route-cache'
import handlers from '../../handlers/index.js'
import { API_PATH } from '../paths/index.js'
import { QueryValidator, UidValidator } from '../../middleware/validation.js'
import { LanguageParameter } from '../../schema/parameters.js'

// ----------------------------------------------------------------------

const router: Router = express.Router()

// ----------------------------------------------------------------------

router.get(
  API_PATH.translations.byUid,
  [
    routeCache.cacheSeconds(1800),
    UidValidator(),
    QueryValidator(LanguageParameter),
  ],
  handlers.translations.getHandler,
)

// ----------------------------------------------------------------------

export default router
