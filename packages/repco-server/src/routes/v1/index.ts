import bodyParser from 'body-parser'
import express, { Router } from 'express'
import recommendations from './recommendations.js'
import translations from './translations.js'

// ----------------------------------------------------------------------

const router: Router = express.Router()

// ----------------------------------------------------------------------

router.use(bodyParser.json())
router.use(recommendations)
router.use(translations)

// ----------------------------------------------------------------------

export default router
