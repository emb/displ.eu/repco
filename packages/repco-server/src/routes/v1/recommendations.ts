import express, { Router } from 'express'
import routeCache from 'route-cache'
import handlers from '../../handlers/index.js'
import { API_PATH } from '../paths/index.js'
import { QueryValidator } from '../../middleware/validation.js'
import { RecommendationsListParameters } from '../../schema/recommendations.js'

// ----------------------------------------------------------------------

const router: Router = express.Router()

// ----------------------------------------------------------------------

router.get(
  API_PATH.recommendations.root,
  [
    routeCache.cacheSeconds(1800),
    QueryValidator(RecommendationsListParameters),
  ],
  handlers.recommendations.getListHandler,
)

// ----------------------------------------------------------------------

export default router
