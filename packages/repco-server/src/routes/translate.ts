import express, { Router } from 'express'
import validator from 'is-my-json-valid'
import { CID } from 'multiformats/cid'
import {
  ContentLoaderStream,
  createTranslationId,
  DagJSON,
  encodeHeader,
  entryIpld,
  HeadersIpld,
  HttpError,
  PrismaIpldBlockStore,
  repoRegistry,
  revisionIpld,
} from 'repco-core'
import { base32 } from 'multiformats/bases/base32'
import { createHash, randomBytes } from 'crypto'
import { Readable } from 'stream'
import { router as adminRouter } from './admin.js'
import { notFoundHandler, ServerError } from '../error.js'
import { getLocals } from '../lib.js'
import { PrismaClient } from 'repco-core'
import {
  acceptNdJson,
  collectStream,
  flattenStream,
  sendNdJsonStream,
} from '../util.js'
import { Json } from 'repco-common/dist/src/zod.js'
import { ContentItemTranslation, Language } from 'repco-core/dist/src/prisma.js'


const router:Router = express.Router()

// const HEADER_JSON = 'application/json'
const HEADER_CAR = 'application/vnd.ipld.car'

const TRANSLATION_TOKEN = process.env.REPCO_TRANSLATION_TOKEN

export function authorizeRequest(req: express.Request) {
  if (!TRANSLATION_TOKEN || TRANSLATION_TOKEN.length < 16) {
    return false
  }

  const authHeader = req.headers['authorization']
  if (!authHeader || !authHeader.startsWith('Bearer ')) {
    return false
  }

  const token = authHeader.substring(7)
  if (token == TRANSLATION_TOKEN) {
    return true
  } else {
    return false
  }
}


router.use((req, res, next) => {
  if (!TRANSLATION_TOKEN || TRANSLATION_TOKEN.length < 16) {
    res.locals.log.warn(
      'TRANSLATION_TOKEN is not set or too short (min 16 characters needed). Admin access disabled.',
    )
    return next(new ServerError(403, 'Unauthorized'))
  }
  if (!authorizeRequest(req)) {
    next(new ServerError(403, 'Unauthorized'))
  } else {
    next()
  }
})

router.get('/test', async (req, res) => {
  await new Promise((resolve) => setTimeout(resolve, 1000))
  res.send({ ok: true })
})

router.get('/missing/:limit', async (req, res) => {
  const { limit } = req.params
  const { prisma } = getLocals(res)
  const dumpsql = `
SELECT * FROM public."MissingTranslations"
WHERE title is null
LIMIT ${limit}
    `
  const records = await prisma.$queryRawUnsafe<Object[]>(dumpsql)
  .catch((err) => {
    res.status(500).send({
      'error': `Error loading missing Translations`,
    }) 
  })

  res.send({ records })
})

router.get('/contentitem/:uid', async (req, res) => {
  const { prisma } = getLocals(res)
  const { uid } = req.params
  //console.log(uid)
  const contentItem = await prisma.contentItem.findUnique({
    where: {
      uid:uid
    }
  }).catch((err) => {
    res.status(500).send({
      'error': `Error ${uid} ${err}`,
    }) 
  })
  const contentItemTranslation = await prisma.contentItemTranslation.findMany({
    where: {
      contentItemUid: uid
    }
  }).catch((err) => {
    res.status(500).send({
      'error': `Error ${uid} ${err}`,
    }) 
  })
  //if (!contentItem) throw new HttpError(404, uid+' Not found')
  //const uid = await repoRegistry.open(prisma, req.params.uid)
/*  const revisionCount = await repo.prisma.revision.count({
    where: { repoDid: repo.did },
  })
  const commitCount = await repo.prisma.commit.count({
    where: { repoDid: repo.did },
  })*/
  const info = {
    calleduid: req.params.uid,
    contentItem,
    contentItemTranslation
  }
  res.send({ info })
})

async function validateTranslationBody(item:Json,lang:string,languages:any) {
  var validate = validator({
    required: true,
    type: 'object',
    properties: {
      contentItemUid: {required: true,type: 'string'},
      revisionId: {required: true,type: 'string'},
      title: {required: true,type: 'string'},
      subtitle: {required: true,type: 'string'},
      summary: {required: true,type: 'string'},
      summaryCleaned: {required: true,type: 'string'},
      content: {required: true,type: 'string'},
      contentCleaned: {required: true,type: 'string'},
      metadata: {required: true,type: 'string'},
      engine: {required: true,type: 'string'},
      targetLanguage: {required: true,type: 'string'},
    }
  })
  validate(item)
  
  if (validate.errors) {
    const info = {
      'errors': validate.errors
    }
    return info;
  }

  if (languages.indexOf(lang) === -1) {
    console.log('langmissing')
    const info = {
      'errors': `language: ${lang} not found`
    }
    return info;
  }
  return true
}

/*function findTranslation(
  contentItemUid:string,
  revisionId:string,
  targetLanguage:string,
  prisma:PrismaClient
) {
  
  const recordexist = prisma.contentItemTranslation.findFirst({
      where : {
        contentItemUid: contentItemUid,
        revisionId: revisionId,
        targetLanguage:targetLanguage
      }
    }
  )
  console.log(contentItemUid,revisionId,targetLanguage, recordexist)
  return recordexist
}*/




async function saveTranslation(
  item:any, prisma:PrismaClient, languages:any, update:boolean = false
) {
  let valid = await validateTranslationBody(item,item.targetLanguage,languages)
  if (valid===true) {
    const body = item
    let record = await prisma.contentItemTranslation.findFirst({
      where : {
        contentItemUid: item.contentItemUid,
        revisionId: item.revisionId,
        targetLanguage:item.targetLanguage
        }
      }
    )

    //console.log(record)
    //console.log(update)
    let savedata = body
    if (update===false) {
      if (record===null) {
        //savedata.uid = createTranslationId()
        savedata.createdAt = new Date();
        savedata.updatedAt = new Date();
          savedata = prisma.contentItemTranslation.create(
            { data: savedata }
          ).catch((err) => {
            return {
              'error': `Insert failed ${err} `,
              item
            }
          })

        savedata.exist = false
        //console.log('created')
        return savedata
      } else {
        //console.log('is not  null??')
        return {
          contentItemUid: item.contentItemUid,
          revisionId: item.revisionId,
          targetLanguage: item.targetLanguage,
          'exist':true,
          'error': 'ContentItemTranslation was found - use PUT for update',
          record
        };
      }
    } else {
      if (record===null) {
        return {
          contentItemUid: item.contentItemUid,
          revisionId: item.revisionId,
          targetLanguage: item.targetLanguage,
          'exist':false,
          'error': 'Related ContentItemTranslation is missing'
        };
      } else {
        savedata.uid=record.uid
        //savedata.createdAt=record.createdAt
        savedata.updatedAt = new Date();
        const where = { 
          uid:savedata.uid
        }
        savedata = await prisma.contentItemTranslation.upsert(
          { create: savedata,
            update: savedata,
            where
          }
        ).catch((err) => {
          //console.error(err)
          return {
            'error': `Insert failed ${err} `,
            item
          }
        })

        savedata.exist = true
        savedata.updated = true
        //console.log('created')
        return savedata
      }
    }
  } else {
    return valid
  }
}

async function saveTranslations(
  { items, prisma,update=false }:
   { items:Array<Json>; prisma:PrismaClient,update:boolean }) {
    let languages = await prisma.language.findMany({select: {shortcode:true}})
    //console.log(languages)
    var langlist = Array()
    for(var i = 0;i<languages.length;i++) {
      var l = languages[i]
      langlist.push(l.shortcode)
    }
    //console.log(langlist)

    let results = Array()
    for(var i = 0;i<items.length;i++) {
      var item = items[i]
      let info = await saveTranslation(item, prisma,langlist,update)
      results.push(info)
    }
    return results
  }

router.post('/save', async (req, res, _next): Promise<void> => {
  let result:any = false
  const { prisma } = getLocals(res)
  //console.log(req.body)
  if (Array.isArray(req.body)) {
    result = await saveTranslations({ items: req.body, prisma: prisma, update: false })
  } else {
    result = 'Array structure is missing'
  }
  const info = {
    result: result
  }
  res.send({ info })
})

router.put('/save', async (req, res, _next): Promise<void> => {
  let result:any = false
  const { prisma } = getLocals(res)
  //console.log(req.body)
  if (Array.isArray(req.body)) {
    result = await saveTranslations({ items: req.body, prisma: prisma, update: true })
  } else {
    result = 'Array structure is missing'
  }
  const info = {
    result: result
  }
  res.send({ info })
})

router.use(notFoundHandler)

export default router
