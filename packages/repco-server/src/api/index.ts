// Importing APIs
import recommendations from './recommendations/index.js'
import translations from './translations/index.js'

// ----------------------------------------------------------------------

// Exporting available APIs
export default {
  recommendations,
  translations,
}
