import { Prisma } from 'repco-prisma'
import prisma from '../../utilities/prisma.js'

// ----------------------------------------------------------------------

/**
 * Implementing API for translations
 */
export default {
  /**
   * Retrieves a single content item translation based on the provided unique input.
   *
   * @param {Prisma.ContentItemTranslationWhereUniqueInput} where - The unique input to find the content item translation.
   * @returns {Promise<Prisma.ContentItemTranslationGetPayload<{ include: { ContentItem: { include: { MediaAssets: { include: { Files: true } } } } } }>>} - The content item translation with its associated content item and media assets.
   */
  get: async (
    where: Prisma.ContentItemTranslationWhereInput,
  ): Promise<
    Prisma.ContentItemTranslationGetPayload<{
      include: {
        ContentItem: {
          include: {
            MediaAssets: {
              include: {
                Files: true
              }
            }
          }
        }
      }
    }>
  > => {
    return prisma.contentItemTranslation.findFirstOrThrow({
      where,
      include: {
        ContentItem: {
          include: {
            MediaAssets: {
              include: {
                Files: true,
              },
              where: {
                mediaType: 'image',
              },
            },
          },
        },
      },
    })
  },
}
