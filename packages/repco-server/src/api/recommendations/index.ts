import { Prisma } from 'repco-prisma'
import prisma from '../../utilities/prisma.js'

// ----------------------------------------------------------------------

/**
 * Implementing API for recommendations
 */
export default {
  /**
   * Retrieves a list of content items based on the provided query parameters.
   * Uses a Prisma transaction to perform two operations: finding the matching content items and counting them.
   * Supports pagination with limit and page parameters.
   *
   * @param {number} [limit=50] - The maximum number of content items to retrieve.
   * @param {number} [page=0] - The page number for pagination.
   * @param {Prisma.ContentItemWhereInput} [where] - The filter conditions for the query.
   * @returns {Promise<{ count: number, elements: Prisma.ContentItemGetPayload<{ include: { ContentItemTranslations: true, MediaAssets: { include: { Files: true } } } }>[] } > } - An object containing the total count of matching content items and the list of content items with their associated translations and media assets.
   */
  getList: async (
    limit = 50,
    page = 0,
    where?: Prisma.ContentItemWhereInput,
  ): Promise<{
    count: number
    elements: Prisma.ContentItemGetPayload<{
      include: {
        ContentItemTranslations: true
        MediaAssets: {
          include: {
            Files: true
          }
        }
      }
    }>[]
  }> => {
    const [recommendations, count] = await prisma.$transaction([
      prisma.contentItem.findMany({
        skip: page * limit,
        take: limit,
        orderBy: {
          pubDate: 'desc',
        },
        where,
        include: {
          ContentItemTranslations: true,
          MediaAssets: {
            include: {
              Files: true,
            },
          },
        },
      }),
      prisma.contentItem.count({ where }),
    ])

    return {
      count,
      elements: recommendations,
    }
  },
}
