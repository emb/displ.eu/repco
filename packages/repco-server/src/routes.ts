import express, { Router } from 'express'
import apiRoutes from './routes/api.js'
import translateRoutes from './routes/translate.js'
import { enableFrontend, router as frontendRoutes } from './routes/frontend.js'

// Enable frontend. This could be put behind a environement variable.
enableFrontend()

const router: Router = express.Router()
router.use('/api', apiRoutes)
router.use('/translate', translateRoutes)
router.use('/', frontendRoutes)

export default router
