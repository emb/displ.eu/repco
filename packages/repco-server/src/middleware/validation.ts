/**
 * Middleware to validate requests using a Zod schema.
 * Inspired by: https://www.reddit.com/r/node/comments/1bcjy5p/comment/kuiwimz
 */

import { NextFunction, Request, Response } from 'express'
import { z, ZodObject, ZodRawShape } from 'zod'
import { ServerError } from '../error.js'

// ----------------------------------------------------------------------

export function BodyValidator<T extends ZodRawShape>(schema: ZodObject<T>) {
  return (req: Request, res: Response, next: NextFunction) => {
    try {
      req.body = schema.parse(req.body)
      next()
    } catch (error) {
      throw new ServerError(400, 'Invalid body: ' + error)
    }
  }
}

// ----------------------------------------------------------------------

export function UidValidator(uidField = 'uid') {
  return (req: Request, res: Response, next: NextFunction) => {
    try {
      req.params[uidField] = z
        .string({ coerce: true })
        .parse(req.params[uidField]) as string
      next()
    } catch (error) {
      throw new ServerError(400, 'Invalid uid: ' + error)
    }
  }
}

// ----------------------------------------------------------------------

export function ParameterValidator<T extends ZodRawShape>(
  schema: ZodObject<T>,
) {
  return (req: Request, res: Response, next: NextFunction) => {
    try {
      req.params = schema.parse(req.params)
      next()
    } catch (error) {
      throw new ServerError(400, 'Invalid parameter: ' + error)
    }
  }
}

// ----------------------------------------------------------------------

export function QueryValidator<T extends ZodRawShape>(schema: ZodObject<T>) {
  return (req: Request, res: Response, next: NextFunction) => {
    try {
      req.query = schema.parse(req.query)
      next()
    } catch (error) {
      throw new ServerError(400, 'Invalid query: ' + error)
    }
  }
}
