import { PrismaClient } from 'repco-prisma'

const prisma = new PrismaClient()

export default prisma
