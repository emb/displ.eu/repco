import { z } from 'zod'
import { DefaultParameters, LanguageParameter } from './parameters.js'

// ----------------------------------------------------------------------

export const Recommendation = z.object({
  date: z.string().datetime(),
  thumbnailUrl: z.string().optional(),
  title: z.string().optional(),
  uid: z.string().cuid2(),
})

export type RecommendationType = z.infer<typeof Recommendation>

// ----------------------------------------------------------------------

export const RecommendationsListParameters =
  DefaultParameters.merge(LanguageParameter)

export type RecommendationsListParametersType = z.infer<
  typeof RecommendationsListParameters
>
