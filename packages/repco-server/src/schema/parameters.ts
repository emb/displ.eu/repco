import { z } from 'zod'

// ----------------------------------------------------------------------

export const QueryOrder = z.enum(['asc', 'desc'])

// ----------------------------------------------------------------------

// Defining type for default parameters
export const DefaultParameters = z.object({
  limit: z.coerce.number().optional(),
  order: QueryOrder.optional(),
  page: z.coerce.number().optional(),
})

// Defining type for language parameters
export const LanguageParameter = z.object({
  language: z.string().min(2).max(2).nullish(),
})

// Defining type for uid parameter
export const UidParameter = z.object({
  uid: z.string().cuid2(),
})
