import { Request, Response } from 'express'
import { Prisma } from 'repco-prisma'
import api from '../../api/index.js'
import { LanguageParameter, UidParameter } from '../../schema/parameters.js'

// ----------------------------------------------------------------------

/**
 * Implementing data handlers for translations
 */
export default {
  /**
   * Handler function to retrieve a single content item translation based on the provided UID and language.
   * Parses the query parameters, queries the database for the translation, and returns the restructured translation data.
   * Handles errors and returns appropriate HTTP status codes and messages.
   *
   * @param {Request} req - The request object.
   * @param {Response} res - The response object.
   * @returns {Promise<Response>} - The response containing the translation data or an error message.
   */
  getHandler: async (req: Request, res: Response) => {
    // Parsing provided query parameters
    const { uid } = UidParameter.parse(req.params)
    const { language } = LanguageParameter.parse(req.query)

    console.log(uid, language)

    try {
      // Querying the database for translations
      const translation = await api.translations.get({
        contentItemUid: uid,
        targetLanguage: String(language),
        ContentItem: {
          removed: false,
        },
      })

      // Returning restructured translation data
      return res.json({
        content: translation.content || undefined,
        date: translation.ContentItem.pubDate || undefined,
        subtitle: translation.subtitle || undefined,
        summary: translation.summary || undefined,
        thumbnailUrl:
          translation.ContentItem.MediaAssets[0]?.Files[0]?.contentUrl ||
          undefined,
        title: translation.title || undefined,
        uid,
      })
    } catch (error) {
      // Checking if error is associated with missing translation
      if (error instanceof Prisma.PrismaClientKnownRequestError) {
        if (error.code === 'P2025') {
          return res
            .status(404)
            .send({ ok: false, message: 'Translation does not exist' })
        }
      }

      // Returning default error
      return res
        .status(500)
        .send({ ok: false, message: 'Error while querying data' })
    }
  },
}
