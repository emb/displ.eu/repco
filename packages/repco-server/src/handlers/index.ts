// Importing handlers
import recommendations from './recommendations/index.js'
import translations from './translations/index.js'

// ----------------------------------------------------------------------

// Exporting available handlers
export default {
  recommendations,
  translations,
}
