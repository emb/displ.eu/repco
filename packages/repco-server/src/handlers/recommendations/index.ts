import { Request, Response } from 'express'
import api from '../../api/index.js'
import { RecommendationsListParameters } from '../../schema/recommendations.js'

// ----------------------------------------------------------------------

/**
 * Implementing data handlers for recommendations
 */
export default {
  /**
   * Handler function to retrieve a list of recommendations based on the provided query parameters.
   * Parses the query parameters, queries the database for recommendations, and returns the restructured recommendation data.
   * Handles errors and returns appropriate HTTP status codes and messages.
   *
   * @param {Request} req - The request object.
   * @param {Response} res - The response object.
   * @returns {Promise<Response>} - The response containing the recommendation data or an error message.
   */
  getListHandler: async (req: Request, res: Response) => {
    // Parsing provided query parameters
    const { language, limit, page } = RecommendationsListParameters.parse(
      req.query,
    )

    try {
      // Querying the database for recommendations
      const recommendations = await api.recommendations.getList(limit, page, {
        removed: false,
        ...(language && {
          ContentItemTranslations: {
            some: {
              targetLanguage: language,
            },
          },
        }),
        MediaAssets: {
          some: {
            mediaType: 'image',
            NOT: {
              uid: undefined,
            },
          },
        },
      })

      // Initializing an empty array to store the output
      const output = []

      // Iterating over each recommendation in the recommendations array
      // We do this so we can restructure the output
      for (const recommendation of recommendations.elements) {
        // Creating a new object with the desired properties and pushing it to the output array
        output.push({
          date: recommendation.pubDate || undefined,
          thumbnailUrl:
            recommendation.MediaAssets[0].Files[0].contentUrl || undefined,
          title: recommendation.ContentItemTranslations[0].title || undefined,
          uid: recommendation.uid,
        })
      }

      // Returning the output as a JSON response
      return res.json({
        count: output.length,
        elements: output,
      })
    } catch (error) {
      // Returning default error
      return res
        .status(500)
        .send({ ok: false, message: 'Error while querying data' })
    }
  },
}
