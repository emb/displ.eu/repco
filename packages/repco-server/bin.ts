import Dotenv from 'dotenv'
import { PrismaClient } from 'repco-core'
import { runServer } from './src/lib.js'

Dotenv.config()
Dotenv.config({ path: '../../.env' })

//console.log(`Memory Usage start: ${JSON.stringify(process.memoryUsage())}`)
const prisma = new PrismaClient()
const port: number = Number(process.env.HTTP_PORT) || Number(process.env.PORT)

const host = String(process.env.REPCO_URL)
const url = `http://${host}:${port}`

runServer({ prisma, opts: { port, publicUrl: url } })
