import { Prisma, PrismaClient } from 'repco-prisma'

let confirmdelete:string = 'no'

export async function startsourceRecordDeduplication(deleterecords:any) {
    const prisma = new PrismaClient()
    confirmdelete = deleterecords
    //const dump = parseInt(args.dump||'')

    const records = await prisma.sourceRecord.groupBy({
    by: ['sourceType', 'sourceUri' ,'containedEntityUris' ],
    _count: {
        _all: true,
    }
    })

    records.forEach((row) => {
    if (row._count._all.valueOf() > 1) {
        console.log("Deeper Check - Duplicates found"+row._count._all.valueOf())
        //console.log("mochmawos"+row.containedEntityUris.toString())
        sourceRecordDeduplicationEntities(
            row._count._all.valueOf(),
            row.containedEntityUris.toString(),
            prisma
        )
    }
    })
}


export async function sourceRecordDeduplicationEntities(
    lastcount: number,
    EntityUris: string,
    prisma: any
) {
    //console.log('inside')
    //return res
    //const prisma = new PrismaClient()
  
    const filteredrecords =  await prisma.sourceRecord.groupBy({
      by: ['body', 'sourceType', 'sourceUri' ,'containedEntityUris' ],
      _count: {
        _all: true
      },
      where: {
        containedEntityUris: { has: EntityUris}
      }
    })
    filteredrecords.forEach((row:any) => {
      if (row._count._all.valueOf() > 1) {
        console.log("Duplicates examined "+ lastcount + " > " +row._count._all.valueOf())
          //+row.containedEntityUris.toString())
          getDuplicateEntitySortedByDate(row,prisma)
      }
    })
  
  }
  
  export async function getDuplicateEntitySortedByDate(
    dataset: any,
    prisma: any
  ) {
    //const prisma = new PrismaClient()
    //const filteredrecords =  await prisma.
    //console.log(dataset)
    const removablerecords =  await prisma.sourceRecord.findMany({
      select: { uid:true, timestamp:true },
      where: {
        body: dataset.body,
        sourceType: dataset.sourceType,
        sourceUri: dataset.sourceUri,
        containedEntityUris: { has: dataset.containedEntityUris.toString() }
      }
    })
    //removablerecords.length
    removablerecords.forEach((row:any,index:number) => {
      console.log(index + "/" +removablerecords.length + " uid " + row.uid + " " + row.timestamp)    
      if (++index < removablerecords.length) {
        console.log('delete')
        DeleteEntity(row.uid,prisma)
      } else {
        console.log('keep last')
      }
    })
  }
  
  export async function DeleteEntity(
    uid: string,
    prisma: any
  ) {
    //const prisma = new PrismaClient()
    console.log('DELETE '+uid + " Status?"+ confirmdelete)
    if (confirmdelete == 'yes') {
        const removablerecords =  await prisma.sourceRecord.delete({
        where: { 
            uid: uid
        }
        })
    }
  }