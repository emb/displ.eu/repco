# Workpackage/Task

## DISPL.EU 2023

* *Name:* **Metadata Replication**
* *Task-ID:* **T2.5**
* *Participants:*
  * **CBA**
  * **fairkom**
* *ERP:* **[TASK00166](https://erp.fairkom.net/app/task/TASK00166)**
